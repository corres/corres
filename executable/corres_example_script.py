#!/usr/bin/python

# import corres
import time

import corres.forecasts as fc
from corres.api import Corres, get_dask_client, save_vars
from corres.auxiliar_functions import clean, mkdir
from corres.fluctuations import (
    Kaimal_S_f,
    Schlez_coherence_jk_f,
    Soerensen_circular_plant_Fwf_f,
    XGLarsen_S_f,
)

if __name__ == "__main__":

    start = time.time()

    # -----------------------------------------------------------------------
    # Working on SOPHIA super computer
    # -----------------------------------------------------------------------

    # ------
    # ERA5
    # ------

    # WPP example
    config_fn = "config.yml"
    locs_fn = "../examples/datasets/WPP_dataset_tests.xlsx"
    power_curves_fn = "../examples/datasets/PowerCurves.xlsx"
    domain = None
    dataset = "ERA5"
    exec_type = "container"
    simulation = "WPP"
    # start_date = '1979-01-01'
    start_date = "2019-01-01"
    end_date = "2020-12-31"
    group_by = None  #'Country' #
    weight_var = None  #'Installed_capacity_MW'
    out_dir = "./Results/"

    # -----------------------------------------------------------------------
    # CorRES code to excecute
    # -----------------------------------------------------------------------

    cr = Corres(
        config_fn=config_fn,
        locs_fn=locs_fn,
        power_curves_fn=power_curves_fn,
        dataset=dataset,
        domain=domain,
        exec_type=exec_type,
        simulation=simulation,
        start_date=start_date,
        end_date=end_date,
        group_by=group_by,
        weight_var=weight_var,
    )

    client = get_dask_client()

    # Extract time series from reanalysis dataset
    cr.extract_ts()

    # conversion to power
    df_out = cr.run_power(case="meso")

    # save outputs as csv
    save_vars(
        df_var=df_out,
        out_dir=out_dir,
    )

    # # Wind speed fluctuation model
    # cr.wind_speed_fluctuations(
    #     seed=0, # seed number
    #     dt = 5*60,  # [s]
    #     period_fluct=2 * 365 * 24 * 60 * 60,  # [s]
    #     spectra=[
    #         XGLarsen_S_f(
    #             a1 = 2e-4,
    #             f0 = 1 / (10 * 60 * 60) )#,
    #         #Kaimal_S_f(
    #         #    sigma=1.,
    #         #    z=100,
    #         #    V0=8,
    #         #    f0 = 1 / (10 * 60 * 60),
    #         #    )
    #         ],
    #     #admittance=Soerensen_circular_plant_Fwf_f(),
    #     coherence=Schlez_coherence_jk_f(),
    #     #t_distributed=None,
    #     #df = 4, # df for t-student
    #     #look_up_WS=False,
    #     #std_factor=1, # std(ws_fluct) scaling factor over ws
    #     )

    # # conversion to power
    # df_out = cr.run_power(
    #     case='fluct'
    #     )

    # # save outputs as csv
    # save_vars(
    #     df_var = df_out,
    #     out_dir = out_dir,
    #     )

    # # Forecast errors
    # # Specify update freq and lead times for all forecast runs (DA, HA, etc.)
    # DA = fc.forecRun(updateFreq = 24, leadTime=0, name='DA')
    # HA = fc.forecRun(updateFreq = 1, leadTime=0, name='HA')
    # forecastRunList = [DA, HA]
    # forecast_run_names = [forecast_run.name for forecast_run in forecastRunList]

    # cr.wind_speed_forecast_errors(
    #     MET_update_f=1,  # in hours, IF < 4 -> THE CODE CAN BE INEFFICIENT
    #     rnd_seed=1,
    #     forecastRunList=forecastRunList,  # Lists of forecast runs
    #     )

    # for fcr_name in forecast_run_names:
    #     # conversion to power
    #     df_out = cr.run_power(
    #         case='forecast',
    #         forecast_run_name=fcr_name,
    #         )

    #     # save outputs as csv
    #     save_vars(
    #         df_var = df_out,
    #         out_dir = out_dir,
    #         sufix = '_'+fcr_name,
    #     )

    # client.close()
    # clean('dask-worker-space')
    clean(cr.temp_dir)

    end = time.time()
    print(f"CorRES is done: exec. time {end - start:.1f} [s]")
