#!/opt/conda/bin/python3.11

import argparse
from pathlib import Path

import yaml

import corres.fluctuations as crfl
import corres.forecasts as fc
from corres.api import Corres, get_dask_client, save_vars
from corres.api_auxiliar_functions import check_simulation_size, read_simulation_inputs


def setup_cfg(input_cfg_file, sim_input, output_cfg_file):
    """Update values in the input_cfg depending on the run configuration

    Parameters
    ----------
    input_cfg_file : str
        Path to input configuration file
    sim_input : dict
        simulation inputs
    output_cfg : str or Path
        Path to save the updated configuration file
    """
    sim_type = sim_input["simulation"]

    with open(input_cfg_file) as f:
        cfg = yaml.safe_load(f)
    sim_cfg = cfg["simulation"][sim_type]

    # Add varWD to simulations with fluctuations
    if sim_input["fluctuations"]:
        if "varWD" not in sim_cfg["vars_xyz"]:
            sim_cfg["vars_xyz"].append("varWD")

    # Add GWA2 scaling if enabled
    if (sim_input["gwa2_scaling"]) & (sim_type != "EXTRACT_ALL_ERA5"):
        sim_cfg["mean_WS_scaler"] = "GWA2"

    # Add extraction variables if enabled
    if "extractions" in sim_input.keys():
        translate_name = {
            "DHI": "varDHI",
            "DNI": "varDNI",
            "Evaporation": "varEVAP",
            "GHI": "varGHI",
            "Runoff": "varRNOF",
            "Snow_depth": "varSNOWD",
            "Sub_Surface_Runoff": "varSSRF",
            "Surface_Runoff": "varSRF",
            "Temperature_2m": "varTAIR",
            "Total_precipitation": "varPERC",
            "Wind_dir": "varWD",
            "Wind_speed": "varWS",
        }

        translated_extractions = {
            translate_name[key]: value
            for key, value in sim_input["extractions"].items()
        }
        sim_input["extractions"] = translated_extractions

        sim_cfg["vars_xy"] = [
            var for var in sim_input["extractions"] if sim_input["extractions"][var]
        ]

    # Add ML_wakes if required
    sim_cfg["ML_wake"] = sim_input["ml_wake"]

    print(output_cfg_file)
    with open(output_cfg_file, "w") as f:
        yaml.dump(cfg, f)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input_fn", help="simulation input yaml file", default="simulation_input.yml"
    )
    parser.add_argument(
        "--parallel",
        default=False,
        action="store_true",
        help="Execute in job in parallel",
    )

    args = parser.parse_args()
    input_fn = args.input_fn

    # Reads inputs and sets defaults if needed
    input_dic = read_simulation_inputs(input_fn)

    # Check limits of simulations
    check_simulation_size(input_dic)

    # Setup configuration file based on inputs from run setup
    run_cfg = Path(input_dic["locs_fn"]).absolute().parent / "config.yml"
    setup_cfg(input_dic["config_fn"], input_dic, run_cfg)

    # Create variables for use in the rest of the script
    config_fn = run_cfg
    locs_fn = input_dic["locs_fn"]
    power_curves_fn = input_dic["power_curves_fn"]
    domain = input_dic["domain"]
    dataset = input_dic["dataset"]
    exec_type = input_dic["exec_type"]
    simulation = input_dic["simulation"]
    start_date = str(input_dic["start_date"])
    end_date = str(input_dic["end_date"])
    group_by = input_dic["group_by"]
    weight_var = input_dic["weight_var"]
    out_dir = input_dic["out_dir"]
    fluctuations = input_dic["fluctuations"]
    forecasts = input_dic["forecasts"]

    # -----------------------------------------------------------------------
    # CorRES code to excecute
    # -----------------------------------------------------------------------

    cr = Corres(
        config_fn=config_fn,
        locs_fn=locs_fn,
        power_curves_fn=power_curves_fn,
        dataset=dataset,
        domain=domain,
        exec_type=exec_type,
        simulation=simulation,
        start_date=start_date,
        end_date=end_date,
        group_by=group_by,
        weight_var=weight_var,
        out_dir=out_dir,
    )

    cr.extract_ts()

    ###############################
    # Wind speed fluctuation model
    ###############################
    if fluctuations:
        # Setup spectral variables
        xgl_params = fluctuations["spectra"]["larsen"]
        xgl_params["f0"] = 1 / (xgl_params["f0"] * 60 * 60)  # h to hz
        spectra = [crfl.XGLarsen_S_f(**xgl_params)]

        if "kaimal" in fluctuations["spectra"]:
            kml_params = fluctuations["spectra"]["kaimal"]
            kml_params["f0"] = 1 / (kml_params["f0"] * 60 * 60)  # h to hz
            spectra.append(crfl.Kaimal_S_f(**kml_params))

        # NOTE: These two are hard coded in the webapp right now, but here for the future
        admittance_method = getattr(
            crfl, fluctuations["admittance"], crfl.Soerensen_circular_plant_Fwf_f
        )()
        coherence_method = getattr(
            crfl, fluctuations["coherence"], crfl.Schlez_coherence_jk_f
        )()

        # Note this is input as a boolean, but can have more than 2 possible inputs
        t_distributed = None if not fluctuations["t_distributed"] else "marginals"

        period_fluct = fluctuations["period_fluct"] * 24 * 3600  # days to seconds

        cr.wind_speed_fluctuations(
            seed=fluctuations["seed"],  # seed number
            dt=fluctuations["dt"],  # [s]
            period_fluct=period_fluct,  # [s]
            spectra=spectra,
            admittance=admittance_method,
            coherence=coherence_method,
            t_distributed=t_distributed,
            df=4,  # df for t-student
            look_up_WS=fluctuations["lookup_ws"],
            std_factor=1,  # std(ws_fluct) scaling factor over ws
        )

        # Start Dask client for parallel excecution of power conversion
        if args.parallel:
            client = get_dask_client(threads_per_worker=1)

        # conversion to power
        df_out = cr.run_power(case="fluct")

        # save outputs as csv
        save_vars(
            df_var=df_out,
            out_dir=out_dir,
        )
    else:
        # Start Dask client for parallel excecution of power conversion
        if args.parallel:
            client = get_dask_client(threads_per_worker=1)

        # conversion to power
        df_out = cr.run_power(case="meso")

        save_vars(
            df_var=df_out,
            out_dir=out_dir,
        )

    ## This should be able to run for every setup
    if forecasts:
        forecastRunList = []
        # Forecast errors
        # Specify update freq and lead times for all forecast runs (DA, HA, etc.)
        for i in range(len(forecasts["name"])):
            forec_run = fc.forecRun(
                updateFreq=forecasts["updateFreq"][i],
                leadTime=forecasts["leadTime"][i],
                name=forecasts["name"][i],
            )
            forecastRunList.append(forec_run)

        forecast_run_names = [forecast_run.name for forecast_run in forecastRunList]

        cr.wind_speed_forecast_errors(
            MET_update_f=forecasts["met_update_freq"],  # [h], IF < 4 CAN BE INEFFICIENT
            rnd_seed=forecasts["seed"],
            forecastRunList=forecastRunList,  # Lists of forecast runs
        )

        for fcr_name in forecast_run_names:
            # Start Dask client for parallel excecution of power conversion
            if args.parallel:
                client = get_dask_client(threads_per_worker=1)
            # conversion to power
            df_out = cr.run_power(
                case="forecast",
                forecast_run_name=fcr_name,
            )

            # save outputs as csv
            save_vars(
                df_var=df_out,
                out_dir=out_dir,
                sufix="_" + fcr_name,
            )
