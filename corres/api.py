#! /usr/bin/env python

# Copyright (C) 2019 DTU Wind

import glob
import os
import time

import dask_jobqueue

# import dask_expr._collection
import numpy as np
import pandas as pd
import utm
import xarray as xr
import yaml
from dask import compute
from dask import dataframe as dd
from dask import delayed
from dask.distributed import Client

import corres
import corres.forecasts as fc
from corres.auxiliar_functions import (
    extract_simulation_requirements,
    flatten_nested_lists,
    get_bin_specific_power,
    get_n_cores,
    get_n_workers_and_memory,
    mkdir,
    offset_timeseries,
    remove_empty,
    replace_elements_in_dic,
    split_in_batch,
    try_n_times,
)
from corres.CSP import computes_generation_CSP, computes_generation_CSP_distributed
from corres.fluctuations import (
    Schlez_coherence_jk_f,
    Soerensen_circular_plant_Fwf_f,
    XGLarsen_S_f,
    extend_fluctuations,
    find_n_power_2,
    find_same_locs,
    get_distance,
    get_H_jk,
    get_time_series,
    look_up_eval_timeseries,
    to_marginal_t_student,
    to_multivariate_t_student,
)
from corres.meso_tools import (
    Meso_extractor,
    apply_interpolation_f,
    apply_interpolation_IDW,
    calculate_ratio_datasets,
    get_ds,
    get_file_to_read,
    get_interpolation_weights,
    project_locations,
    read_projections_zarr,
)
from corres.SPP import (
    computes_generation_SPP,
    computes_generation_SPP_distributed,
    get_altitude,
)
from corres.WPP import computes_generation_WPP, computes_generation_WPP_distributed

pd.options.mode.chained_assignment = None


class Corres(object):
    def __init__(
        self,
        config_fn,
        locs_fn,
        power_curves_fn,
        domain=None,
        dataset="ENTSOE",
        exec_type="local",
        simulation="WPP",
        start_date="2018-01-01",
        end_date="2018-01-31",
        group_by="Country",
        weight_var="Installed_capacity_MW",
        n_batch=10,
        n_batch_locs_interp=100,
        out_dir="./Results/",
    ):
        """
        An object of class Corres handles all simulation settings and routines
        available in CorRES, including interpolation and time-series extraction,
        fluctuation models, forecast uncertainty models and power production.

        Parameters
        ----------

        config_fn: string
            File name with the corres configuration

        locs_fn: string
            File name with the location(s) information

        power_curves_fn: string
            File name with the power curve information.


        """

        print("\n\n\n")
        print("-----------------------------------------------")
        print(f"Executing CorRES version {corres.__version__}")
        print("-----------------------------------------------\n\n\n")

        print("config_fn:", config_fn)
        if isinstance(locs_fn, pd.core.frame.DataFrame):
            print("locs_fn:", locs_fn.head())
        else:
            print("locs_fn:", locs_fn)
        print("domain:", domain)
        print("dataset:", dataset)
        print("exec_type:", exec_type)
        print("simulation:", simulation)
        print("start_date:", start_date)
        print("end_date:", end_date)
        print("group_by:", group_by)
        print("weight_var:", weight_var)
        print("n_batch:", n_batch)
        print("n_batch_locs_interp:", n_batch_locs_interp)

        print("\n\n\n")

        with open(config_fn) as file:
            config = yaml.load(file, Loader=yaml.FullLoader)

        # Read the locations
        if isinstance(locs_fn, pd.core.frame.DataFrame):
            locs = locs_fn.copy()
        else:
            locs = pd.read_excel(locs_fn)
        if "locs_ID" not in locs.columns:
            locs["locs_ID"] = range(len(locs))
        locs.index = locs.locs_ID.values

        if "Name" not in locs.columns:
            locs["Name"] = locs.locs_ID.values

        # make output directory
        out_dir = mkdir(out_dir + "/")

        n_locs_orig = len(locs)
        locs.drop_duplicates(subset=["Name"], keep="first", inplace=True)

        # To solve the problem of running only one plant
        if len(locs) == 1:
            locs.loc[1, :] = locs.loc[0, :]
            locs.loc[1, "locs_ID"] = locs.loc[0, "locs_ID"] + 1
            locs.loc[1, "Name"] = locs.loc[0, "Name"] + "_clone"

        # if plants do not give Hub_height
        var_dict = config["weather_data"][dataset]
        var_z = var_dict["var_z"]
        if var_z not in locs.columns:
            locs[var_z] = 1  # [m] 1 meter above ground

        # extract all needed variables for interpolation and conversion to power
        params_sim = extract_simulation_requirements(config, simulation, dataset)

        # Store modifications in the locs_fn
        if n_locs_orig != len(locs):
            locs_fn = (
                out_dir
                + locs_fn.replace(".xlsx", "_modified_by_CorRES.xlsx").split("/")[-1]
            )
            print(f"\nlocs_fn has duplicated locations. Used file is: \n\n{locs_fn}\n")
            locs.to_excel(locs_fn, index=False)

        if "WPP" in simulation:
            # ------------------------------------
            # get mean_WS_scaler type from config
            # ------------------------------------
            mean_WS_scaler = config["simulation"][simulation]["mean_WS_scaler"]

            if mean_WS_scaler.lower() == "none":
                mean_WS_scaler = None
                var_ratio = None

            elif mean_WS_scaler is not None:
                var_ratio = "ratio_" + mean_WS_scaler + "_" + dataset + "_mean_WS"

                locs = calculate_ratio_datasets(
                    locs, config, dataset, mean_WS_scaler, exec_type
                )

                locs_fn = (
                    out_dir
                    + locs_fn.replace(".xlsx", "_modified_by_CorRES.xlsx").split("/")[
                        -1
                    ]
                )
                print(
                    f"\nlocs_fn does not include WS scaling variable ({var_ratio})."
                    + f"\nValues have been extracted and are available in:"
                    + f"\n\n{locs_fn}\n"
                )
                locs.to_excel(locs_fn, index=True)

            params_sim["var_ratio"] = var_ratio

            # -----------------------------------------
            # create power_curve_file if generic farm
            # -----------------------------------------
            ML_wake: bool = config["simulation"][simulation]["ML_wake"]

            if ML_wake:
                ML_wake_vars_needed = list(config["generic_farm_vars"].values())
                # these can be left out to be filled with default values
                var_TI = get_safe_from_cfg(config, ["generic_farm_vars", "var_TI"])
                if var_TI is not None:
                    ML_wake_vars_needed.remove(var_TI)
                var_rho = get_safe_from_cfg(config, ["generic_farm_vars", "var_rho"])
                if var_rho is not None:
                    ML_wake_vars_needed.remove(
                        get_safe_from_cfg(config, ["generic_farm_vars", "var_rho"])
                    )
                if not all([var in locs.columns for var in ML_wake_vars_needed]):
                    raise ValueError(
                        "Not all columns needed for applying ML_wake are in locs_fn:\n"
                        + f"{ML_wake_vars_needed}"
                    )

                # to solve problems with plants with zero turbines
                locs = locs.loc[locs[config["generic_farm_vars"]["var_Nwt"]] > 0, :]

                if "power_regulation" not in locs.columns:
                    locs["power_regulation"] = ["Pitch"] * len(locs)

                    if "modified_by_CorRES" not in locs_fn:
                        locs_fn = (
                            out_dir
                            + locs_fn.replace(
                                ".xlsx", "_modified_by_CorRES.xlsx"
                            ).split("/")[-1]
                        )
                    locs.to_excel(locs_fn, index=True)
                    print(
                        f"\nlocs_fn has been modified by adding a power_regulation column (all set to pitch)"
                        + f"\nThe new file is available in:"
                        f"\nlocs_fn: {locs_fn} "
                    )

                if ("Stall" in list(locs["power_regulation"])) & (
                    power_curves_fn is not None
                ):
                    locs["power_regulation"] = locs["power_regulation"].str.lower()
                    stall_mask = locs["power_regulation"] == "stall"
                    pitch_mask = locs["power_regulation"] == "pitch"

                    stall_locs = locs.loc[stall_mask]
                    pitch_locs = locs.loc[pitch_mask]

                    pc_df_pitch, locs_pitch = get_power_curves_gen_wf(
                        pitch_locs,
                        WL_ML_fn=config["weather_data"][dataset][exec_type]["WL_ML_fn"],
                        vars_dic=config["generic_farm_vars"],
                    )

                    pc_df_stall, locs_stall = get_frequent_power_curves_stall(
                        stall_locs,
                        power_curves_fn,
                        int(pc_df_pitch.iloc[len(pc_df_pitch) - 1]["Power_curve"]),
                    )

                    # keep in the stall power curves the same column names (wind speeds) with pitch power curves
                    pc_pitch_ws = list(pc_df_pitch.columns)
                    pc_stall_ws = list(pc_df_stall.columns)
                    if (
                        pc_pitch_ws[len(pc_pitch_ws) - 1]
                        > pc_stall_ws[len(pc_stall_ws) - 1]
                    ):
                        pc_df_stall[pc_pitch_ws[len(pc_pitch_ws) - 1]] = pc_df_stall[
                            pc_stall_ws[len(pc_stall_ws) - 1]
                        ]
                        pc_df_stall = pc_df_stall[pc_pitch_ws]

                    pc_df = pd.concat(
                        [pc_df_pitch, pc_df_stall], ignore_index=False, sort=False
                    )
                    locs = pd.concat(
                        [locs_pitch, locs_stall], ignore_index=False, sort=False
                    )

                else:
                    pc_df, locs = get_power_curves_gen_wf(
                        locs,
                        WL_ML_fn=config["weather_data"][dataset][exec_type]["WL_ML_fn"],
                        vars_dic=config["generic_farm_vars"],
                    )

                if "modified_by_CorRES" not in locs_fn:
                    locs_fn = (
                        out_dir
                        + locs_fn.replace(".xlsx", "_modified_by_CorRES.xlsx").split(
                            "/"
                        )[-1]
                    )
                if power_curves_fn is None:
                    power_curves_fn = out_dir + "generic_power_curves_by_CorRES.xlsx"
                else:
                    power_curves_fn = (
                        out_dir
                        + power_curves_fn.replace(
                            ".xlsx", "_modified_by_CorRES.xlsx"
                        ).split("/")[-1]
                    )

                print(
                    f"\nlocs_fn has been modified by adding a Power_curve column"
                    + f"\nthat identifies the power curve used and are available in:"
                    + f"\n\nlocs_fn: {locs_fn} "
                    + f"\n\npower_curves_fn: {power_curves_fn} "
                )
                locs.to_excel(locs_fn, index=True)

                pc_df[100] = pc_df[25]
                #
                pc_df.to_excel(power_curves_fn, index=False)

                use_extended_table = get_safe_from_cfg(
                    config, ["generic_farm", "use_extended_table"]
                )
                if use_extended_table:
                    pc_ds = get_power_curves_gen_wf_extended(
                        locs,
                        WL_ML_fn=config["weather_data"][dataset][exec_type]["WL_ML_fn"],
                        vars_dic=config["generic_farm_vars"],
                        default_TI=(
                            config.get("generic_farm", {})
                            .get("defaults", {})
                            .get("TI", 0.1)
                        ),
                        default_rho=(
                            config.get("generic_farm", {})
                            .get("defaults", {})
                            .get("rho", 1.225)
                        ),
                    )
                    self.ext_ML_table = pc_ds

        elif ("SPP" in simulation) | ("CSP" in simulation):

            # check if Altitude is available in the columns otherwise compute
            if "Altitude" in locs.columns:
                pass
            else:
                elev_ds = xr.open_dataset(
                    config["weather_data"][dataset][exec_type]["elevation"]
                )
                locs_xr = locs.loc[:, ["Latitude", "Longitude"]].to_xarray()

                locs["Altitude"] = (
                    elev_ds["elev"]
                    .interp(
                        latitude=locs_xr.Latitude,
                        longitude=locs_xr.Longitude,
                        kwargs={"fill_value": 0.0},
                    )
                    .values
                )

                locs_fn = (
                    out_dir
                    + locs_fn.replace(".xlsx", "_modified_by_CorRES.xlsx").split("/")[
                        -1
                    ]
                )
                print(
                    f"\nlocs_fn does not include Altitude column"
                    + f"\nValues have been interpolated and are available in "
                    + f"{locs_fn}\n"
                )
                locs.to_excel(locs_fn, index=False)

        # group by
        if (group_by is None) | (group_by == "Name"):
            group_by = "Name_ID"
            weight_var = "Installed_capacity_MW"

            locs["Name_ID"] = locs["Name"]
            if "Installed_capacity_MW" not in locs.columns:
                locs["Installed_capacity_MW"] = 1

        if weight_var is None:
            weight_var = "Installed_capacity_MW"

        self.config_fn = config_fn
        self.locs = locs
        self.power_curves_fn = power_curves_fn
        self.domain = domain
        self.dataset = dataset
        self.exec_type = exec_type
        self.simulation = simulation
        self.start_date = start_date
        self.end_date = end_date
        self.group_by = group_by
        self.weight_var = weight_var
        self.n_batch = n_batch
        self.n_batch_locs_interp = n_batch_locs_interp
        self.config = config
        self.params_sim = params_sim
        self.out_dir = out_dir
        self.temp_dir = mkdir(f"{out_dir}temp/")

    def extract_ts(self):
        """
        Method to extract timeseries at all the locations.
        Names of ranalysis dataset, variable names and interpolation method (e.x.
        linear vs logaritmic in vertical) or order can be modified by changing the
        config_fn file.
        """

        print(f"\nExecuting reanalysis interpolation:")

        config_fn = self.config_fn
        locs = self.locs
        power_curves_fn = self.power_curves_fn
        domain = self.domain
        dataset = self.dataset
        exec_type = self.exec_type
        simulation = self.simulation
        start_date = self.start_date
        end_date = self.end_date
        group_by = self.group_by
        weight_var = self.weight_var
        n_batch = self.n_batch
        n_batch_locs_interp = self.n_batch_locs_interp
        config = self.config
        params_sim = self.params_sim
        out_dir = self.out_dir
        temp_dir = self.temp_dir

        # get variable for interpolation based on the config
        n_stencil = config["interpolation"]["n_stencil"]

        # variables in loc_fn to used for interpolation
        var_x = params_sim["var_x"]
        var_y = params_sim["var_y"]
        var_z = params_sim["var_z"]
        # grid variable names
        var_x_grid = params_sim["var_x_grid"]
        var_y_grid = params_sim["var_y_grid"]
        var_z_grid = params_sim["var_z_grid"]
        var_grid = params_sim["var_grid"]
        # vars in the weather dataset
        varWS = params_sim["varWS"]
        varWD = params_sim["varWD"]
        varGHI = params_sim["varGHI"]
        varDNI = params_sim["varDNI"]
        varDHI = params_sim["varDHI"]
        varRHO = params_sim["varRHO"]
        varTAIR = params_sim["varTAIR"]
        varRMOL = params_sim["varRMOL"]
        varUST = params_sim["varUST"]
        varEVAP = params_sim["varEVAP"]
        varRNOF = params_sim["varRNOF"]
        varSNOWD = params_sim["varSNOWD"]
        varSSRF = params_sim["varSSRF"]
        varSRF = params_sim["varSRF"]
        varPERC = params_sim["varPERC"]

        vars_xy_logz = params_sim["vars_xy_logz"]
        vars_xyz = params_sim["vars_xyz"]
        vars_xy = params_sim["vars_xy"]
        vars_nearest_xy = params_sim["vars_nearest_xy"]
        vars_nearest_xyz = params_sim["vars_nearest_xyz"]
        vars_needed = params_sim["vars_needed"]

        if dataset == "ERA5" or dataset == "ERA5_comsmo":
            # get paths
            meso_dir = params_sim[exec_type]["meso_dir"]

            ds = xr.open_zarr(meso_dir)
            # To avoid problems with duplicated days
            _, index = np.unique(ds.time.to_dataframe().time, return_index=True)
            ds = ds.isel(time=index)
            ds = ds.sel(time=slice(start_date, end_date))
            locs_new = locs

            if (
                simulation == "SPP"
                or simulation == "CSP"
                or simulation == "EXTRACT_ALL_ERA5"
            ):
                dni_dir = params_sim[exec_type]["dni_dir"]
                print(dni_dir)
                ds_dni = xr.open_zarr(dni_dir)
                ds_dni = ds_dni.sortby("longitude")
                ds_dni = ds_dni.sortby("latitude")
                ds_dni["height"] = [0.001]
                _, index = np.unique(ds_dni.time.to_dataframe().time, return_index=True)
                ds_dni = ds_dni.isel(time=index)
                ds_dni = ds_dni.sel(time=slice(start_date, end_date))

            if simulation == "EXTRACT_ALL_ERA5":
                hydro_dir = params_sim[exec_type]["hydro_dir"]
                print(hydro_dir)
                ds_hydro = xr.open_zarr(hydro_dir)
                ds_hydro = ds_hydro.sortby("longitude")
                ds_hydro = ds_hydro.sortby("latitude")
                ds_hydro["height"] = [0.001]
                _, index = np.unique(
                    ds_hydro.time.to_dataframe().time, return_index=True
                )
                ds_hydro = ds_hydro.isel(time=index)
                ds_hydro = ds_hydro.sel(time=slice(start_date, end_date))

        elif (
            dataset == "CORDEX_CNRM_CERFACS_CNRM_CM5_ALADIN63_hist"
            or dataset == "CORDEX_CNRM_CERFACS_CNRM_CM5_ALADIN63_rcp26"
            or dataset == "CORDEX_CNRM_CERFACS_CNRM_CM5_ALADIN63_rcp85"
            or dataset == "CORDEX_MOHC_HadGEM2_ES_ALADIN63_hist"
            or dataset == "CORDEX_MOHC_HadGEM2_ES_ALADIN63_rcp85"
            or dataset == "CORDEX_NCC_NorESM1_M_ALADIN63_hist"
            or dataset == "CORDEX_NCC_NorESM1_M_ALADIN63_rcp85"
            or dataset == "CORDEX_CNRM_CERFACS_CNRM_CM5_RegCM4_6_hist"
            or dataset == "CORDEX_CNRM_CERFACS_CNRM_CM5_RegCM4_6_rcp85"
            or dataset == "CORDEX_ICHEC_EC_EARTH_RegCM4_6_hist"
            or dataset == "CORDEX_ICHEC_EC_EARTH_RegCM4_6_rcp85"
            or dataset == "CORDEX_MPI_M_MPI_ESM_LR_RegCM4_6_hist"
            or dataset == "CORDEX_MPI_M_MPI_ESM_LR_RegCM4_6_rcp26"
            or dataset == "CORDEX_MPI_M_MPI_ESM_LR_RegCM4_6_rcp85"
            or dataset == "CORDEX_NCC_NorESM1_M_RegCM4_6_hist"
            or dataset == "CORDEX_NCC_NorESM1_M_RegCM4_6_rcp26"
            or dataset == "CORDEX_NCC_NorESM1_M_RegCM4_6_rcp85"
        ):
            # get paths
            meso_dir = params_sim[exec_type]["meso_dir"]
            crs_proj = params_sim[exec_type]["crs_proj"]
            ds = xr.open_zarr(meso_dir)
            # ds = xr.open_dataset(meso_dir)
            # To avoid problems with duplicated days
            _, index = np.unique(ds.time.to_dataframe().time, return_index=True)
            ds = ds.isel(time=index)
            ds = ds.sel(time=slice(start_date, end_date))
            # locs_new = locs
            locs_new = locs.copy()
            # project the locations (lat, lon) into WRF coordinates (x,y)
            import geopandas as gpd
            import pyproj

            crs = pyproj.CRS(crs_proj)
            locs_gpd = gpd.GeoDataFrame(
                locs,
                geometry=gpd.points_from_xy(locs.Longitude, locs.Latitude),
                crs="EPSG:4326",
            )
            locs_gpd_new = locs_gpd.to_crs(crs=crs_proj)
            locs_new["x"] = locs_gpd_new.geometry.x.values
            locs_new["y"] = locs_gpd_new.geometry.y.values

        elif dataset == "NEWA":
            # get paths
            meso_dir = params_sim[exec_type]["meso_dir"]
            crs_proj = params_sim[exec_type]["crs_proj"]

            ds = xr.open_zarr(meso_dir)
            ds = ds.sel(time=slice(start_date, end_date))

            locs_new = locs.copy()
            # project the locations (lat, lon) into WRF coordinates (x,y)
            import geopandas as gpd
            import pyproj

            crs = pyproj.CRS(crs_proj)
            locs_gpd = gpd.GeoDataFrame(
                locs,
                geometry=gpd.points_from_xy(locs.Longitude, locs.Latitude),
                crs="EPSG:4326",
            )
            locs_gpd_new = locs_gpd.to_crs(crs=crs_proj)
            locs_new["x"] = locs_gpd_new.geometry.x.values
            locs_new["y"] = locs_gpd_new.geometry.y.values

        elif dataset == "ENTSOE":
            # get paths
            meso_dir = params_sim[exec_type]["meso_dir"].replace("{domain}", domain)
            region_domain_fn = params_sim[exec_type]["region_domain_fn"]

            ds = xr.open_zarr(meso_dir)
            ds = ds.sel(time=slice(start_date, end_date))

            # project the locations (lat, lon) into WRF coordinates (x,y)
            locs_new = project_locations(
                locs=locs, region_domain_fn=region_domain_fn, ds=ds, domain=domain
            )

        elif (
            (dataset == "CORDEX_PECD25_ICHEC_EC_EARTH_KNMI_RACMO22E_rcp45")
            | (dataset == "CORDEX_PECD25_EUCX_SMHI_ELRA_rcp45")
            | (dataset == "CORDEX_P_EUCX_SMHI_HGRA_rcp45")
            | (dataset == "CORDEX_CMPI6")
            | (dataset == "CORDEX_CMR5")
            | (dataset == "CORDEX_ECE3")
            | (dataset == "CORDEX_MEHR")
            | (dataset == "CORDEX_ECEC")
            | (dataset == "CORDEX_CMCC")
            | (dataset == "CORDEX_MPI")
            | (dataset == "CORDEX_CMR5_SP126")
            | (dataset == "CORDEX_CMR5_SP370")
            | (dataset == "CORDEX_CMR5_SP585")
            | (dataset == "CORDEX_MEHR_SP126")
            | (dataset == "CORDEX_MEHR_SP370")
            | (dataset == "CORDEX_MEHR_SP585")
            | (dataset == "CORDEX_ECE3_SP126")
            | (dataset == "CORDEX_ECE3_SP370")
            | (dataset == "CORDEX_ECE3_SP585")
        ):
            # get paths
            meso_dir = params_sim[exec_type]["meso_dir"]

            ds = xr.open_zarr(meso_dir)
            # To avoid problems with duplicated days
            _, index = np.unique(ds.time.to_dataframe().time, return_index=True)
            ds = ds.isel(time=index)
            ds = ds.sel(time=slice(start_date, end_date))
            locs_new = locs

            if simulation == "SPP" or simulation == "CSP":
                dni_dir = params_sim[exec_type]["dni_dir"]
                ds_dni = xr.open_zarr(dni_dir)
                ds_dni["height"] = [0.001]
                _, index = np.unique(ds_dni.time.to_dataframe().time, return_index=True)
                ds_dni = ds_dni.isel(time=index)
                ds_dni = ds_dni.sel(time=slice(start_date, end_date))

        else:
            print(f"\n\nWrong dataset: {dataset}")
            return

        batches = split_in_batch(locs_new.index.values, n_batch_locs_interp)
        Nb = len(batches)

        hydro_vars = [varEVAP, varPERC, varRNOF, varSNOWD, varSRF, varSSRF]
        solar_vars = [varDNI, varDHI, varGHI]
        wind_vars = [varWS, varWD, varTAIR, varRMOL, varUST, varRHO]

        vars_xy_interp = vars_xy
        vars_xy_interp = list(set(vars_xy) - set(solar_vars))
        vars_xy_interp = list(set(vars_xy_interp) - set(hydro_vars))
        vars_nearest_xy_interp = list(set(vars_nearest_xy) - set([varDNI]))
        if simulation == "SPP":
            vars_xy_interp.append("T2")

        if vars_xy_logz is None:
            vars_xy_logz = []

        for ib, batch in enumerate(batches):
            start = time.time()
            locs_sel = locs_new.loc[batch, :].copy()

            # weights for interpolation
            weights_ds = get_interpolation_weights(
                px=locs_sel[var_x].values,
                py=locs_sel[var_y].values,
                pz=locs_sel[var_z].values,
                all_x=ds[var_x_grid].values,
                all_y=ds[var_y_grid].values,
                all_z=ds[var_z_grid].values,
                n_stencil=n_stencil,
                locs_ID=locs_sel.locs_ID.values,
            )

            # Apply interpolation
            ds_interp = apply_interpolation_f(
                wrf_ds=ds,
                weights_ds=weights_ds,
                vars_xy_logz=vars_xy_logz,
                vars_xyz=vars_xyz,
                vars_xy=vars_xy_interp,
                vars_nearest_xy=vars_nearest_xy_interp,
                vars_nearest_xyz=vars_nearest_xyz,
                var_x_grid=var_x_grid,
                var_y_grid=var_y_grid,
                var_z_grid=var_z_grid,
                varWD=varWD,
            )

            # Use ERA5 land for solar simulations in ERA5
            if (
                dataset == "ERA5"
                or dataset == "ERA5_comsmo"
                or dataset == "CORDEX_PECD25_ICHEC_EC_EARTH_KNMI_RACMO22E_rcp45"
                or dataset == "CORDEX_PECD25_EUCX_SMHI_ELRA_rcp45"
                or dataset == "CORDEX_P_EUCX_SMHI_HGRA_rcp45"
                or dataset == "CORDEX_CMR5"
                or dataset == "CORDEX_ECE3"
                or dataset == "CORDEX_MEHR"
                or dataset == "CORDEX_ECEC"
                or dataset == "CORDEX_CMCC"
                or dataset == "CORDEX_MPI"
                or dataset == "CORDEX_CMR5_SP126"
                or (dataset == "CORDEX_CMR5_SP370")
                or (dataset == "CORDEX_CMR5_SP585")
                or (dataset == "CORDEX_MEHR_SP126")
                or (dataset == "CORDEX_MEHR_SP370")
                or (dataset == "CORDEX_MEHR_SP585")
                or (dataset == "CORDEX_ECE3_SP126")
                or (dataset == "CORDEX_ECE3_SP370")
                or (dataset == "CORDEX_ECE3_SP585")
            ) & ((simulation == "SPP") | (simulation == "CSP")):

                ds_interp_2 = apply_interpolation_f(
                    wrf_ds=ds_dni,
                    weights_ds=weights_ds,
                    vars_xy_logz=[],
                    vars_xyz=vars_xyz,
                    vars_xy=[varDNI],
                    vars_nearest_xy=vars_nearest_xy_interp,
                    vars_nearest_xyz=vars_nearest_xyz,
                    var_x_grid=var_x_grid,
                    var_y_grid=var_y_grid,
                    var_z_grid=var_z_grid,
                    varWD=varWD,
                )

                ds_interp_2[varDNI] = ds_interp_2[varDNI].transpose("locs_ID", "time")

                if simulation == "SPP":
                    ds_interp_temp = apply_interpolation_f(
                        wrf_ds=ds_dni,
                        weights_ds=weights_ds,
                        vars_xy_logz=[],
                        vars_xyz=vars_xyz,
                        vars_xy=[varDHI, varGHI],
                        vars_nearest_xy=vars_nearest_xy_interp,
                        vars_nearest_xyz=vars_nearest_xyz,
                        var_x_grid=var_x_grid,
                        var_y_grid=var_y_grid,
                        var_z_grid=var_z_grid,
                        varWD=varWD,
                    )

                    ds_interp_temp["GHI"] = ds_interp_temp["GHI"].transpose(
                        "locs_ID", "time"
                    )
                    ds_interp_temp["DHI"] = ds_interp_temp["DHI"].transpose(
                        "locs_ID", "time"
                    )

                    ds_interp_2 = xr.merge([ds_interp_2, ds_interp_temp])

                ds_interp_2["locs_ID"] = locs_sel.locs_ID.values
                ds_interp[varDNI] = ds_interp_2[varDNI]
                if simulation == "SPP":
                    for irr_var in [varGHI, varDHI]:
                        ds_interp[irr_var] = ds_interp_2[irr_var]

            if (dataset == "ERA5") & (simulation == "EXTRACT_ALL_ERA5"):
                ds_interp_2 = apply_interpolation_f(
                    wrf_ds=ds_hydro,
                    weights_ds=weights_ds,
                    vars_xy_logz=[],
                    vars_xyz=[],
                    vars_xy=list(set(vars_xy) - set(solar_vars) - set(wind_vars)),
                    vars_nearest_xy=vars_nearest_xy_interp,
                    vars_nearest_xyz=vars_nearest_xyz,
                    var_x_grid=var_x_grid,
                    var_y_grid=var_y_grid,
                    var_z_grid=var_z_grid,
                    varWD=varWD,
                )

                ds_interp_3 = apply_interpolation_f(
                    wrf_ds=ds_dni,
                    weights_ds=weights_ds,
                    vars_xy_logz=[],
                    vars_xyz=[],
                    vars_xy=list(set(vars_xy) - set(hydro_vars) - set(wind_vars)),
                    vars_nearest_xy=vars_nearest_xy_interp,
                    vars_nearest_xyz=vars_nearest_xyz,
                    var_x_grid=var_x_grid,
                    var_y_grid=var_y_grid,
                    var_z_grid=var_z_grid,
                    varWD=varWD,
                )
                if "height" in ds_interp.coords:
                    ds_interp = ds_interp.sel(height=10)
                    ds_interp = ds_interp.drop_vars("height")

                for hydro_var in list(set(vars_xy) - set(solar_vars) - set(wind_vars)):
                    ds_interp[hydro_var] = ds_interp_2[hydro_var]

                for solar_var in list(set(vars_xy) - set(hydro_vars) - set(wind_vars)):
                    ds_interp[solar_var] = ds_interp_3[solar_var]

            ds_interp["locs_ID"] = locs_sel.locs_ID.values

            # Apply WS scaling and high wind speed correction
            if (varWS in vars_needed) & (simulation != "SPP"):
                # Scale the wind speed with the varratio extracted in the
                # __init__

                if isinstance(
                    config["simulation"][simulation]["mean_WS_scaler"], str
                ):  # fixing probelm with " config['simulation'][simulation]['mean_WS_scaler'] " being None and "None" in different batches
                    if (
                        config["simulation"][simulation]["mean_WS_scaler"].lower()
                        == "none"
                    ):
                        config["simulation"][simulation]["mean_WS_scaler"] = None

                if config["simulation"][simulation]["mean_WS_scaler"] is not None:
                    ds_interp[varWS] = (
                        ds_interp[varWS] * locs_sel[params_sim["var_ratio"]].values
                    )
                # High wind speed correction
                if config["simulation"][simulation]["hi_wind_scaling"] is not None:
                    # Parameters [ws_lim_start, ws_lim_end, scale_percent] are used to scale high wind speeds.
                    # Wind speeds > ws_lim_end are multiplied with 1+scale_percent.
                    # Wind speeds < ws_lim_start are not modified.
                    # Wind speeds between ws_lim_start and ws_lim_end are modified to create a linear increase
                    # from 0 percent increase at ws_lim_start to scale_percent
                    # increase at ws_lim_end.

                    ws_lim_start = config["simulation"][simulation]["hi_wind_scaling"][
                        0
                    ]
                    ws_lim_end = config["simulation"][simulation]["hi_wind_scaling"][1]
                    scale_percent = config["simulation"][simulation]["hi_wind_scaling"][
                        2
                    ]

                    ws_hw = [0, ws_lim_start, ws_lim_end, 1000]
                    ratio_hw = [1, 1, 1 + scale_percent, 1 + scale_percent]

                    ds_factor = xr.Dataset(
                        data_vars={
                            "ws_ratio": (
                                ["time", "locs_ID"],
                                np.interp(xp=ws_hw, fp=ratio_hw, x=ds_interp[varWS]),
                                {"description": "high wind speed ratio"},
                            ),
                        },
                        coords={
                            "time": ds_interp.time.values,
                            "locs_ID": ds_interp.locs_ID.values,
                        },
                    )

                    ds_interp[varWS] = ds_interp[varWS] * ds_factor.ws_ratio

            # Store meso dataset
            ds_interp_fn = f"{temp_dir}ds_meso_{ib:03}.nc"
            ds_interp.to_netcdf(ds_interp_fn, mode="w")
            end = time.time()
            print(f"{ib+1} batch out of {Nb}, exec. time:", end - start)

        return None

    def wind_speed_fluctuations(
        self,
        dt,  # [s]
        look_up_WS=False,
        t_distributed=None,
        spectra=[XGLarsen_S_f()],
        admittance=Soerensen_circular_plant_Fwf_f(),
        coherence=Schlez_coherence_jk_f(),
        period_fluct=2 * 365 * 24 * 60 * 60,  # [s]
        df=4,  # df for t-student
        seed=0,  # seed number
        std_factor=1,
        # std(ws_fluct) scaling factor over ws
    ):
        """
        Method to extract timeseries at all the locations.
        Names of ranalysis dataset, variable names and interpolation method (e.x.
        linear vs logaritmic in vertical) or order can be modified by changing the
        config_fn file.

        Parameters
        ----------

        dt: float
            Time step in the simulation in seconds

        look_up_WS: Boolean
            Flag to select model of evaluation of cross-spectra and fluctuations:
            (False) Single evaluation of cross-spectra at mean_WS and mean_WD
            (True)  Look-up table of evaluation of cross-spectra for WS in [4,8,16,50]
                    and mean_WD, interpolation is based on local ws per time.

        t_distributed: string
            Flag to apply transformation to t-distributed fluctuations
            (None)         Gaussian fluctuations
            ('marginals')  t-distributed marginal
                           (note: the resulting fluctuations are limited between -10 and 13 m/s)
            ('multidim-t') multi-dimensional t-student distribution

            # TODO add dof

        spectra: List of spectrum objects
            The spectra will be summed S_out(f) = Sum_i Si(f)
            Default [XGLarsen_S_f()]

        admittance: Admittance object
            Admittance object. S_out(f) = F(f) * S(f)
            Default: None

        coherence: Coherence object
            Coherence object.
            Default: Schlez_coherence_jk_f() which is the Schlez_coherence_jk_f function
            with the coherence_coeff_func=Schlez_coherence_coeff_Soerensen().
            Schlez_coherence_jk_f(coherence_coeff_func=Schlez_coherence_coeff_Soerensen())

        period_fluct: int
            Length of the periodicity of fluctuations. The length of the timeseries will
            be constructed by repeating the fluctuations.
        """

        print(f"\nExecuting wind speed fluctuations:")

        config_fn = self.config_fn
        locs = self.locs.copy()
        power_curves_fn = self.power_curves_fn
        domain = self.domain
        dataset = self.dataset
        exec_type = self.exec_type
        simulation = self.simulation
        start_date = self.start_date
        end_date = self.end_date
        group_by = self.group_by
        weight_var = self.weight_var
        n_batch = self.n_batch
        n_batch_locs_interp = self.n_batch_locs_interp
        config = self.config
        params_sim = self.params_sim
        out_dir = self.out_dir
        temp_dir = self.temp_dir

        # vars in the weather dataset
        varWS = params_sim["varWS"]
        start = time.time()

        # load the meso time series
        file_list = sorted(glob.glob(temp_dir + "ds_meso_*.nc"))
        if len(file_list) > 0:
            ds = xr.open_mfdataset(file_list, combine="nested", concat_dim="locs_ID")
        else:
            ds = xr.open_dataset(file_list[0])

        # Compute the time range of the final time series
        ts = pd.date_range(
            start=start_date, end=end_date + " 23:59:59", freq=f"{dt/60}min"
        )
        nt = len(ts)

        # interpolate the meso dataset
        ds_interp = ds.interp(time=ts, kwargs={"fill_value": "extrapolate"})

        # compute distance matrix
        x, y, utm_zone, utm_letter = utm.from_latlon(
            locs.Latitude.values, locs.Longitude.values
        )
        locs["x"] = x
        locs["y"] = y

        if "Area_km2" not in locs.columns:
            locs["Area_km2"] = locs.Installed_capacity_MW / 7  # with 7 MW/mk2
        locs["WF_D_km"] = 2 * np.sqrt(locs.Area_km2 / np.pi) * 1000.0  # [m]

        # To remove duplicate locations to solve fluctuations equations
        rows_droped = locs.loc[
            locs.duplicated(subset=["Latitude", "Longitude"], keep="first")
        ]
        locs_red = locs.drop_duplicates(subset=["Latitude", "Longitude"], keep="first")
        duplicate_locs = find_same_locs(rows_droped, locs_red)

        # compute mean wind direction
        tot_MW = locs.Installed_capacity_MW.sum()
        mean_WD = np.arctan2(
            (
                np.sin(np.radians(ds_interp.WD)).mean("time")
                * locs.Installed_capacity_MW
            ).sum("locs_ID")
            / tot_MW,
            (
                np.cos(np.radians(ds_interp.WD)).mean("time")
                * locs.Installed_capacity_MW
            ).sum("locs_ID")
            / tot_MW,
        )
        mean_WD = float(np.mod(np.rad2deg(compute(mean_WD)[0].values), 360))

        n = find_n_power_2(period_fluct / dt)
        timestep = dt  # [s]
        fs = 1 / timestep  # sampling freq

        if look_up_WS:
            mean_WS = (ds_interp.WS.mean("time") * locs.Installed_capacity_MW).sum(
                "locs_ID"
            ) / tot_MW
            mean_WS = float(compute(mean_WS)[0])

            ws = mean_WS
            wd = mean_WD

            ws_fluct = look_up_eval_timeseries(
                wd=wd,
                n=n,
                fs=fs,
                nt=nt,
                seed=seed,
                std_factor=std_factor,
                duplicate_locs=duplicate_locs,
                locs_red=locs_red,
                spectra=spectra,
                admittance=admittance,
                coherence=coherence,
                ds_interp=ds_interp,
            )

        else:
            mean_WS = (ds_interp.WS.mean("time") * locs.Installed_capacity_MW).sum(
                "locs_ID"
            ) / tot_MW
            mean_WS = float(compute(mean_WS)[0])

            ws = mean_WS
            wd = mean_WD
            H_jk, freq, var, S_jk, S = get_H_jk(
                ws=ws,
                wd=wd,
                n=n,
                fs=fs,
                locs_df=locs_red,
                spectra=spectra,
                admittance=admittance,
                coherence=coherence,
                return_all=True,
            )

            V_t = get_time_series(H_jk, freq, var, n, seed=seed)

            if t_distributed == "multidim-t":
                Vt_t = to_multivariate_t_student(V_t, df=df)
            elif t_distributed == "marginals":
                Vt_t = to_marginal_t_student(V_t, df=df, lim_low=-10, lim_high=13)
            else:
                Vt_t = V_t

            ws_fluct = extend_fluctuations(Vt_t, nt, duplicate_locs, locs_red)

        ds_interp[varWS + "_fluc"] = xr.DataArray(
            dims=["time", "locs_ID"], data=ws_fluct
        )

        ds_interp[varWS + "_interp"] = ds_interp[varWS]
        ds_interp[varWS] = ds_interp[varWS + "_interp"] + ds_interp[varWS + "_fluc"]
        ds_interp.where(ds_interp[varWS] < 0)[varWS] = 0
        ds_interp[varWS] = ds_interp[varWS].where(ds_interp[varWS] > 0)
        ds_interp[varWS] = ds_interp[varWS].fillna(0.0)

        # Store meso dataset
        ds_interp_fn = f"{temp_dir}ds_fluct.nc"
        ds_interp.to_netcdf(ds_interp_fn, mode="w")
        end = time.time()
        print(f"exec. time:", end - start)

        return None

    def wind_speed_forecast_errors(
        self,
        MET_update_f=1,  # in hours, IF < 4 -> THE CODE CAN BE INEFFICIENT
        rnd_seed=1,
        forecastRunList=[],  # Lists of forecast runs
    ):
        """
        Method to generate wind speed forecast errors.

        Parameters
        ----------

        dt: float
            Time step in the simulation in seconds

        """

        print(f"\nExecuting wind speed forecast error simulation:")

        # Specify the forecast simulation model
        # for now, always a multivariate ARMA(1,1)
        ARMA = fc.ARMA11_wind_forecSim(MET_update_freq=MET_update_f, seed=rnd_seed)
        print(ARMA)

        # Specify update freq and lead times for all forecast runs (DA, HA,
        # etc.)
        for forecastRun in forecastRunList:
            print(forecastRun)
            ARMA.add_forecRun(forecastRun)

        # Give WPP information to the forecast sim model
        ARMA.set_locs(self.locs.copy())

        # Load meso data
        print("Loading the CorRES meso ws and wd data")
        ARMA.load_mesoTimeser(self.temp_dir)

        out_dir = mkdir(self.out_dir + "/Forecasts/")

        # Get ready to run: Set up to forecast update times
        print("Setting up the run...")
        t_count = time.time()
        ARMA.set_forecUpdateTimes()
        elapsed = time.time() - t_count
        print("Done: took: " + str(elapsed))

        # Run all years
        print("Running the forecar err simulation...")
        t_count = time.time()
        ARMA.run_all()
        elapsed = time.time() - t_count
        print("Done: took: " + str(elapsed))

        # Save result to files
        print("Saving results to files (NetCDF)...")
        t_count = time.time()
        ARMA.save_to_netcdf(self.temp_dir)
        # ARMA.save_to_csv(out_dir)
        elapsed = time.time() - t_count
        print("Done: took: " + str(elapsed))

    def run_power(
        self,
        case="meso",  # 'fluct', or 'forecast'
        forecast_run_name=None,
    ):
        """
        Method to transform resources to power time-series.
        """

        print(f"\nExecuting power conversion:")

        config_fn = self.config_fn
        locs = self.locs
        power_curves_fn = self.power_curves_fn
        domain = self.domain
        dataset = self.dataset
        exec_type = self.exec_type
        simulation = self.simulation
        start_date = self.start_date
        end_date = self.end_date
        group_by = self.group_by
        weight_var = self.weight_var
        n_batch = self.n_batch
        n_batch_locs_interp = self.n_batch_locs_interp
        config = self.config
        params_sim = self.params_sim
        out_dir = self.out_dir
        temp_dir = self.temp_dir

        if case == "fluct":
            fn_list = [f"{temp_dir}ds_fluct.nc"]
        elif case == "forecast":
            fn_list = [f"{temp_dir}{forecast_run_name}_forecast.nc"]
        else:
            fn_list = sorted(glob.glob(f"{temp_dir}ds_meso_*.nc"))

        extract_all = False

        df_out = dict()
        Nb = len(fn_list)
        for ib, ds_fn in enumerate(fn_list):

            ds = xr.open_dataset(ds_fn)
            locs_ID = ds.locs_ID.values

            start = time.time()
            locs_sel = locs.loc[locs_ID, :].copy()

            if "WPP" in simulation:
                use_extended_table = get_safe_from_cfg(
                    config, ["generic_farm", "use_extended_table"]
                )
                use_ML_wake = config["simulation"][simulation]["ML_wake"]
                power_curves = (
                    self.ext_ML_table
                    if (use_ML_wake and use_extended_table)
                    else power_curves_fn
                )
                df_var = computes_generation_WPP_distributed(
                    WPP=locs_sel,
                    ds_interp_fn=ds_fn,
                    power_curves=power_curves,
                    storm_shutdown=params_sim["storm_shutdown"],
                    varWS=params_sim["varWS"],
                    varWD=params_sim["varWD"],
                    n_batch=n_batch,
                )

            elif simulation == "SPP":
                df_var = computes_generation_SPP_distributed(
                    SPP=locs_sel,
                    ds_interp_fn=ds_fn,
                    verbose=False,
                    varWS=params_sim["varWS"],
                    varTAIR=params_sim["varTAIR"],
                    n_batch=n_batch,
                    ERA5_dni_fix=params_sim["compute_dni_from_ghi"],
                    clear_sky=False,
                )

            elif simulation == "CSP":
                df_var = computes_generation_CSP_distributed(
                    locs=locs_sel,
                    ds_interp_fn=ds_fn,
                    model=params_sim["model"],
                    n_batch=n_batch,
                    ERA5_dni_fix=params_sim["compute_dni_from_ghi"],
                )

            else:
                # # Extract vars_needed
                vars_needed = list(ds.keys())
                df_var = dict()
                for var in vars_needed:
                    df_var[var] = (
                        ds[var]
                        .to_dataframe()
                        .reset_index()
                        .set_index("time")
                        .pivot(columns="locs_ID", values=var)
                    )
                    df_var[var].columns = locs_sel.Name.values

            ds.close()

            # Group by
            if "WPP" in simulation:
                df_grouped = groupby_weighted_dask(
                    locs=locs.copy(),
                    df_var=df_var,
                    group_by=group_by,
                    weight_var=weight_var,
                )
            else:
                df_grouped = groupby_weighted(
                    locs=locs.copy(),
                    df_var=df_var,
                    group_by=group_by,
                    weight_var=weight_var,
                )

            if ib == 0:
                df_out = dict()
                for var in df_grouped.keys():
                    df_out[var] = df_grouped[var].fillna(0.0)
            else:
                for var in df_grouped.keys():
                    df_out[var] += df_grouped[var].fillna(0.0)

            end = time.time()
            print(f"{ib+1} batch out of {Nb}, exec. time:", end - start)

        return df_out


def get_dask_client(
    exec_type="local",
    walltime="08:00:00",
    threads_per_worker=1,
    number_of_nodes=1,
):
    """
    Function to start a parallel setup.

    Parameters
    ----------

    exec_type: str
        Execution types. Available are 'jess', 'local'

    TODO:
        Add parallel excecution setup for sophia.dtu.dk

    """
    if exec_type == "jess_dask_jobqueue":
        cluster = dask_jobqueue.PBSCluster(
            queue="windq",
            name="dask-worker",
            walltime=walltime,
            cores=20,
            processes=2,
            memory="32GB",
            resource_spec="nodes=1:ppn=20",
            interface="ib0",
            use_stdin=True,
        )  # For dask to submit workers via: bsub < /tmp/jobscript.sh

        cluster.scale(number_of_nodes)
        client = Client(cluster)
        n_workers, memory_available = get_n_workers_and_memory(client)

    else:  # exec_type == 'local':

        client = Client(processes=True, threads_per_worker=threads_per_worker)
        n_workers, memory_available = get_n_workers_and_memory(client)

    print()
    print(str(client).replace("<", "").replace(">", ""))
    try:
        dashboard_port = client.__dict__["_scheduler_identity"]["services"]["dashboard"]
        print(f"dashboard port: {dashboard_port}")
    except BaseException:
        pass

    return client


def save_vars(
    df_var,
    out_dir,
    var_to_save=None,
    sufix="",
    return_fns=False,
    verbatim=True,
):
    """
    Function to save specific variables

    Parameters
    ----------

    df_var: DataFrame
        table with results

    out_dir: str
        oputput folder

    var_to_save: str or list of str
        variables to save

    return_fns: bool
        Flag to return folder

    verbatim: bool
        Flag to print details

    """
    if verbatim:
        print(f"\nSaving output files:")

    if var_to_save is None:
        var_to_save = list(df_var.keys())

    if not os.path.exists(out_dir):
        mkdir(out_dir)

    # Variables to save
    out_fns = []
    for var in var_to_save:
        out_fn = os.path.join(out_dir, var + sufix + ".csv")
        out_fns += [out_fn]
        aux = df_var[var]
        aux = aux.loc[:, ~aux.columns.duplicated()]

        columns_with_suffix_cloned = [
            col for col in aux.columns if col.endswith("_clone")
        ]
        if (len(aux.keys()) == 2) & (len(columns_with_suffix_cloned) > 0):
            aux = aux.drop(columns=[columns_with_suffix_cloned[0]])

        if var in [
            "Evaporation",
            "Runoff",
            "Sub_surface_runoff",
            "Surface_runoff",
            "Total_precipitation",
        ]:
            aux = aux * 1000000
            out_fn = out_fn.replace(".csv", "_micrometers.csv")
            sufix = "_micrometers"
        elif var in ["Snow_depth"]:
            aux = aux * 1000
            out_fn = out_fn.replace(".csv", "_millimeters.csv")
            sufix = "_millimeters"

        aux.to_csv(out_fn, float_format="%.4f")

        out_stats_fn = os.path.join(out_dir, var + sufix + "_stats.csv")
        aux.describe().to_csv(out_stats_fn, float_format="%.4f")
        if var in [
            "Evaporation",
            "Runoff",
            "Sub_surface_runoff",
            "Surface_runoff",
            "Total_precipitation",
            "Snow_depth",
        ]:
            sufix = ""

    if verbatim:
        print("\n".join(out_fns))

    if return_fns:
        return out_fns


def groupby_weighted(
    locs,
    df_var,
    group_by,
    var_to_group=None,
    weight_var="Installed_capacity_MW",
):
    """
    Function to groupby specific variables

    Parameters
    ----------

    locs: DataFrame
        table with plant locations

    df_var: DataFrame
        table with results

    group_by: str
        Variable to group by

    var_to_group: list(strs)
        Variables to be grouped

    weight_var: str
        Weight variable
        default='Installed_capacity_MW'

    """

    if "Name" in locs.columns:
        locs.set_index("Name", inplace=True)

    if var_to_group is None:
        var_to_group = list(df_var.keys())

    df_var_out = dict()

    for var in var_to_group:
        df_var_out[var] = pd.DataFrame()

    for case in locs[group_by].unique():
        locs_sel = locs.loc[locs[group_by] == case, :]
        names_in_sel = [
            name for name in locs_sel.index.tolist() if name in df_var[var].columns
        ]

        for var in var_to_group:
            if len(names_in_sel) > 0:
                # Apply circular mean to wind direction
                if (var == "WDIR") or (var == "WD"):
                    WDx = (
                        np.cos(df_var[var].loc[:, names_in_sel]) * locs_sel[weight_var]
                    ).sum(axis=1) / locs_sel[weight_var].sum()
                    WDy = (
                        np.sin(df_var[var].loc[:, names_in_sel]) * locs_sel[weight_var]
                    ).sum(axis=1) / locs_sel[weight_var].sum()

                    df_var_out[var][case] = np.mod(
                        np.rad2deg(np.arctan2(WDy, WDx)) + 360, 360
                    )
                else:
                    df_var_out[var][case] = (
                        df_var[var].loc[:, names_in_sel] * locs_sel[weight_var]
                    ).sum(axis=1) / locs_sel[weight_var].sum()
            else:
                df_var_out[var][case] = np.NaN

    locs = locs.reset_index()

    # ensure all grouby variables are returned
    for var in var_to_group:
        df_var_out[var] = df_var_out[var].loc[:, locs[group_by].unique()]

    return df_var_out


def groupby_weighted_dask(
    locs,
    df_var,
    group_by,
    var_to_group=None,
    weight_var="Installed_capacity_MW",
):
    """
    Function to groupby specific variables using Dask DataFrame

    Parameters
    ----------

    locs: Dask DataFrame
        table with plant locations

    df_var: Dask DataFrame
        table with results

    group_by: str
        Variable to group by

    var_to_group: list(strs)
        Variables to be grouped

    weight_var: str
        Weight variable
        default='Installed_capacity_MW'

    """

    if "locs_ID" in locs.columns:
        locs = locs.set_index("locs_ID")

    if var_to_group is None:
        var_to_group = list(df_var.keys())

    df_var_out = {var: pd.DataFrame() for var in var_to_group}

    for var in var_to_group:
        if group_by == "Name_ID":
            if isinstance(df_var[var], pd.DataFrame):
                df_var_out[var] = df_var[var]
            else:
                df_var_out[var] = df_var[var].compute()
                df_var_out[var].columns = locs[group_by].values[:]
        else:
            for iter1, case in enumerate(locs[group_by].unique()):

                locs_sel = locs.loc[locs[group_by] == case, :]
                names_in_sel = [
                    name
                    for name in locs_sel.index.tolist()
                    if name in df_var[var].columns
                ]
                locs_sel_in_batch = locs_sel[locs_sel.index.isin(names_in_sel)]

                if len(names_in_sel) > 0:

                    if (var == "WDIR") or (var == "WD"):
                        WDx = (
                            (
                                np.cos(df_var[var].loc[:, names_in_sel].compute())
                                * locs_sel[weight_var]
                            )
                        ).sum(axis=1) / locs_sel[weight_var].sum()

                        WDy = (
                            (
                                np.sin(df_var[var].loc[:, names_in_sel].compute())
                                * locs_sel[weight_var]
                            )
                        ).sum(axis=1) / locs_sel[weight_var].sum()

                        df_var_out[var][case] = np.mod(
                            np.rad2deg(np.arctan2(WDy, WDx)) + 360, 360
                        )

                    else:

                        df_aux = (
                            (
                                df_var[var].loc[:, names_in_sel].compute()
                                * locs_sel_in_batch[weight_var]
                            )
                        ).sum(axis=1) / locs_sel[weight_var].sum()

                        df_var_out[var][case] = df_aux

                else:
                    df_var_out[var][case] = np.NaN

    locs = locs.reset_index()

    return df_var_out


def groupby_summary(
    locs,
    group_by,
    weight_var="Installed_capacity_MW",
):
    """
    Function to groupby specific variables when you have large datasets

    Parameters
    ----------

    locs: DataFrame
        table with plant locations

    df_var: DataFrame
        table with results

    group_by: str
        Variable to group by

    var_to_group: list(strs)
        Variables to be grouped

    weight_var: str
        Weight variable
        default='Installed_capacity_MW'

    """
    numeric_types = ["int16", "int32", "int64", "float16", "float32", "float64"]

    # Define a lambda function to compute the weighted mean:
    def wm(x):
        return np.average(x, weights=locs.loc[x.index, weight_var])

    # select columns to do weighted mean grouping:
    #   - Only apply to numeric variables
    #   - Not to 'ID' nor 'Installed_capacity_MW' columns
    #   - Not to variables that include nPoints, or start with ws_

    cols_out = [
        col
        for col in locs.columns
        if (col not in ["ID", "locs_ID", weight_var])
        & ("nPoints" not in col)
        & (~col.startswith("ws_"))
        & (locs.dtypes[col] in numeric_types)
    ]
    col_nPoint = [col for col in locs.columns if ("nPoints" in col)]

    # Groupby and aggregate with weighted mean:
    locs_grouped = (
        locs.loc[:, cols_out + [group_by, weight_var]].groupby(group_by).agg(wm)
    ).loc[:, cols_out]
    locs_grouped.columns = [col + "_wm" for col in cols_out]
    locs_grouped = locs_grouped.dropna(axis=1)

    # Other variables in summary
    # Number of plants are the count
    locs_grouped["Number_of_plants"] = (
        locs.loc[:, [group_by, weight_var]]
        .groupby(group_by)
        .agg("count")
        .loc[:, weight_var]
    )
    # Installed_capacity_MW it the sum of all Installed_capacity_MW per plant
    locs_grouped[weight_var] = (
        locs.loc[:, [group_by, weight_var]]
        .groupby(group_by)
        .agg("sum")
        .loc[:, weight_var]
    )

    # Columns with nPoints information are summed
    for col in col_nPoint:
        locs_grouped[col] = (
            locs.loc[:, [group_by, col]].groupby(group_by).agg("sum").loc[:, col]
        )

    return locs_grouped


def groupby_additional_groups(
    locs,
    df_var,
    group_by,
    var_to_group=None,
    weight_var="Installed_capacity_MW",
    df_add_group=[],
    add_group_var="AggregationCode",
    add_group_part="AggregationDescription",
):
    """

    Function to perform additional groupings.

    Parameters
    ----------

    locs: DataFrame
        power plant locations table

    df_var: dict
        Dictionary with all the tables for all the variables.

    var_to_group: list, default ['WSPD', 'WDIR', 'POW']
        List of variables to group into individual tables, with a column for each
        instance of the group_by variable.

    group_by: str
        Variable to group the plant by. All variables will be given as a weighted mean
        with respect the Installed_capacity_MW column in locs.

    weight_var: str, default 'Installed_capacity_MW'
        Variable in locs to use in the weighted mean per each group


    df_add_group: DataFrame, default none
        DataFrame that describes additional grouping to be performed. This can be used
        to return multiple groupings such as generation time series per electric grid
        region and per country. The file should describe which regions are to be used on
        which countries.

    add_group_var:str, default 'AggregationCode'
        Variable name (column) that defines the name of the  additional groups in the
        df_add_group table.

    add_group_part:str, default 'AggregationDescription'
        Variable name (column) that defines the parts to be aggregated.
        The parts should be a string separated by ',' and it should be consistent with
        the group_by selection.

        For example if you set:

        group_by = 'Country'
        df_add_group=df_add_group
        add_group_var='AddGroup'
        add_group_part='Parts'

        .. code-block:: python

            df_add_group = pd.DataFrame(columns = ['AddGroup','Parts'])
            df_add_group.loc[0, 'AddGroup'] = 'NEurope'
            df_add_group.loc[0, 'Parts'] = ','.join(
                locs.loc[locs.WRF_Domain=='NEurope','Country'].unique().tolist())
            df_add_group.loc[1, 'AddGroup'] = 'SEurope'
            df_add_group.loc[1, 'Parts'] = ','.join(
                locs.loc[locs.WRF_Domain=='SEurope','Country'].unique().tolist())

        will return two additional groups 'NEurope' and 'SEurope'
    """

    if "Name" in locs.columns:
        locs.set_index("Name", inplace=True)

    if var_to_group is None:
        var_to_group = list(df_var.keys())

    locs_grouped = groupby_summary(locs, group_by)
    cases_list = locs[group_by].unique()

    df_var_out = df_var.copy()
    if len(df_add_group) >= 0:
        for i_case in df_add_group.index.tolist():
            case = df_add_group.loc[i_case, add_group_var]
            # Only compute groups not already avalibale
            if case not in cases_list:
                # parts are expected to be a string separated by ','
                parts = df_add_group.loc[i_case, add_group_part].split(",")
                for var in var_to_group:
                    # Apply circular mean to wind direction
                    if var == "WDIR":
                        WDx = (
                            np.cos(df_var_out[var][parts]) * locs_grouped[weight_var]
                        ).sum(axis=1) / locs_grouped[weight_var].sum()
                        WDy = (
                            np.sin(df_var_out[var][parts]) * locs_grouped[weight_var]
                        ).sum(axis=1) / locs_grouped[weight_var].sum()

                        df_var_out[var][case] = np.mod(
                            np.rad2deg(np.arctan2(WDy, WDx)) + 360, 360
                        )
                    else:
                        df_var_out[var][case] = (
                            df_var_out[var][parts] * locs_grouped[weight_var]
                        ).sum(axis=1) / locs_grouped[weight_var].sum()

    return df_var_out


def get_power_curves_gen_wf(
    locs_pd,
    WL_ML_fn,
    vars_dic={
        "var_sp": "specific_power",
        "var_D": "Rotor_diameter",
        "var_Nwt": "Number_of_turbines",
        "var_wind_MW_per_km2": "wind_MW_per_km2",
    },
):
    """

    Function to perform additional groupings.

    Parameters
    ----------

    locs_pd: DataFrame
        power plant locations table

    WL_ML_fn: str
        filename of the databse of generic farms

    vars_dic: dict
        Dictionary with variables for estimating required variables
    """

    genWF_db = xr.open_dataset(WL_ML_fn)

    # Remove cases that extrapolate
    locs_pd.loc[locs_pd[vars_dic["var_Nwt"]] == 0, vars_dic["var_Nwt"]] = 1
    locs_pd.loc[
        locs_pd[vars_dic["var_sp"]] < genWF_db.specific_power.values.min(),
        vars_dic["var_sp"],
    ] = genWF_db.specific_power.values.min()
    locs_pd.loc[
        locs_pd[vars_dic["var_sp"]] > genWF_db.specific_power.values.max(),
        vars_dic["var_sp"],
    ] = genWF_db.specific_power.values.max()
    locs_pd.loc[
        locs_pd[vars_dic["var_D"]] < genWF_db.Rotor_diameter.values.min(),
        vars_dic["var_D"],
    ] = genWF_db.Rotor_diameter.values.min()
    locs_pd.loc[
        locs_pd[vars_dic["var_D"]] > genWF_db.Rotor_diameter.values.max(),
        vars_dic["var_D"],
    ] = genWF_db.Rotor_diameter.values.max()

    specific_power = locs_pd[vars_dic["var_sp"]].values
    Rotor_diameter = locs_pd[vars_dic["var_D"]].values
    Number_of_turbines = locs_pd[vars_dic["var_Nwt"]].values
    wind_MW_per_km2 = locs_pd[vars_dic["var_wind_MW_per_km2"]].values

    locs_xr = xr.Dataset(
        data_vars={
            "specific_power": (["iloc"], specific_power),
            "Rotor_diameter": (["iloc"], Rotor_diameter),
            "Number_of_turbines": (["iloc"], Number_of_turbines),
            "wind_MW_per_km2": (["iloc"], wind_MW_per_km2),
        },
        coords={
            "iloc": locs_pd.index.values,
        },
    )

    genWF = genWF_db.interp(locs_xr, kwargs={"fill_value": 0})

    pc_df = (
        genWF.pc.to_dataframe().loc[:, ["pc"]].pivot_table(index="iloc", columns="ws")
    )
    pc_df.columns = [col[1] for col in pc_df.columns]
    pc_df.index.name = "Power_curve"
    pc_df = pc_df.reset_index()

    locs_pd["Power_curve"] = locs_pd.index.values

    return pc_df, locs_pd


def get_power_curves_gen_wf_extended(
    locs_pd: pd.DataFrame,
    WL_ML_fn: str,
    vars_dic: dict = {
        "var_sp": "specific_power",
        "var_D": "Rotor_diameter",
        "var_Nwt": "Number_of_turbines",
        "var_wind_MW_per_km2": "wind_MW_per_km2",
        "var_TI": "TI",
    },
    default_TI: float = 0.1,
    default_rho: float = 1.225,
) -> xr.Dataset:
    """Interpolate the generic wind farm dataset to the specific WPP spec.
    Based on the extended ML lookup table containing TI & air density as
    opposed to the previous version.

    Parameters
    ----------
    locs_pd : _type_
        Wind power plant locations & specification table.
    WL_ML_fn : _type_
        Filename of the generic wind farm dataset.
    vars_dic : dict, optional
        Variable mapping in the generic wind farm dataset.
    default_TI : float, optional
        Default value for the turbulence intensity.
    default_rho : float, optional
        Default value for the air density.

    Returns
    -------
    xr.Dataset
        Generic wind farm dataset interpolated to the specific WPP spec.
    """

    genWF_db = xr.open_dataset(WL_ML_fn)

    if not isinstance(default_TI, float) or not isinstance(default_rho, float):
        min_ti = genWF_db[vars_dic["var_TI"]].min().values
        max_ti = genWF_db[vars_dic["var_TI"]].max().values
        min_rho = genWF_db[vars_dic["var_rho"]].min().values
        max_rho = genWF_db[vars_dic["var_rho"]].max().values
        raise ValueError(
            f"default_TI must be a float between {min_ti} and {max_ti},"
            f"and default_rho must be a float between {min_rho} and {max_rho}"
        )

    # validate that the variables are in the coordinates
    binary_found_mask = [var in genWF_db.coords for var in vars_dic.values()]
    not_found = np.array(list(vars_dic.values()))[~np.array(binary_found_mask)]
    assert all(binary_found_mask), (
        f"Coordinates not found in the generic farm dataset: {not_found}. "
        f"Available coordinates are: {list(genWF_db.coords)}."
    )

    if vars_dic["var_TI"] not in locs_pd.columns:
        locs_pd[vars_dic["var_TI"]] = default_TI
    if vars_dic["var_rho"] not in locs_pd.columns:
        locs_pd[vars_dic["var_rho"]] = default_rho

    # remove cases that extrapolate
    locs_pd.loc[locs_pd[vars_dic["var_Nwt"]] == 0, vars_dic["var_Nwt"]] = 1
    for _, var_values in vars_dic.items():
        min_value = genWF_db[var_values].values.min()
        max_value = genWF_db[var_values].values.max()
        locs_pd.loc[locs_pd[var_values] < min_value, var_values] = min_value
        locs_pd.loc[locs_pd[var_values] > max_value, var_values] = max_value

    locs_xr = xr.Dataset(
        data_vars={
            var: (["iloc"], locs_pd[var].values)
            for var in vars_dic.values()
            # do not interpolate density now; might
            # be provided later as time series
            if var != vars_dic["var_rho"]
        },
        coords={"iloc": locs_pd.index.values},
    )
    genWF_db = genWF_db.interp(locs_xr)
    from scipy.interpolate import pchip_interpolate  # fmt: skip
    locations = genWF_db.iloc.values
    air_densities = genWF_db.Air_density.values
    wss = [0] + list(genWF_db.ws.values)
    new_wss = np.arange(0, np.max(wss) + 0.5, 0.5)
    new_pcs = np.zeros((len(locations), len(air_densities), len(new_wss)))
    for i, loc in enumerate(locations):
        for j, rho in enumerate(air_densities):
            pc = genWF_db.sel(iloc=loc, Air_density=rho).power.values
            pc = pchip_interpolate(wss, [0] + list(pc), new_wss)
            new_pcs[i, j, :] = pc

    return xr.Dataset(
        {
            "pc": (["iloc", "Air_density", "ws"], new_pcs),
            "ws": new_wss,
        },
        coords={
            "iloc": locs_pd.index.values,
            "Air_density": air_densities,
        },
    )


def get_frequent_power_curves_stall(stall_locs, power_curves_fn, pitch_pcs_n):
    """
    Function to get the most frequent power curves at stall locations and assign new power curve IDs to them.

    Parameters
    ----------
    stall_locs : pandas DataFrame
        The power plant locations table containing information about the stall locations including
        the power curve IDs.

    power_curves_fn : str
       filename of the input power curves

    pitch_pcs_n : int
        An integer representing the starting value of new power curve IDs.
        power_curves=pd.read_excel(power_curves_fn)
    """
    power_curves = pd.read_excel(power_curves_fn)

    # bins=get_bins(stall_locs)
    bins = [
        {"min": 0, "max": 350},
        {"min": 350, "max": 400},
        {"min": 400, "max": 450},
        {"min": 450, "max": None},
    ]

    pc_df_stall = pd.DataFrame()
    locs_stall = pd.DataFrame()
    for iter1 in bins:
        stall_bin_df, mask = get_bin_specific_power(stall_locs, iter1)
        if len(stall_bin_df) > 0:

            most_frequent_value = stall_bin_df["Power_curve"].value_counts().idxmax()

            rest_values = list(stall_bin_df["Power_curve"])
            rest_values = list(filter(lambda x: x != most_frequent_value, rest_values))

            mask_rest = power_curves["Power_curve"].isin(rest_values)
            mask_freq = power_curves["Power_curve"] == most_frequent_value

            freq_pc = power_curves.loc[mask_freq]
            freq_pc = pd.concat(
                [freq_pc] + [freq_pc] * (len(stall_bin_df) - 1), ignore_index=True
            )
            freq_pc["Power_curve"] = stall_bin_df["locs_ID"].values

            stall_bin_df["Power_curve"] = stall_bin_df["locs_ID"]

            pc_df_stall = pd.concat(
                [pc_df_stall, freq_pc], ignore_index=False, sort=False
            )

            locs_stall = pd.concat(
                [locs_stall, stall_bin_df], ignore_index=False, sort=False
            )
        else:
            continue

    return pc_df_stall, locs_stall


def get_safe_from_cfg(cfg: dict, keys: list):
    # keys are nested items
    tmp_cfg = cfg.copy()
    try:
        for key in keys:
            tmp_cfg = tmp_cfg[key]
    except KeyError:
        return None
    return tmp_cfg
