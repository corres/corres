import os

# main libraries
import numpy as np
import pandas as pd
import xarray as xr
from dask import compute, delayed

# pvlib imports
from pvlib import atmosphere, irradiance, pvsystem, tools
from pvlib.location import Location
from pvlib.modelchain import ModelChain
from pvlib.pvsystem import PVSystem
from pvlib.temperature import TEMPERATURE_MODEL_PARAMETERS
from pvlib.tracking import SingleAxisTracker

from corres.auxiliar_functions import offset_timeseries, split_in_batch
from corres.meso_tools import apply_interpolation_f


def get_altitude(
    meso_extractor,
):
    """
    Interpolates the altitude for each location.

    Parameters
    ----------

    meso_extractor: Object
        Meso extractor object that carries interpolation weights, domains and other info
        The solar power plants table is taken from the meso_extractor. This table
        includes properties of each plant: Name, Latitude, Longitude, Surface_tilt,
        Surface_azimuth.

    """

    # Solar plants properties from the meso_extractor
    SPP = meso_extractor.locs.loc[
        meso_extractor.locs.WRF_Domain.isin(meso_extractor.domains)
    ]

    # Compute the altitude based on the WRF static files elevation
    SPP["Altitude"] = 0.0
    for domain in meso_extractor.domains:
        static_fn = os.path.join(
            meso_extractor.meso_dir,
            meso_extractor.static_template.replace("{domain}", domain),
        )
        static = xr.open_dataset(static_fn)
        weights_ds = xr.open_dataset(meso_extractor.weights_fn[domain])
        interp = apply_interpolation_f(
            static,
            weights_ds,
            vars_xy_logz=[],
            vars_xyz=[],
            vars_xy=["HGT"],
            vars_nearest_xy=[],
            vars_nearest_xyz=[],
        )
        ind = SPP.loc[SPP.WRF_Domain == domain, :].index.to_list()
        SPP.loc[ind, "Altitude"] = (
            interp.to_dataframe().loc[SPP.loc[ind, "ID"], "HGT"].values
        )

    return SPP


def computes_generation_SPP(
    SPP,
    ds_interp_fn,
    verbose=False,
    varWS="WS",
    varTAIR="T2",
    batch=[],
    ERA5_dni_fix=False,
    clear_sky=False,
):
    """
    Makes the prediction of the time series of PV production for multiple solar
    power plants.

    Parameters
    ----------

    SPP: DataFrame
        Solar power plants table including properties of each plant: Name, Latitude,
        Longitude, Surface_tilt, Surface_azimuth.

    ds_interp_fn: Str
        Filename of the netcdf with the time series of the irradiances at each WPP.

    """

    ds_interp = xr.open_dataset(ds_interp_fn).sel(locs_ID=batch)

    # # Extract vars_needed
    vars_needed = list(ds_interp.keys())
    df_var = dict()
    for var in vars_needed:
        df_var[var] = (
            ds_interp[var]
            .to_dataframe()
            .reset_index()
            .set_index("time")
            .pivot(columns="locs_ID", values=var)
        )
        df_var[var].columns = SPP.Name.values

        if var in ["DNI", "DHI", "GHI"]:
            aux = offset_timeseries(df_var[var], offset=-30, unit="min")
            df_var[var] = aux.interpolate()  # .fillna(0.0)
            df_var[var][df_var[var] < 0] = 0

    if len(batch) > 0:
        SPP = SPP.copy().loc[batch, :]
        wspd_df = df_var[varWS]
        tair_df = df_var[varTAIR]

        if "DNI" in vars_needed:
            dni_df = df_var["DNI"]
        else:
            dni_df = tair_df * np.nan

        if "DHI" in vars_needed:
            dif_df = df_var["DHI"]
        else:
            dif_df = tair_df * np.nan

        if "GHI" in vars_needed:
            ghi_df = df_var["GHI"]
        else:
            ghi_df = tair_df * np.nan

    # allocate outputs
    solar_ac = tair_df * np.nan
    ghi_clearsky = tair_df * np.nan
    dhi_clearsky = tair_df * np.nan
    dni_clearsky = tair_df * np.nan
    solar_ac_clearsky = tair_df * np.nan

    # For now always use the same PV + inverter. This should be given in the
    # SPP excel.

    # Sandia
    sandia_modules = pvsystem.retrieve_sam("SandiaMod")
    module_name = "Canadian_Solar_CS5P_220M___2009_"
    module = sandia_modules[module_name]
    module["aoi_model"] = irradiance.aoi

    # 2. Inverter
    # -------------
    inverters = pvsystem.retrieve_sam("cecinverter")
    inverter = inverters["ABB__MICRO_0_25_I_OUTD_US_208__208V_"]

    temp_model = TEMPERATURE_MODEL_PARAMETERS["sapm"]["open_rack_glass_glass"]

    if "Tracking" not in SPP.columns:
        SPP["Tracking"] = False

    if "Overpower" not in SPP.columns:
        SPP["Overpower"] = 1.1

    for i in range(len(SPP)):
        loc_sel = SPP.iloc[i, :]
        latitude = loc_sel.Latitude
        longitude = loc_sel.Longitude
        altitude = loc_sel.Altitude
        name = loc_sel.Name

        pvloc = Location(
            latitude=loc_sel.Latitude,
            longitude=loc_sel.Longitude,
            # tz=loc_sel.Time_zone,
            altitude=loc_sel.Altitude,
            name=loc_sel.Name,
        )

        if ~loc_sel.Tracking:

            system = PVSystem(
                module_parameters=module,
                inverter_parameters=inverter,
                temperature_model_parameters=temp_model,
                surface_tilt=loc_sel.Surface_tilt,
                surface_azimuth=loc_sel.Surface_azimuth,
            )

        else:
            system = SingleAxisTracker(
                module_parameters=module,
                inverter_parameters=inverter,
                temperature_model_parameters=temp_model,
                axis_tilt=loc_sel.Surface_tilt,
                axis_azimuth=loc_sel.Surface_azimuth,
                max_angle=90,
                backtrack=True,
                gcr=0.2857142857142857,
            )

        times = tair_df.index
        cs = pvloc.get_clearsky(times)
        pressure = atmosphere.alt2pres(loc_sel.Altitude)
        solpos = pvloc.get_solarposition(times, pressure)

        weather = pd.DataFrame(index=times)
        if ERA5_dni_fix:
            # # To termporarly solve the bug in ERA5.dni
            weather["ghi"] = ghi_df.loc[:, [name]]

            disc_out = irradiance.disc(
                ghi=weather["ghi"],
                solar_zenith=solpos.zenith,
                datetime_or_doy=times,
                pressure=pressure,
                min_cos_zenith=0.065,
                max_zenith=87,
                max_airmass=12,
            )
            weather["dni"] = disc_out["dni"]
            weather["dhi"] = weather.ghi - weather.dni * tools.cosd(solpos.zenith)

        else:
            weather["dni"] = dni_df[name]

            if isinstance(dif_df, pd.core.frame.DataFrame):
                weather["dhi"] = dif_df[name]
            else:
                weather["dhi"] = cs.dhi

            if isinstance(ghi_df, pd.core.frame.DataFrame):
                weather["ghi"] = ghi_df[name]
            else:
                # Compute ghi from PVLib complete irradiance tool
                # mc.complete_irradiance(weather)
                weather["ghi"] = weather.dni * tools.cosd(solpos.zenith) + weather.dhi
                weather.loc[weather["ghi"].abs() > 2000, "ghi"] = np.nan
                # Fill in missing values with clear_sky
                weather["ghi"] = weather["ghi"].fillna(cs.dhi)

        weather["temp_air"] = tair_df[name] - 273.15  # Celcium
        weather["wind_speed"] = wspd_df[name]

        mc = ModelChain(system, pvloc)

        # Run solar with the WRF weather
        mc.run_model(weather)
        # scaled for overpower
        solar_ac[name] = loc_sel.Overpower * (mc.ac / inverter.Paco)
        # curtail excess from overpower
        solar_ac.loc[solar_ac[name] > 1, name] = 1

        if clear_sky:
            ghi_clearsky[name] = cs.ghi
            dhi_clearsky[name] = cs.dhi
            dni_clearsky[name] = cs.dni

            # Run solar with clear sky weather timeseries
            mc.run_model(cs)
            # scaled for overpower
            solar_ac_clearsky[name] = loc_sel.Overpower * (mc.ac / inverter.Paco)
            # curtail excess from overpower
            solar_ac_clearsky.loc[solar_ac_clearsky[name] > 1, name] = 1

    vars_out = dict()
    vars_out["DHI"] = dif_df
    vars_out["DNI"] = dni_df
    vars_out["GHI"] = ghi_df
    vars_out["P"] = solar_ac.fillna(solar_ac.min())
    if clear_sky:
        vars_out["GHI_CLEARSKY"] = ghi_clearsky
        vars_out["DHI_CLEARSKY"] = dhi_clearsky
        vars_out["DNI_CLEARSKY"] = dni_clearsky
        vars_out["P_CLEARSKY"] = solar_ac_clearsky.fillna(solar_ac_clearsky.min())

    return vars_out


def computes_generation_SPP_distributed(
    SPP,
    ds_interp_fn,
    verbose=False,
    varWS="WS",
    varTAIR="T2",
    n_batch=10,
    ERA5_dni_fix=False,
    clear_sky=False,
):
    """
    Makes the prediction of the time series of PV production for multiple solar
    power plants using dask distributed computations.

    Parameters
    ----------

    SPP: DataFrame
        Solar power plants table including properties of each plant: Name, Latitude,
        Longitude, Surface_tilt, Surface_azimuth.

    ds_interp_fn: Str
        Filename of the netcdf with the time series of the irradiances at each WPP.

    """

    batches = split_in_batch(SPP.index.tolist(), n_batch)

    dly = dict()
    for ib, batch in enumerate(batches):
        if len(batch) > 0:
            locs_batch = SPP.loc[batch, :]
            dly[ib] = delayed(computes_generation_SPP)(
                SPP=locs_batch,
                ds_interp_fn=ds_interp_fn,
                verbose=verbose,
                varWS=varWS,
                varTAIR=varTAIR,
                batch=batch,
                ERA5_dni_fix=ERA5_dni_fix,
                clear_sky=clear_sky,
            )

    all_v = compute(dly)[0]
    out = dict()
    for var in all_v[0].keys():
        out[var] = pd.concat([all_v[key][var] for key in all_v.keys()], axis="columns")
    return out
