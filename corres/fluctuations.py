import string

import numpy as np
import pandas as pd
import scipy as sp
import utm
import xarray as xr
from numpy import linalg as la
from numpy import newaxis as na
from scipy import linalg


def find_n_power_2(n):
    """
    Auxiliar function to find nearest (next) power of 2.
    """
    i = 1
    while (2**i) < n:
        i += 1
    return 2**i


class XGLarsen_S_f(object):
    """
    Low frequency wind speed power density spectrum.

    Larsen, X. G., Ott, S., Badger, J., Hahmann, A. N., & Mann, J. (2012).
    Recipes for correcting the impact of effective mesoscale resolution on
    the estimation of extreme winds. Journal of Applied Meteorology and Climatology,
    51(3), 521-533.
    doi = https://doi.org/10.1175/JAMC-D-11-090.1

    Parameters
    ----------
    a1: float
        Variance parameter

    f0: float
        Starting frequency
    """

    def __init__(self, a1=2e-4, f0=1 / (10 * 60 * 60)):  # 1/10h
        self.a1 = a1
        self.f0 = f0

    def __eval__(self, f):

        a1 = self.a1
        f0 = self.f0

        return (f >= f0) * a1 / (f0 ** (5 / 3) + f ** (5 / 3))


class Kaimal_S_f(object):
    """
    Turbulence wind speed power density spectrum.

    Kaimal, J. C., Wyngaard, J. C. J., Izumi, Y., & Coté, O. R. (1972).
    Spectral characteristics of surface‐layer turbulence. Quarterly Journal
    of the Royal Meteorological Society, 98(417), 563-589.
    doi = https://doi.org/10.1002/qj.49709841707

    Parameters
    ----------
    sigma: float
        Horizontal turbulence standard deviation

    z: float
        Height above ground

    V0: float
        Reference wind speed
    """

    def __init__(self, sigma=1.0, z=100, V0=8, f0=1 / (10 * 60 * 60)):  # 1/10h
        if z <= 60:  # [m]
            L1 = 5.67 * z
        else:
            L1 = 340.2  # [m]

        self.sigma = sigma
        self.z = z
        self.V0 = V0
        self.L1 = L1
        self.f0 = f0

    def __eval__(self, f):

        sigma = self.sigma
        z = self.z
        V0 = self.V0
        L1 = self.L1
        f0 = self.f0

        return (f >= f0) * sigma**2 * (2 * L1 / V0) / (1 + 6 * L1 * f / V0) ** (5 / 3)


def Frandsen_sigma_wf(sigma, V0, CT, S1, S2):
    """
    Frandsen added turbulence model.

    Kaimal, J. C., Wyngaard, J. C. J., Izumi, Y., & Coté, O. R. (1972).
    Spectral characteristics of surface‐layer turbulence. Quarterly Journal
    of the Royal Meteorological Society, 98(417), 563-589.
    doi = https://doi.org/10.1002/qj.49709841707

    Parameters
    ----------
    sigma: float
        Horizontal turbulence standard deviation

    V0: float
        Reference wind speed

    CT: float
        Trhust coefficient

    S1: float
        Spacing in the streamwise direction

    S2: float
        Spacing in the crossflow direction
    """

    # Frandsen S.
    # Turbulence and Turbulence-Generated Structural Loading in Wind Turbine Clusters.
    # Risoe-R-1188(EN),
    # Risoe National Laboratory, Roskilde, 2007.
    # Available online: http://www.risoe.dk/rispubl/VEA/reapdf/ris-r-1188.pdf

    return 0.5 * (
        np.sqrt((0.36 * V0 / (1 + 0.2 * np.sqrt(S1 * S2 / CT))) ** 2 + sigma**2)
        + sigma
    )


class Schlez_coherence_coeff(object):
    """
    Schlez coherence coefficients calculator.

    Schlez, W., & Infield, D. (1998).
    Horizontal, two point coherence for separations greater than the measurement height.
    Boundary-Layer Meteorology, 87(3), 459-480.
    doi = https://doi.org/10.1023/A:1000997610233

    Parameters
    ----------
    sigma: float
        Horizontal turbulence standard deviation

    c_lat: float
        Lateral coefficient: $Alat = self.c_lat * self.sigma$

    c_lon: float
        Longitudinal coefficient per V0: $Alon = self.c_lon * self.sigma / V0$
    """

    def __init__(self, sigma, c_lat=17.5, c_lon=15):
        self.sigma = sigma
        self.c_lat = c_lat
        self.c_lon = c_lon

    def __eval__(self, V0):
        Alat = self.c_lat * self.sigma
        Alon = self.c_lon * self.sigma / V0

        return Alat, Alon


class Schlez_coherence_coeff_Soerensen(object):
    """
    Updated Schlez coherence coefficients calculator.

    Sørensen, Poul, Nicolaos Antonio Cutululis, Antonio Vigueras‐Rodríguez,
    Henrik Madsen, Pierre Pinson, Leo E. Jensen, Jesper Hjerrild, and Martin Donovan.
    (2008).
    Modelling of power fluctuations from large offshore wind farms.
    Wind Energy: An International Journal for Progress and Applications in Wind Power
    Conversion Technology 11, no. 1: 29-43.
    doi =  https://doi.org/10.1002/we.246

    Parameters
    ----------
    A_lat_V0_ratio: float
        Lateral coefficient: $Alat = self.A_lat_V0_ratio * V0$

    A_lon: float
        Longitudinal coefficient
    """

    def __init__(self, A_lat_V0_ratio=0.5, A_lon=4):
        self.A_lat_V0_ratio = A_lat_V0_ratio
        self.A_lon = A_lon

    def __eval__(self, V0):
        Alat = self.A_lat_V0_ratio * V0
        Alon = self.A_lon

        return Alat, Alon


class Schlez_coherence_jk_f(object):
    """
    Schlez coherence

    Schlez, W., & Infield, D. (1998).
    Horizontal, two point coherence for separations greater than the measurement height.
    Boundary-Layer Meteorology, 87(3), 459-480.
    doi = https://doi.org/10.1023/A:1000997610233

    Parameters
    ----------
    Alat: float
        Lateral coefficient

    Alon: float
        Longitudinal coefficient

    coherence_coeff_func: class
        coherence coefficienct calculator object, used if Alat or Alon are not given
    """

    def __init__(
        self,
        Alat=None,
        Alon=None,
        coherence_coeff_func=Schlez_coherence_coeff_Soerensen(),
    ):

        self.Alat = Alat
        self.Alon = Alon
        self.coherence_coeff_func = coherence_coeff_func

    def __eval__(self, freq, locs_df, V0, wd):

        Alat = self.Alat
        Alon = self.Alon

        if (Alat is None) or (Alon is None):
            Alat, Alon = self.coherence_coeff_func.__eval__(V0=V0)

        djk = get_distance(locs_df)
        locs_j = locs_df.loc[:, ["x", "y"]].values
        locs_k = locs_df.loc[:, ["x", "y"]].values

        N_locs = locs_j.shape[0]
        dif_jk = locs_k[na, :] - locs_j[:, na]

        theta_wd = np.deg2rad(90.0 - wd)
        theta_jk = np.arctan2(dif_jk[:, :, 1], dif_jk[:, :, 0])
        alpha_jk = theta_wd - theta_jk

        Ajk = (Alon * np.cos(alpha_jk)) ** 2.0 + (Alat * np.sin(alpha_jk)) ** 2

        tau_jk = np.cos(alpha_jk) * djk / V0
        coherence_jk = np.exp(
            0.2
            * (
                -Ajk[na, :, :] * djk[:, :] * freq[:, na, na] / V0
                + -1j * 2 * np.pi * freq[:, na, na] * tau_jk
            )
        )

        return coherence_jk


class Soerensen_rotor_Fwt_f(object):
    """
    Soerensen rotor/turbine admittance

    Sørensen, Poul, Nicolaos Antonio Cutululis, Antonio Vigueras‐Rodríguez,
    Henrik Madsen, Pierre Pinson, Leo E. Jensen, Jesper Hjerrild, and Martin Donovan.
    (2008).
    Modelling of power fluctuations from large offshore wind farms.
    Wind Energy: An International Journal for Progress and Applications in Wind Power
    Conversion Technology 11, no. 1: 29-43.
    doi =  https://doi.org/10.1002/we.246

    Parameters
    ----------
    V0: float
        Reference wind speed

    D: float
        Turbine rotor diameter

    z: float
        Turbine's hub height
    """

    def __init__(self, var_z="Hub_height", var_D="Rotor_diameter"):
        self.var_z = var_z
        self.var_D = var_D

    def __eval__(self, freq, locs_df, V0, wd):

        var_z = self.var_z
        var_D = self.var_D

        z = locs_df[var_z].values
        D = locs_df[var_D].values

        L1 = 5.67 * z * (z <= 60) + 340.2 * (z > 60)
        A = 12
        R = D / 2  # Rotor radius in [m]
        f0 = np.sqrt(2) * V0 / (A * R)
        f1 = 0.12 * V0 / L1

        return 1 / (
            1 + (np.sqrt(freq[:, na] ** 2 + f1[na, :] ** 2) / f0[na, :]) ** (4 / 3)
        ) ** (3 / 2)


class Soerensen_plant_Fwf_f(object):
    """
    Soerensen wind plant admittance based on the coherence function and the turbine locations

    Sørensen, Poul, Nicolaos Antonio Cutululis, Antonio Vigueras‐Rodríguez,
    Henrik Madsen, Pierre Pinson, Leo E. Jensen, Jesper Hjerrild, and Martin Donovan.
    (2008).
    Modelling of power fluctuations from large offshore wind farms.
    Wind Energy: An International Journal for Progress and Applications in Wind Power
    Conversion Technology 11, no. 1: 29-43.
    doi =  https://doi.org/10.1002/we.246

    Parameters
    ----------
    coherence: class
        Coherence object
    """

    def __init__(self, coherence):

        self.coherence = coherence

    def __eval(self, freq, locs_df, V0, wd):

        coherence = self.coherence

        N_locs = locs_df.values.shape[0]

        coherence_jk = coherence.__eval__(freq, locs_df, V0, wd)

        return (1 / (N_locs**2)) * np.sum(np.sum(coherence_jk, axis=2), axis=1)


def get_S_f(spectra, f):
    """
    Auxiliar function to evaluate multiple power density spectra objects

    Parameters
    ----------
    spectra: list
        List of power density spectra obejects

    f: array
        frequencies to be evaluated
    """

    for i_s, spect in enumerate(spectra):
        if i_s == 0:
            out = spect.__eval__(f)
        else:
            out += spect.__eval__(f)
    return out


def get_F_f(admittance, f):
    """
    Auxiliar function to evaluate multiple admittance objects

    Parameters
    ----------
    admittance: list
        List of admittance obejects

    f: array
        frequencies to be evaluated
    """

    for i_a, adm in enumerate(admittance):
        if i_a == 0:
            out = adm.__eval__(f)
        else:
            out *= adm.__eval__(f)
    return out


class Soerensen_circular_plant_Fwf_f(object):
    """
    Soerensen circular wind plant admittance approximation. It assumes infinite number of turbines uniformly distributed within a circular plant and Schlez coherence.

    Sørensen, Poul. Theory manual for CorWind

    Parameters
    ----------
    Alat: float
        Lateral coefficient

    Alon: float
        Longitudinal coefficient

    coherence_coeff_func: class
        coherence coefficienct calculator object, used if Alat or Alon are not given
    """

    def __init__(
        self,
        var_D_WF="WF_D_km",
        Alat=None,
        Alon=None,
        coherence_coeff_func=Schlez_coherence_coeff_Soerensen(),
    ):

        self.var_D_WF = var_D_WF
        self.Alat = Alat
        self.Alon = Alon
        self.coherence_coeff_func = coherence_coeff_func

    def __eval__(self, freq, locs_df, V0, wd):

        Alat = self.Alat
        Alon = self.Alon

        if (Alat is None) or (Alon is None):
            Alat, Alon = self.coherence_coeff_func.__eval__(V0=V0)

        if Alat >= Alon:
            Acoh = 0.529 * Alon + 0.436 * Alat + 0.035 * ((Alat * Alat) / Alon)
        else:
            Acoh = 0.529 * Alat + 0.436 * Alon + 0.035 * ((Alon * Alon) / Alat)

        locs_WF_D = locs_df[self.var_D_WF].values

        delta_cd = Acoh * locs_WF_D[na, :] * freq[:, na] / V0

        return np.exp(-delta_cd / 2) + 0.057 * delta_cd * np.exp(-0.23 * delta_cd)


def get_H_jk(
    ws,
    wd,
    n,
    fs,
    locs_df,
    spectra=[XGLarsen_S_f()],
    admittance=Soerensen_circular_plant_Fwf_f(),
    coherence=Schlez_coherence_jk_f(),
    return_all=False,
    atol=1e-12,
):
    """
    Function to compute the Cholevsky decomposition of the cross power density spectra.


    Sørensen, Poul, Anca D. Hansen, and Pedro André Carvalho Rosas.
    "Wind models for simulation of power fluctuations from wind farms."
    Journal of wind engineering and industrial aerodynamics 90.12-15 (2002): 1381-1402.
    https://doi.org/10.1016/S0167-6105(02)00260-X

    Parameters
    ----------
    ws: float
        Wind speed

    wd: float
        Wind direction

    n: int
        Number of steps. should be a power of 2 for better preformance.

    fs: float
        Sampling frequency

    locs_df: DataFrame
        Locations table including Lattitude, Longitude or x,y. And land use diameter.

    spectra: list
        List of power density spectra obejects

    admittance: list
        List of admittance obejects

    coherence: object
        Coherence object
    """

    timestep = 1 / fs
    freq = np.fft.rfftfreq(n, d=timestep)
    # freq = np.linspace(spectra[0].f0,fs,2**15)
    df = np.diff(freq)[0]
    N_freq = len(freq)

    coherence_jk = coherence.__eval__(freq, locs_df, V0=ws, wd=wd)

    S = get_S_f(spectra, freq)
    S = S * df

    if admittance is not None:
        F_f = admittance.__eval__(freq=freq, locs_df=locs_df, V0=ws, wd=wd)
    else:
        F_f = np.ones([N_freq, len(locs_df)])

    S_jk = coherence_jk * np.sqrt(
        F_f[:, na, :] * S[:, na, na] * F_f[:, :, na] * S[:, na, na]
    )

    H_jk = np.zeros_like(S_jk) + 0j
    for i_f in range(len(freq)):
        if np.allclose(
            np.abs(S_jk[i_f, :, :]), np.zeros_like(np.abs(S_jk[i_f, :, :])), atol=atol
        ):
            H_jk[i_f, :, :] = np.zeros_like(S_jk[i_f, :, :])
        else:
            try:
                H_jk[i_f, :, :] = np.linalg.cholesky(S_jk[i_f, :, :])
            except BaseException:
                H_jk[i_f, :, :] = np.zeros_like(S_jk[i_f, :, :])

    var = 2 * np.real(np.diag(np.sum(S_jk[:, :, :], axis=0)))

    if return_all:
        return H_jk, freq, var, S_jk, S
    else:
        return H_jk, freq, var


def get_time_series(H_jk, freq, var, n, seed=0):
    """
    Function to compute the time-series given the Cholevsky decomposition of the cross power density spectra.

    Sørensen, Poul, Anca D. Hansen, and Pedro André Carvalho Rosas.
    "Wind models for simulation of power fluctuations from wind farms."
    Journal of wind engineering and industrial aerodynamics 90.12-15 (2002): 1381-1402.
    https://doi.org/10.1016/S0167-6105(02)00260-X

    Parameters
    ----------
    Hjk: array
        Cholevsky decomposition of CSD: CSD = H_jk * H_jk.H

    freq: array
        Frequency array

    var: array
        Variance from the spectra for cheking the time-series

    n: int
        Number of steps. should be a power of 2 for better preformance.

    seed: int
        Random number generation's seed number
    """

    N_freq = len(freq)
    N_locs = H_jk.shape[2]

    # sample white noise
    # random phases
    np.random.seed(seed)
    phi_r = np.random.uniform(low=0.0, high=2 * np.pi, size=[N_freq, N_locs])

    V_freq = np.zeros_like(phi_r) + 0j
    for i_f in range(N_freq):
        V_freq[i_f] = np.dot(H_jk[i_f], np.exp(phi_r[i_f] * 1j))

    # 2 * V_freq because ifft takes the sum of positive
    # and negative frequency values for n equal a power of 2
    V_t = np.fft.ifft(2 * V_freq, n, axis=0)
    var_V_t = np.var(V_t, axis=0)
    scale_v_t = np.sqrt(var / var_V_t)

    # print('scale_v_t: ', scale_v_t)

    return np.real(scale_v_t * V_t)


def to_multivariate_t_student(V_t, df=4):
    """
    Function that transform Gaussian fluctuations into t-distributed with df, using Cholevsky decomposition to transform the Multi-variate Gaussian to independent Gaussian then transforms to independent t-student and then back to correlated t-student.

    Parameters
    ----------
    V_t: array
        Time series on multiple locations. V_t.shape = N_locs, N_times

    df: float
        Degrees of freedom in the t-student distribution
    """

    x = np.real(V_t).T
    mu = np.mean(x, axis=1)[:, na]

    LA = np.linalg.cholesky(np.cov(x))
    # LA = np.dot(U,(S**0.5)*np.eye(len(S)))
    LA_inv = np.linalg.inv(LA)
    Z_t = np.dot(LA_inv, x - mu).T
    UZ_t = sp.stats.norm.cdf(Z_t)
    Ut_t = sp.stats.distributions.t.ppf(UZ_t, df=df)
    std_of_t = sp.stats.distributions.t.std(df)
    Vt_t = (np.dot(LA, Ut_t.T * 1 / std_of_t) + mu).T

    # This is the equivalent definition of multivariate t
    # Ut_t_v2 = Z_t / np.sqrt(sp.stats.gamma.rvs(a=4/2, size=UZ_t.shape))
    # Vt_t_v2 = ( np.dot(LA, Ut_t_v2.T) + mu ).T

    return Vt_t


def to_marginal_t_student(V_t, df=4, lim_low=None, lim_high=None):
    """
    Function that transform Gaussian fluctuations into marginally t-distributed with df.

    Parameters
    ----------
    V_t: array
        Time series on multiple locations. V_t.shape = N_locs, N_times

    df: float
        Degrees of freedom in the t-student distribution

    lim_low: float
        Simulated fluctuations lower than this will be forced to this value

    lim_high: float
        Simulated fluctuations higher than this will be forced to this value
    """
    x = np.real(V_t).T
    mu = np.mean(x, axis=1)[:, na]
    sigma = np.std(x, axis=1)[:, na]
    Z_tmarg_A = (x - mu) / sigma
    UZ_tmarg_A = sp.stats.norm.cdf(Z_tmarg_A)
    std_of_t = sp.stats.distributions.t.std(df)
    Vt_tmarg_A = (
        sp.stats.distributions.t.ppf(UZ_tmarg_A, df=df) * sigma / std_of_t + mu
    ).T

    if lim_low is not None:
        Vt_tmarg_A[Vt_tmarg_A < lim_low] = lim_low

    if lim_high is not None:
        Vt_tmarg_A[Vt_tmarg_A > lim_high] = lim_high

    return Vt_tmarg_A


def get_distance(locs_df):
    """
    Function that computes distances.
    """
    if ("x" in locs_df.columns) & ("y" in locs_df.columns):

        locs = locs_df.loc[:, ["x", "y"]].values
        distance = sp.spatial.distance_matrix(locs, locs)

    else:
        locs = locs_df.loc[:, ["Longitude", "Latitude"]].values
        locs_1 = np.radians(locs)
        locs_2 = np.radians(locs)
        R = 6373.0 * 1e3

        dif_ij = locs_2[na, :] - locs_1[:, na]

        lat1 = locs_1[na, :, 1]
        lat2 = locs_2[na, :, 1]
        dlon = dif_ij[:, :, 0]
        dlat = dif_ij[:, :, 1]

        a = np.sin(dlat / 2) ** 2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon / 2) ** 2
        c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))

        distance = R * c

    return distance


def find_same_locs(rows_droped, locs_red):
    """
    Function that detects locations with the same position and returns a table with index of locs to use (first of the duplicated locations) to fill in the dropped duplicates
    """

    used_locs = []
    drop_locs = []
    for iter1 in rows_droped.index:
        drop_locs.append(iter1)
        mask = (locs_red.Latitude == rows_droped["Latitude"].loc[iter1]) & (
            locs_red.Longitude == rows_droped["Longitude"].loc[iter1]
        )
        used_locs.append(locs_red.loc[mask].index.values[0])
    duplicate_locs = pd.DataFrame()
    duplicate_locs["use_loc"] = used_locs
    duplicate_locs["drop_loc"] = drop_locs

    return duplicate_locs


def extend_fluctuations(Vt_t, nt, duplicate_locs, locs_red):
    """
    Function that extends a multivariate fluctuation array in time to match the desired simulation length and concatenates the time series from the duplicated/droped locations.
    """

    n = Vt_t.shape[0]
    nlocs = Vt_t.shape[1]
    # nt = len(ts)
    nlocs_out = nlocs + len(duplicate_locs)

    Vt_t = np.resize(Vt_t, [nt, nlocs])

    if len(duplicate_locs) > 0:
        Vt_t_ext = np.zeros([nt, nlocs_out])

        Vt_t_ext[:, locs_red.index.to_list()] = Vt_t
        Vt_t_ext[:, duplicate_locs.drop_loc.values] = Vt_t[
            :, duplicate_locs.use_loc.values
        ]

        return Vt_t_ext
    else:
        return Vt_t


def get_wf_i(points, i):
    """
    Linear interpolation in vectorial approach.

    This function returns the weight function (wf) given a set of points, with only one of the points i active (y[i]=1) while the output is zero in all other points.
    """
    if i < len(points):

        y = np.zeros_like(points)
        y[i] = 1

        return lambda x: np.interp(x, points, y, left=0, right=0)
    else:
        return lambda x: np.nan * x


alphabet_string = string.ascii_lowercase
alphabet_list = list(alphabet_string)


def find(array, x):
    """
    Auxiliar find function with checks and consistent return types.
    """
    try:
        find = np.where(array == x)[0]
        if len(find) > 0:
            return find[0]
        else:
            return -100
    except BaseException:
        return -100


def interpolate_look_up_multidims(LK, LK_x, x, ein_str, verbose=False):
    """
    Linear interpolation in vectorial approach in multiple dimensions with broadcasting of additional dimensions

    Parameters
    ----------
    LK: array
        Output of the look-up table in multiple dimensions

    LK_x: array
        Inputs values of the look up tables. The shares dimensions with LK will be boradcasted.

    x: array
        Input array for evaluation

    ein_str: str
        Einstein string to ensure vectorial multiplication of the precomputed weights.
        For help run hte function once with verbose=True to obtain shpe information.

    verbose: boolean
        Verbose flag. True will print shapes of LK, LK_x, broadcast dimensions, and shape of the interpolation weights.

    """
    if verbose:
        print("LK.shape: ", LK.shape)
        print("x.shape: ", x.shape)

    n_dims_to_broadcast = np.max(
        [find(np.array(x.shape), LK.shape[i]) + 1 for i in range(len(LK.shape))]
    )
    broadcast_shape = np.array(x.shape[:n_dims_to_broadcast])
    if verbose:
        print("n_dims_to_broadcast:", n_dims_to_broadcast, " of size ", broadcast_shape)

    ND = x.shape[-1]
    nD = np.array(LK.shape)[-ND:]

    if verbose:
        print("ND: ", ND)
        print("nD: ", nD)

    weight_d = [
        np.stack([get_wf_i(LK_x[d], i=i)(x[:, :, d]) for i in range(nD[d])], axis=-1)
        for d in range(ND)
    ]

    if verbose:
        for d in range(ND):
            print(f"weight_d[{d}].shape:", weight_d[d].shape)

    y = np.einsum(ein_str, *weight_d, LK)
    if verbose:
        print("y.shape: ", y.shape)
    return y


def look_up_eval_timeseries(
    wd,
    n,
    fs,
    nt,
    duplicate_locs,
    locs_red,
    ds_interp,
    seed=0,
    std_factor=1,
    spectra=[XGLarsen_S_f()],
    admittance=Soerensen_circular_plant_Fwf_f(),
    coherence=Schlez_coherence_jk_f(),
):
    """
    Function for look_up table based generation of timeseries, given changes in WS in the evaluation of spectra, std and coherence.

    Parameters
    ----------

    wd: float
        Wind direction

    n: int
        Number of steps. should be a power of 2 for better preformance.

    fs: float
        Sampling frequency

    seed: int
        Random number generation's seed number

    spectra: list
        List of power density spectra obejects

    admittance: list
        List of admittance obejects

    coherence: object
        Coherence object
    """
    V_fluct = []
    WS_look_up = [6, 8, 16]
    for ws in WS_look_up:
        H_jk, freq, var, S_jk, S = get_H_jk(
            ws=ws,
            wd=wd,
            n=n,
            fs=fs,
            locs_df=locs_red,
            spectra=spectra,
            admittance=admittance,
            coherence=coherence,
            return_all=True,
        )
        V_t = get_time_series(H_jk, freq, var, n, seed)
        Vt_fluct = extend_fluctuations(
            V_t * ws / 8 * std_factor, nt, duplicate_locs, locs_red
        )
        V_fluct += [Vt_fluct]

    V_fluct = np.stack(V_fluct)

    WS_fluc_look_up = xr.Dataset()
    WS_fluc_look_up["WS_fluc_look_up"] = xr.DataArray(
        dims=["WS", "time", "locs_ID"],
        data=np.stack(V_fluct),
        coords={
            "WS": WS_look_up,
            "time": ds_interp.time.values,
            "locs_ID": ds_interp.locs_ID.values,
        },
    ).transpose("time", "locs_ID", "WS")
    ws_aux = WS_fluc_look_up.isel(WS=0)
    ws_aux["WS_fluc_look_up"] = 0 * ws_aux["WS_fluc_look_up"]
    ws_aux["WS"] = 0

    ws_aux_2 = WS_fluc_look_up.isel(WS=0)
    ws_aux_2["WS_fluc_look_up"] = 50 / 8 * std_factor * ws_aux_2["WS_fluc_look_up"]
    ws_aux_2["WS"] = 50

    WS_fluc_look_up = xr.concat([ws_aux, WS_fluc_look_up, ws_aux_2], dim="WS")

    LK = WS_fluc_look_up.WS_fluc_look_up.values
    LK_x = [WS_fluc_look_up.WS.values]
    x = ds_interp.WS.values[..., np.newaxis]

    ein_str = "abi,abi->ab"
    y = interpolate_look_up_multidims(LK, LK_x, x, ein_str)

    return y


def near_psd(A):
    """Find the nearest positive-definite matrix to input

    https://stackoverflow.com/questions/43238173/python-convert-matrix-to-positive-semi-definite/43244194

    A Python/Numpy port of John D'Errico's `nearestSPD` MATLAB code [1], which
    credits [2].

    [1] https://www.mathworks.com/matlabcentral/fileexchange/42885-nearestspd

    [2] N.J. Higham, "Computing a nearest symmetric positive semidefinite
    matrix" (1988): https://doi.org/10.1016/0024-3795(88)90223-6
    """

    B = (A + A.T) / 2
    _, s, V = la.svd(B)

    H = np.dot(V.T, np.dot(np.diag(s), V))

    A2 = (B + H) / 2

    A3 = (A2 + A2.T) / 2

    if isPD(A3):
        return A3

    spacing = np.spacing(la.norm(A))
    # The above is different from [1]. It appears that MATLAB's `chol` Cholesky
    # decomposition will accept matrixes with exactly 0-eigenvalue, whereas
    # Numpy's will not. So where [1] uses `eps(mineig)` (where `eps` is Matlab
    # for `np.spacing`), we use the above definition. CAVEAT: our `spacing`
    # will be much larger than [1]'s `eps(mineig)`, since `mineig` is usually on
    # the order of 1e-16, and `eps(1e-16)` is on the order of 1e-34, whereas
    # `spacing` will, for Gaussian random matrixes of small dimension, be on
    # othe order of 1e-16. In practice, both ways converge, as the unit test
    # below suggests.
    I = np.eye(A.shape[0])
    k = 1
    while not isPD(A3):
        mineig = np.min(np.real(la.eigvals(A3)))
        A3 += I * (-mineig * k**2 + spacing)
        k += 1

    return A3


def isPD(B):
    """Returns true when input is positive-definite, via Cholesky"""
    try:
        _ = la.cholesky(B)
        return True
    except la.LinAlgError:
        return False
