# auxiliar system
import glob
import itertools
import os
import shutil
import time
from pathlib import Path

import numpy as np
import xarray as xr
import yaml


def removefile(fn, verbose=True):
    if os.path.exists(fn):
        try:
            shutil.rmtree(fn)
        except BaseException:
            if verbose:
                print("Could not remove file", fn)


def clean(path):
    list_files = glob.glob(path)
    for fn in list_files:
        removefile(fn)


def split_in_batch(lst, n_batch):
    return np.array_split(lst, int(np.ceil(len(lst) / n_batch)))


def flatten_nested_lists(nested):
    return sorted(list(itertools.chain(*nested)))


def mkdir(dir_):
    if str(dir_).startswith("~"):
        dir_ = str(dir_).replace("~", os.path.expanduser("~"))
    try:
        os.stat(dir_)
    except BaseException:
        try:
            os.mkdir(dir_)
            # Path(dir_).mkdir(parents=True, exist_ok=True)
        except BaseException:
            pass
    return dir_


def last_path(full):
    """
    Function to extract the filename or last folder in a full path
    """
    return os.path.basename(os.path.normpath(full))


def RepresentsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def get_shortname(fn):
    """
    Return filename without file type ending
    """
    if fn is None:
        return None
    else:
        return Path(fn).name.split(".")[0]


def return_element(lst, i):
    """
    Return an element of a list including empty lists
    """
    if len(lst) == 0:
        return None
    elif i > len(lst):
        return None
    else:
        return lst[i]


def split(string, separators):
    """
    Split a string with a list of separators
    """
    string_out = string
    for sep in separators:
        string_out = string_out.replace(sep, " ")
    return string_out.split()


def make_date_string(fn, meso_dir_template):
    """
    Returns consistent date_string out of a filename with date data given the
    meso_dir_template
    """
    separators = split(get_shortname(meso_dir_template), ["{year}", "{month}", "{day}"])
    date_order = split(get_shortname(meso_dir_template), separators)

    date_dict = {}
    date_read = [a for a in split(get_shortname(fn), separators)]
    for i, var in enumerate(date_order):
        date_dict[var] = date_read[i]
    date_dict

    return "-".join([date_dict[var] for var in date_order])


def replace_str(string, var_dict):
    if isinstance(string, str):
        for key, value in var_dict.items():
            if ("var" in key) & ("var_" not in key):
                string = string.replace(key, value)
    return string


def replace_element(lst, var_dict):
    if isinstance(lst, list):
        return [replace_str(var, var_dict) for var in lst]
    else:
        return replace_str(lst, var_dict)


def remove_empty(lst):
    if isinstance(lst, list):
        out_lst = [el for el in lst if el != ""]
        return out_lst
    else:
        if lst == "":
            return None
        else:
            return lst


def replace_empty_list(lst):
    if isinstance(lst, list):
        if lst == []:
            return None
        else:
            return lst
    else:
        if lst == "":
            return None
        else:
            return lst


def replace_elements_in_dic(dct, var_dict):
    for key, value in dct.items():
        if isinstance(value, dict):
            for key_1, value_1 in dct[key].items():
                dct[key][key_1] = remove_empty(replace_element(value_1, var_dict))
        else:
            dct[key] = remove_empty(replace_element(value, var_dict))
    return dct


def replace_empty_list_in_dic(dct):
    for key, value in dct.items():
        if isinstance(value, dict):
            for key_1, value_1 in dct[key].items():
                dct[key][key_1] = replace_empty_list(dct[key][key_1])
        else:
            dct[key] = replace_empty_list(dct[key])
    return dct


def extract_simulation_requirements(config, simulation, dataset):

    dict_sim = config["simulation"]
    var_dict = config["weather_data"][dataset]

    # replaces the variables in the reanalisys dataset into the
    # interpolation dictionary
    dict_sim = replace_elements_in_dic(dict_sim, var_dict)

    vars_xy_logz = remove_empty(dict_sim[simulation]["vars_xy_logz"])
    vars_xyz = remove_empty(dict_sim[simulation]["vars_xyz"])
    vars_xy = remove_empty(dict_sim[simulation]["vars_xy"])
    vars_nearest_xy = remove_empty(dict_sim[simulation]["vars_nearest_xy"])
    vars_nearest_xyz = remove_empty(dict_sim[simulation]["vars_nearest_xyz"])

    vars_needed = vars_xy_logz + vars_xyz + vars_xy + vars_nearest_xy + vars_nearest_xyz

    params_sim = replace_empty_list_in_dic(dict_sim[simulation])

    if simulation == "CSP":
        # this line probably holds for all simulation not only CSP (to be
        # checked!!)
        params_sim["vars_xy_logz"] = vars_xy_logz

    params_sim["vars_xyz"] = vars_xyz
    params_sim["vars_xy"] = vars_xy
    params_sim["vars_nearest_xy"] = vars_nearest_xy
    params_sim["vars_nearest_xyz"] = vars_nearest_xyz
    params_sim["vars_needed"] = vars_needed

    dict_to_app = replace_empty_list_in_dic(var_dict)

    for key, value in dict_to_app.items():
        params_sim[key] = value

    return params_sim


def extract_n_workers_and_memory(client):
    n_workers = int(str(client.__dict__["cluster"]).split("workers=")[1].split(",")[0])
    if n_workers == 0:
        memory_available = 0.0
    else:
        memory_available = (
            0.5
            * float(str(client.__dict__["cluster"]).split("memory=")[1].split()[0])
            * 1e9
            / n_workers
        )  # in Bytes
    return n_workers, memory_available


def get_n_workers_and_memory(client, N_max_tries=5):
    # Try and wait for the workers to land
    n_workers, memory_available = extract_n_workers_and_memory(client)
    # for approx 5 mins of trying
    i_try = 0
    while (i_try <= N_max_tries) & (n_workers == 0):
        time.sleep(1 * 60)  # wait one minute for the workers to land and retry
        n_workers, memory_available = extract_n_workers_and_memory(client)
        i_try += 1

    if n_workers == 0:
        message = "\n\n\nWorkers took to long to start, possibly beacuse the server is busy. Please try again later. In general the server is less busy after working hours.\n\n\n"

        client.close()

        print(message)
        raise Exception(message)

    return n_workers, memory_available


def get_n_cores(client):
    return int(str(client.__dict__["cluster"]).split("threads=")[1].split(",")[0])


def try_n_times(n_times=3):
    def tryIt(func):
        def f():
            attempts = 0
            while attempts < n_times:
                try:
                    return func()
                except BaseException:
                    attempts += 1

        return f

    return tryIt


def watch_dog(file_path, N_max_tries=120):
    # for approx 2 mins of trying
    i_try = 0
    while (i_try <= N_max_tries) & (not os.path.exists(file_path)):
        time.sleep(1)  # wait one second
        i_try += 1

    if os.path.isfile(file_path):
        pass
    else:
        raise ValueError("%s isn't a file!" % file_path)


def valid_netcdf(file_path, var_check="Time", min_count=24):
    if not os.path.exists(file_path):
        return False
    else:
        try:
            with xr.open_dataset(file_path) as ds:
                if len(ds[var_check]) >= min_count:
                    return True
                else:
                    return False

        except BaseException:
            return False


def valid_netcedf_error(file_path, var_check="Time", min_count=24):
    if not os.path.exists(file_path):
        return False
    else:
        try:
            with xr.open_dataset(file_path) as ds:
                if len(ds[var_check]) >= min_count:
                    return True
                else:
                    return False

        except BaseException:
            raise Exception(f"writing file error for {file_path}")


def watch_dog_netcdf(file_path, N_max_tries=120, var_check="Time", min_count=24):

    # for approx 2 mins of trying
    i_try = 0
    while (i_try <= N_max_tries) & valid_netcdf(file_path, var_check, min_count):
        time.sleep(1)  # wait one second
        i_try += 1

    return valid_netcedf_error(file_path, var_check, min_count)


def offset_timeseries(df, offset=-30, unit="s"):
    delta = int(2 * np.abs(offset))
    df_out = (
        df.resample(f"{delta}{unit}", offset=f"{offset}{unit}")
        .interpolate("time")
        .resample(f"{int(delta/2)}{unit}")
        .interpolate("time")
        .resample(f"{delta}{unit}")
        .interpolate("time")
        .reindex(df.index)
        .interpolate("linear", limit_area="outside")
    )  # to fill in the NaN in last step.

    return df_out


def get_bin_specific_power(df, bin_range):

    if bin_range["max"] is not None:
        mask = (df["specific_power"] > bin_range["min"]) & (
            df["specific_power"] <= bin_range["max"]
        )
    else:
        mask = df["specific_power"] > bin_range["min"]
    df_masked = df.loc[mask]

    return df_masked, mask


# def get_bins(stall_locs):
#     bins_edges, _ = pd.qcut(stall_locs["specific_power"], q=4, labels=False, retbins=True)
#     bins=[{"min":bins_edges[0] , "max":bins_edges[1]},
#             {"min":bins_edges[1] , "max":bins_edges[2]},
#             {"min":bins_edges[2] , "max":bins_edges[3]},
#             {"min":bins_edges[3] , "max": bins_edges[4]}]
#     return bins
