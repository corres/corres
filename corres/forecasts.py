import glob
import time

import geopy.distance
import numpy as np
import pandas as pd
import xarray as xr
from statsmodels.stats.moment_helpers import corr2cov

# from corres.WPP import computes_generation_WPP
# from corres.auxiliar_functions import clean, mkdir


def get_distMatrix(lats, lons):
    dist_matrix = np.empty((len(lats), len(lats)))
    dist_matrix[:] = np.nan
    for i in range(0, len(lats)):
        for j in range(0, len(lats)):
            coords_i = [
                lats[i],
                lons[i],
            ]
            coords_j = [
                lats[j],
                lons[j],
            ]
            dist_ij = geopy.distance.distance(coords_i, coords_j).km
            dist_matrix[i, j] = dist_ij
    return dist_matrix


def get_spatCorMatrix(dist_matrix, Lambda_in_km):
    Cor_matrix = np.empty(dist_matrix.shape)
    Cor_matrix[:] = np.nan
    for i in range(0, len(dist_matrix)):
        for j in range(0, len(dist_matrix)):
            Cor_matrix[i, j] = np.exp(-(dist_matrix[i, j] / Lambda_in_km))
    return Cor_matrix


def sim_ARMA11(
    A1, B1, Cov_u, T_sim, extraHorizon_nSteps
):  # With Gaussian error/innovation term
    d = len(A1)

    u_sim = np.random.multivariate_normal(
        np.zeros(d), Cov_u, T_sim + 1 + extraHorizon_nSteps
    )
    u_sim = u_sim.T

    y_sim = np.empty((T_sim + 1 + extraHorizon_nSteps, d))
    y_sim[:] = np.nan
    y_sim = y_sim.T

    # time 0
    u_sim[:, 0] = np.zeros((d, 1)).T
    y_sim[:, 0] = np.zeros((d, 1)).T

    # time 1 ->
    for t in range(1, T_sim + 1 + extraHorizon_nSteps):
        y_sim[:, t] = (
            np.dot(A1, y_sim[:, t - 1]) + u_sim[:, t] + np.dot(B1, u_sim[:, t - 1])
        )

    y_sim = y_sim.T
    u_sim = u_sim.T

    return y_sim[1 + extraHorizon_nSteps :, :], u_sim[1 + extraHorizon_nSteps :, :]


class forecRun(object):
    # This object holds info about forecast error run (e.g., DA or HA)
    def __init__(
        self,
        name="DA",
        # first_forecast_update = None,
        updateFreq=24,  # In hours (24 = DA)
        # In hours (time delay between when the forecast is made and
        # when it is actually used)
        leadTime=0,
    ):
        # self.first_forecast_update = first_forecast_update
        self.updateFreq = updateFreq
        self.leadTime = leadTime
        self.name = name
        self.df_forec_updateTimes = None
        self.sim_data = None

    def __str__(self):
        return (
            self.name
            + ": Update freq="
            + str(self.updateFreq)
            + "h, Lead time="
            + str(self.leadTime)
            + "h"
        )

    def __repr__(self):
        return (
            self.name
            + ": Update freq="
            + str(self.updateFreq)
            + "h, Lead time="
            + str(self.leadTime)
            + "h"
        )

    def add_simData(self, df_sim):
        if self.sim_data is not None:
            self.sim_data = pd.concat([self.sim_data, df_sim])
        else:
            self.sim_data = df_sim


class ARMA11_wind_forecSim(object):
    # This object handles simulation of ARMA(1,1) based forecast error simulation
    # Specs:
    # 1) (default) As in CorWind manual / C# code
    # a1 = 0.99
    # b1 = 0.4069531
    # SD_point_base = 0.2893359 # this when D_area = 0
    # Lambda_in_km = 50
    # 2) As in paper by Lennart / paper by Boone
    # a1 = 0.97
    # b1 = -0.38
    # SD_point_base = 1.31 # this when D_area = 0
    # Lambda_in_km = 50
    def __init__(
        self,
        a1=0.99,  # This goes to the diagonal of A1 (all else is zero)
        b1=0.4069531,
        # This goes to the diagonal of B1 (all else is zero)
        SD_point_base=0.2893359,  # When D_area = 0
        Lambda_in_km=50,  # Used in specifiying the error/innovation term Cor matrix
        MET_update_freq=6,
        seed=1,  # Random seed
        # first_MET_update = None # NOTE: Changing this should be
        # supported in the future
    ):
        self.a1 = a1
        self.b1 = b1
        self.SD_point_base = SD_point_base
        self.Lambda_in_km = Lambda_in_km
        self.MET_update_freq = MET_update_freq
        self.MET_maxHorizon = None
        self.first_MET_update = None
        self.first_forecast_update = None
        self.seed = seed
        self.locs = None
        self.df_dist_matrix = None
        self.Cor_u = None
        self.SD_u = None
        self.Cov_u = None
        self.df_ws = None
        # This is the entire simulated forecast range (can be longer then the
        # meso data range)
        self.t_sim = None
        # self.df_wd = None
        self.ds_meso = None  # This is the full mesodata in xarray
        self.df_MET_updateTimes = None
        self.df_MET_sim_err_all = None
        self.forecRuns = []
        self.xarray_locs_ID = None
        self.xarray_time = None

    def __str__(self):
        return (
            "a1="
            + str(self.a1)
            + ", b1="
            + str(self.b1)
            + ", lambda="
            + str(self.Lambda_in_km)
            + ", MET update freq="
            + str(self.MET_update_freq)
            + "h, Point SD="
            + str(self.SD_point_base)
        )

    def __repr__(self):
        return (
            "a1="
            + str(self.a1)
            + ", b1="
            + str(self.b1)
            + ", lambda="
            + str(self.Lambda_in_km)
            + ", MET update freq="
            + str(self.MET_update_freq)
            + "h, Point SD="
            + str(self.SD_point_base)
        )

    def add_forecRun(self, fRun):
        self.forecRuns.append(fRun)
        required_horizon = (
            fRun.updateFreq + fRun.leadTime + self.MET_update_freq
        )  # In the forecast simulation
        # print(required_horizon)
        if self.MET_maxHorizon is None:
            self.MET_maxHorizon = required_horizon
        elif required_horizon > self.MET_maxHorizon:
            self.MET_maxHorizon = required_horizon

    def set_locs(self, locs):
        self.locs = locs
        dist_matrix = get_distMatrix(locs["Latitude"], locs["Longitude"])
        self.df_dist_matrix = pd.DataFrame(
            data=dist_matrix, index=locs.index, columns=locs.index
        )
        self.Cor_u = get_spatCorMatrix(
            self.df_dist_matrix.to_numpy(), self.Lambda_in_km
        )

        # Scale error/innovation term SDs using D_area:
        area_km2 = locs["Area_km2"]  # must be in km^2!
        D_area = (np.sqrt(area_km2 / np.pi)) * 2  # D_area = diameter
        SD_u = np.empty(len(D_area))
        D_area_SD_reduct_fact = np.empty(len(D_area))

        for i in range(0, len(D_area)):
            D_Area_rel_i = D_area[i] / self.Lambda_in_km

            # NOTE: This is  hard coded, as in CorWind (should be changed, user
            # should be able to define something else)
            D_area_SD_reduct_fact[i] = np.exp(
                -D_Area_rel_i / 2
            ) + 0.057 * D_Area_rel_i * np.exp(-0.23 * D_Area_rel_i)

            SD_u[i] = np.sqrt(D_area_SD_reduct_fact[i]) * self.SD_point_base
        self.SD_u = SD_u
        self.Cov_u = corr2cov(self.Cor_u, self.SD_u)

    def load_mesoTimeser(self, temp_dir):
        file_list = sorted(glob.glob(temp_dir + "ds_meso_*.nc"))
        if len(file_list) > 0:
            ds = xr.open_mfdataset(file_list, combine="nested", concat_dim="locs_ID")
        else:
            ds = xr.open_dataset(file_list[0])

        self.xarray_time = ds.time.values
        self.xarray_locs_ID = ds.locs_ID

        self.ds_meso = ds

        # Save the WS data in dataframe (NOTE: NOT EFFICIENT FOR LARGE RUNS ->
        # SHOULD BE CHANGED TO BE FULLY IN XARRAY)
        ds_ws = ds.WS
        df_ws = ds_ws.to_dataframe()
        df_ws = df_ws.reset_index(1)
        df_ws = df_ws.pivot(columns="locs_ID", values="WS")
        df_ws = df_ws.reindex(
            sorted(df_ws.columns), axis=1
        )  # to make sure columns are in numerical order
        self.df_ws = df_ws

        # Start the simulations a few days before the 1st meso data time stamp
        # to get forecasts going from the 1st meso data day
        self.t_sim = pd.date_range(
            start=df_ws.index[0] - pd.Timedelta("5 days"),
            end=df_ws.index[-1],
            freq="1H",
        )

        # NOTE: It is assumed that the first time step is the 1st forecast
        # update time! (this should be changed at some point)
        self.first_MET_update = self.t_sim[0]  # Give in hourly reso
        # Assumed to be the same for all HRs
        self.first_forecast_update = self.t_sim[0]

        # Set up forec update times:
        t = self.t_sim
        first_indx = np.argwhere(t == self.first_MET_update)[0, 0]
        MET_updateTimes = np.zeros(len(t))
        for i in range(first_indx, len(t), self.MET_update_freq):
            MET_updateTimes[i] = 1
        df_MET_updateTimes = pd.DataFrame(MET_updateTimes, index=t)
        self.df_MET_updateTimes = df_MET_updateTimes

    def set_forecUpdateTimes(self):
        # Loop all HRs:
        MET_updateTimes_asTimeStamps = self.df_MET_updateTimes.index[
            self.df_MET_updateTimes[0] == 1
        ]

        for h in range(0, len(self.forecRuns)):
            # print(self.forecRuns[h])

            HR_updateFreq_h = self.forecRuns[h].updateFreq
            # print(HR_updateFreq_h)

            HR_leadTime_h = self.forecRuns[h].leadTime
            # print(HR_leadTime_h)

            t_sim = self.t_sim

            first_indx = np.argwhere(t_sim == self.first_MET_update)[0, 0]

            first_indx_h = np.argwhere(t_sim == self.first_forecast_update)[0, 0]
            forec_updateTimes_h = np.zeros(len(t_sim))
            for i in range(first_indx, len(t_sim), HR_updateFreq_h):
                forec_updateTimes_h[i] = 1

            df_forec_updateTimes_h = pd.DataFrame(
                forec_updateTimes_h, index=t_sim, columns=["UpdateTime"]
            )
            df_forec_updateTimes_h["METupdateTimes"] = self.df_MET_updateTimes.iloc[
                :, 0
            ]

            latest_available_forecast = np.empty(len(t_sim), dtype=pd.DatetimeIndex)
            latest_available_forecast[:] = np.nan

            latest_available_update = None
            for i in range(first_indx_h, len(t_sim)):
                t_i = t_sim[i]

                if (
                    df_forec_updateTimes_h.loc[t_i, "UpdateTime"] == 1
                ):  # -> We consider a MET update
                    # Find closes MET update (but far-enough in the past,
                    # considering lead time):
                    possible_times_i = MET_updateTimes_asTimeStamps[
                        MET_updateTimes_asTimeStamps
                        <= (t_i - pd.Timedelta(HR_leadTime_h, unit="h"))
                    ]  # MAY BE INEFFICIENT FOR LONG TIME SERIES??
                    t_diff_i = np.array(t_i - possible_times_i)
                    t_diff_i = t_diff_i / np.timedelta64(1, "h")  # To hours
                    if len(t_diff_i) > 0:
                        indx_i = np.argmin(t_diff_i)
                        latest_available_update = possible_times_i[indx_i]

                latest_available_forecast[i] = latest_available_update

            df_forec_updateTimes_h["LatestAvailMETforec"] = latest_available_forecast
            self.forecRuns[h].df_forec_updateTimes = df_forec_updateTimes_h

    def get_one_month(self, year, month):
        A1 = self.a1 * np.eye(len(self.locs))
        B1 = self.b1 * np.eye(len(self.locs))

        DICT_MET_sim_err_all = {}

        t_all = self.t_sim
        t = t_all[np.logical_and(t_all.year == year, t_all.month == month)]

        MET_updates_this_month = self.df_MET_updateTimes.loc[t]
        MET_updates_this_month = MET_updates_this_month[
            MET_updates_this_month.iloc[:, 0].values == 1
        ]

        for t_i in MET_updates_this_month.index:
            # print(t_i)

            # THE FIRST time stamp IS A 1 STEP AHEAD FORECAST
            y_sim_i, u_sim_i = sim_ARMA11(A1, B1, self.Cov_u, self.MET_maxHorizon, 0)

            t_sim_i = pd.date_range(
                t_i, t_i + pd.Timedelta(hours=self.MET_maxHorizon - 1), freq="1h"
            )

            df_MET_sim_err_i = pd.DataFrame(
                data=y_sim_i, index=t_sim_i, columns=self.locs.index
            )
            DICT_MET_sim_err_all[t_i] = df_MET_sim_err_i

        df_MET_sim_err_all = pd.concat(DICT_MET_sim_err_all)
        df_MET_sim_err_all.index.names = ["forecMadeAt", "forecFor"]

        return df_MET_sim_err_all, t

    # RUN THIS ONLY AS A PART OF A FULL SIMULATION
    def run_one_month(self, year, month, prevMonth):
        [df_MET_sim_err_all, t] = self.get_one_month(year, month)

        if prevMonth is not None:
            df_MET_sim_err_all = pd.concat([prevMonth, df_MET_sim_err_all])

        for h in range(0, len(self.forecRuns)):
            # print(self.forecRuns[h].name)
            df_forec_updateTimes_h = self.forecRuns[h].df_forec_updateTimes

            data_all_h = np.empty([len(t), len(self.locs.index)])
            data_all_h[:] = np.nan

            for i in range(0, len(t)):
                t_i = t[i]  # Time step analysed at i
                # t_MET_i = df_forec_updateTimes_h['LatestAvailMETforec'][i] # Latest forec date avaiable for i
                # print(t_i)
                # Latest forec date avaiable for time i
                t_MET_i = df_forec_updateTimes_h.at[t_i, "LatestAvailMETforec"]

                if not pd.isnull(t_MET_i):  # if some forecast available
                    # print(t_MET_i)
                    data_all_h[i, :] = df_MET_sim_err_all.loc[(t_MET_i, t_i), :]

            df_sim_h = pd.DataFrame(data=data_all_h, index=t, columns=self.locs.index)
            self.forecRuns[h].add_simData(df_sim_h)
        return df_MET_sim_err_all

    def run_all(self):
        np.random.seed(self.seed)

        # Clean all data
        for h in range(0, len(self.forecRuns)):
            self.forecRuns[h].sim_data = None

        # months = np.unique(self.df_ws.index.month)
        # years = np.unique(self.df_ws.index.year)

        month_year_combs = np.unique([self.t_sim.year, self.t_sim.month], axis=1).T

        # prevMonth = None
        # for y in years:
        #     # print(y)
        #     for m in months:
        #         # print(m)
        #         #print(str(y) + "  " + str(m))
        #         prevMonth = self.run_one_month(y, m, prevMonth)

        prevMonth = None
        for month_year_comb in month_year_combs:
            # print(month_year_comb)
            prevMonth = self.run_one_month(
                month_year_comb[0], month_year_comb[1], prevMonth
            )

        # Remove the extra time steps in the beginning
        for h in range(0, len(self.forecRuns)):
            self.forecRuns[h].sim_data = self.forecRuns[h].sim_data.loc[
                self.df_ws.index
            ]
            self.forecRuns[h].df_forec_updateTimes = self.forecRuns[
                h
            ].df_forec_updateTimes.loc[self.df_ws.index]

    def save_to_netcdf(self, temp_dir):
        # To forecast from forecast errors (y_hat = y - e)
        HR_forec_withNegat = []
        HR_forec = []  # Negative values simply foreced to zero
        for h in range(0, len(self.forecRuns)):
            # print(self.forecRuns[h].name)

            df_h_withNegat = self.df_ws - self.forecRuns[h].sim_data
            HR_forec_withNegat.append(df_h_withNegat)

            df_h = df_h_withNegat.copy()
            df_h[df_h < 0] = 0
            HR_forec.append(df_h)

        for h in range(0, len(self.forecRuns)):
            fileName_i = temp_dir + self.forecRuns[h].name + "_forecast.nc"
            df_i = HR_forec[h]
            df_i = df_i.stack()  # Stack so can be added to the xarray dataset
            ds_i = self.ds_meso  # As a starting point load all orig meso data
            # Replace meso WS with the forecasted WS
            ds_i["WS"] = df_i.to_xarray()
            ds_i.to_netcdf(fileName_i, mode="w")

    def save_to_csv(self, out_dir):
        HR_forec_withNegat = []
        HR_forec = []  # Negative values simply foreced to zero
        for h in range(0, len(self.forecRuns)):
            # print(self.forecRuns[h].name)

            df_h_withNegat = self.df_ws - self.forecRuns[h].sim_data
            HR_forec_withNegat.append(df_h_withNegat)

            df_h = df_h_withNegat.copy()
            df_h[df_h < 0] = 0
            HR_forec.append(df_h)

        for h in range(0, len(self.forecRuns)):
            # # Simulated forecast errors
            # df_i = HR_forecErr[h]
            # fileName_i = out_dir + HR_name[h] + '_err.csv'
            # df_i.to_csv(fileName_i)

            # Simulated forecast (negat values removed)
            df_i = HR_forec[h]
            df_i.columns = self.locs.Name  # Save csv with plant names
            fileName_i = out_dir + self.forecRuns[h].name + "_forecast.csv"
            df_i.to_csv(fileName_i)

            # # Simulated forecast (with negat values)
            # df_i = HR_forec_withNegat[h]
            # fileName_i = out_dir + HR_name[h] + '_forec_wNegatVals.csv'
            # df_i.to_csv(fileName_i)

            # # Simulated forecast power values
            # df_i = HR_forec_p[h]
            # fileName_i = out_dir + HR_name[h] + '_forec_pow.csv'
            # df_i.to_csv(fileName_i)

            # Save also info about forec update times
            df_i = self.forecRuns[h].df_forec_updateTimes
            fileName_i = out_dir + self.forecRuns[h].name + "_update_times.csv"
            df_i.to_csv(fileName_i)
