import argparse
from datetime import datetime, timedelta

import pandas as pd
import yaml
from dateutil.relativedelta import relativedelta

from corres.auxiliar_functions import clean, mkdir


def returnNone():
    return None


def read_simulation_inputs(input_fn="simulation_input.yml"):

    with open(input_fn) as file:
        input_dic = yaml.load(file, Loader=yaml.FullLoader)

    keys_defualt_valuesNone = ["power_curves_fn", "domain", "group_by", "weight_var"]

    for iter1 in keys_defualt_valuesNone:
        if iter1 not in input_dic.keys():
            input_dic[iter1] = None

    keys_defualt_valuesFalse = ["fluctuation", "forecast_errors"]

    for iter1 in keys_defualt_valuesFalse:
        if iter1 not in input_dic.keys():
            input_dic[iter1] = False

    for iter1 in input_dic.keys():
        if input_dic[iter1] == "None":
            input_dic[iter1] = None

    return input_dic


def check_simulation_size(input_dic):

    if (
        (input_dic["simulation"] == "WPP")
        and (input_dic["fluctuation"] == False)
        and (input_dic["group_by"] == "Region")
    ):

        WPP_excel = pd.read_excel(input_dic["locs_fn"])

        if len(WPP_excel) > 20000:
            raise Exception(
                "Maximum number of WPPs with current simulation settings is 20000"
            )

        if len(WPP_excel["Region"].unique()) > 200:
            raise Exception(
                "Maximum number of regions to groupby with the current simulation settings is 200"
            )

    if (
        (input_dic["simulation"] == "WPP")
        and (input_dic["fluctuation"] == False)
        and ((input_dic["group_by"] == "Name") or (input_dic["group_by"] is None))
    ):

        WPP_excel = pd.read_excel(input_dic["locs_fn"])

        if len(WPP_excel["Name"].unique()) > 200:
            raise Exception(
                "Maximum number of WPPs with the current simulation settings is 200"
            )

    if (
        (input_dic["simulation"] == "WPP")
        and (input_dic["fluctuation"])
        and (input_dic["dt"] >= 5 * 60)
    ):
        WPP_excel = pd.read_excel(input_dic["locs_fn"])

        groupby_col = input_dic["group_by"]
        if groupby_col is None:
            groupby_col = "Name"

        if len(WPP_excel[groupby_col].unique()) > 200:
            raise Exception(
                "Maximum number of WPPs (or Regions) with the current simulation settings is 200"
            )

    if (
        (input_dic["simulation"] == "WPP")
        and (input_dic["fluctuation"])
        and (input_dic["dt"] <= 5 * 60)
    ):
        WPP_excel = pd.read_excel(input_dic["locs_fn"])

        groupby_col = input_dic["group_by"]
        if groupby_col is None:
            groupby_col = "Name"

        if len(WPP_excel[groupby_col].unique()) > 200:
            raise Exception(
                "Maximum number of WPPs (or Regions) with the current simulation settings is 200"
            )

        start_date = datetime.strptime(input_dic["start_date"], "%Y-%m-%d")
        end_date = datetime.strptime(input_dic["end_date"], "%Y-%m-%d")

        difference_in_years = relativedelta(end_date, start_date).years
        difference_in_days = relativedelta(end_date, start_date).days

        if input_dic["dt"] >= 60:
            if difference_in_years > 1:
                raise Exception(
                    "Maximum years of simulation with the current simulation settings is 1"
                )
        if input_dic["dt"] < 60:
            if difference_in_days > 7:
                raise Exception(
                    "Maximum weeks of simulation with the current simulation settings is 1"
                )

    if ((input_dic["simulation"] == "SPP") or (input_dic["simulation"] == "CSP")) and (
        input_dic["group_by"] == "Region"
    ):

        SPP_excel = pd.read_excel(input_dic["locs_fn"])

        if len(SPP_excel) > 5000:
            raise Exception(
                "Maximum number of individual plants with current simulation settings is 5000"
            )
        if len(SPP_excel["Region"].unique()) > 200:
            raise Exception(
                "Maximum number of unique regions with current simulation settings is 200"
            )

    if ((input_dic["simulation"] == "SPP") or (input_dic["simulation"] == "CSP")) and (
        (input_dic["group_by"] == "Name") or (input_dic["group_by"] is None)
    ):
        SPP_excel = pd.read_excel(input_dic["locs_fn"])

        if len(SPP_excel) > 200:
            raise Exception(
                "Maximum number of individual plants with current simulation settings is 200"
            )

    if input_dic["forecast_errors"]:
        WPP_excel = pd.read_excel(input_dic["locs_fn"])

        if len(WPP_excel) > 500:
            raise Exception(
                "Maximum number of individual plants when forecast errors are enabled is 500"
            )

    if (input_dic["dataset"] != "ERA5") & (input_dic["dataset"] != "NEWA"):
        raise Exception("Only ERA5 and NEWA datasets are available")


def check_wsscale_column_exists(config, locs, simulation):

    if "var_scaler" in config["interpolation"][simulation].keys():

        if config["interpolation"][simulation]["var_scaler"] not in locs.columns:

            raise NameError(
                f'\n The simulation: {simulation} requires a column named: {config["interpolation"][simulation]["var_scaler"]}.\n'
            )

    return locs
