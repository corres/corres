import os

# main libraries
import numpy as np
import pandas as pd

# pvlib imports
import pvlib
import xarray as xr
from dask import compute, delayed
from pvlib import atmosphere, irradiance, pvsystem, tools
from pvlib.location import Location
from pvlib.modelchain import ModelChain
from pvlib.pvsystem import PVSystem
from scipy import signal

from corres.auxiliar_functions import offset_timeseries, split_in_batch
from corres.meso_tools import apply_interpolation_f


def computes_generation_CSP(
    locs,
    ds_interp_fn,
    model="ANI",  # 'ANI_IAM'
    batch=[],
    ERA5_dni_fix=False,
):
    """
    Makes the prediction of the time series of Concentrated Solar Power for multiple
    power plants (WPP).

    Parameters
    ----------

    locs: DataFrame
        Concentrated solar power plants table including properties of each plant:
        Name, Latitude, Longitude, Energy_storage_h [in hours at full capacity].

    dni_df: DataFrame
        Time series of the direct normal irradiance at each WPP.

    """

    ds_interp = xr.open_dataset(ds_interp_fn).sel(locs_ID=batch)

    # # Extract vars_needed
    vars_needed = list(ds_interp.keys())
    df_var = dict()
    for var in vars_needed:
        df_var[var] = (
            ds_interp[var]
            .to_dataframe()
            .reset_index()
            .set_index("time")
            .pivot(columns="locs_ID", values=var)
        )
        df_var[var].columns = locs.Name.values

        if var in ["DNI", "DHI", "GHI"]:
            aux = offset_timeseries(df_var[var], offset=-30, unit="min")
            df_var[var] = aux.interpolate()  # .fillna(0.0)
            df_var[var][df_var[var] < 0] = 0

    if "Name" in locs.columns:
        locs.set_index("Name", inplace=True)
    locs = locs.fillna(0)

    # Optimal SM at different thermal energy storage levels from IRENA CSP
    # report.
    SM_table = pd.DataFrame(
        columns=["Energy_storage_h", "SM"],
        data=[[0, 1.5], [3, 1.75], [6, 2.0], [9, 2.5], [12, 2.9], [18, 3.0]],
    )
    locs["SM"] = np.interp(
        locs.Energy_storage_h,
        SM_table.Energy_storage_h,
        SM_table.SM,
        left=1.5,
        right=3.0,
    )

    # Time constant of 1st order system. in hours as the timeseries in 1 hour
    # timestep
    time_constant = 0.6
    alpha = time_constant  # for recurrent filter
    Turbine_efficiency = 0.38
    charging_efficiency = 0.9
    discharging_efficiency = 0.9

    if ERA5_dni_fix:
        dni_df = 0 * df_var["GHI"]
    else:
        dni_df = df_var["DNI"]

    CSP_df = 0 * df_var[vars_needed[0]].copy()
    storage_df = 0 * df_var[vars_needed[0]].copy()
    for i, name in enumerate(dni_df.columns):

        latitude = locs.loc[name, "Latitude"]
        longitude = locs.loc[name, "Longitude"]
        altitude = locs.loc[name, "Altitude"]

        pvloc = Location(
            latitude=latitude,
            longitude=longitude,
            tz="UTC",
            altitude=altitude,
            name=name,
        )
        times = CSP_df.index
        pressure = atmosphere.alt2pres(altitude)
        solpos = pvloc.get_solarposition(times, pressure)

        if "Generation_efficiency" in locs.columns:
            Generation_efficiency = locs.loc[name, "Generation_efficiency"]
        else:
            Generation_efficiency = 1

        # Solar multiple
        SM = locs.loc[name, "SM"]

        if ERA5_dni_fix:
            # # To termporarly solve the bug in ERA5.dni

            disc_out = pvlib.irradiance.disc(
                ghi=df_var["GHI"].loc[:, name],
                solar_zenith=solpos.zenith,
                datetime_or_doy=times,
                pressure=pressure,
                min_cos_zenith=0.065,
                max_zenith=87,
                max_airmass=12,
            )
            dni_df[name] = disc_out["dni"]

        # Force proper limits of dni
        dni_df.loc[dni_df[name] > 1200, name] = 1200
        dni_df.loc[dni_df[name] < 0, name] = 0

        # Scaling asumes that the CSP is designed based on maximum dni
        F = Turbine_efficiency * 2.6 * SM / np.maximum(dni_df.loc[:, name].max(), 100)
        # First order dynamic system filter
        y = signal.lfilter(
            b=[F * (1 - alpha)],
            a=[1, -alpha],
            x=dni_df.iloc[:, i],
            axis=-1,
        )

        #
        incidence_angle = solpos.apparent_zenith
        solar_elev = np.cos(np.deg2rad(incidence_angle))
        if model == "ANI_IAM":
            # ANI and emprical IAM correction
            solar_term = np.interp(solar_elev, [0.1, 0.5, 0.9], [0.0, 0.5, 1.0])
        else:  # 'ANI'
            solar_term = np.interp(solar_elev, [0.0, 1], [0.0, 1.0])
        y = solar_term * y

        storage_df.iloc[:, i] = charging_efficiency * (np.maximum(y, 1) - 1)
        CSP_df.iloc[:, i] = np.minimum(y, 1)

    # Computes the ideal energy stored
    storage_daily = storage_df.resample("1D").sum()
    storage_daily_original = storage_daily.copy()
    # Limits the energy stored to match the capacity
    for name in list(locs.index):
        Energy_storage_h = locs.loc[name, "Energy_storage_h"]
        storage_daily.loc[
            storage_daily.loc[:, name] > Energy_storage_h, name
        ] = Energy_storage_h

    # Computes the ratio of actual and ideal (infinite) storage
    actual_storage_ratio_daily = storage_daily / storage_daily_original
    actual_storage_ratio_daily = actual_storage_ratio_daily.fillna(1.0)
    actual_storage_ratio = actual_storage_ratio_daily.reindex(
        storage_df.index, method="bfill"
    ).fillna(1.0)

    # Energy to be dispatched
    energy_to_full_cumsum = ((1 - CSP_df) / discharging_efficiency).cumsum()
    power_given_by_storage = 0 * CSP_df.copy()

    for i_day in range(len(storage_daily.index))[:-1]:
        start_time = str(storage_daily.index[i_day] + pd.DateOffset(hour=12))
        end_time = str(storage_daily.index[i_day + 1] + pd.DateOffset(hour=12))

        # Energy missing to reach full capacity
        energy_to_full = (
            energy_to_full_cumsum.loc[start_time:end_time, :]
            - energy_to_full_cumsum.loc[start_time, :]
        )

        # Energy dispatch to reach full capacity untill daily stored is drained
        aux = energy_to_full[energy_to_full < storage_daily.iloc[i_day]].fillna(
            storage_daily.iloc[i_day]
        )
        power_given_by_storage.loc[
            start_time:end_time, :
        ] = discharging_efficiency * diff_cs(aux)

    CSP_with_excess_df = CSP_df + storage_df * actual_storage_ratio
    CSP_with_storage_df = CSP_df + power_given_by_storage

    vars_out = dict()
    # vars_out['CSP_without_storage'] = Generation_efficiency * CSP_df
    vars_out["DNI"] = dni_df
    vars_out["CSP_with_excess"] = Generation_efficiency * CSP_with_excess_df
    vars_out["CSP_with_storage_dispatched"] = (
        Generation_efficiency * CSP_with_storage_df
    )

    locs = locs.reset_index()
    return vars_out


def computes_generation_CSP_distributed(
    locs, ds_interp_fn, model="ANI", n_batch=10, ERA5_dni_fix=False  # 'ANI_IAM'
):
    """
    Makes the prediction of the time series of Concentrated Solar Power for multiple
    power plants (WPP) using dask distributed computations.

    Parameters
    ----------

    locs: DataFrame
        Concentrated solar power plants table including properties of each plant:
        Name, Latitude, Longitude, Energy_storage_h [in hours at full capacity].

    ds_interp_fn: Str
        Filename of the netcdf with the time series of the irradiances at each WPP.

    """

    batches = split_in_batch(locs.index.tolist(), n_batch)

    dly = dict()
    for ib, batch in enumerate(batches):
        if len(batch) > 0:
            locs_batch = locs.loc[batch, :]
            dly[ib] = delayed(computes_generation_CSP)(
                locs=locs_batch,
                ds_interp_fn=ds_interp_fn,
                model=model,
                batch=batch,
                ERA5_dni_fix=ERA5_dni_fix,
            )
    all_v = compute(dly)[0]
    out = dict()
    for var in all_v[0].keys():
        out[var] = pd.concat([all_v[key][var] for key in all_v.keys()], axis="columns")
    return out


# ----------------------------
# Auxiliary functions for CSP
# ----------------------------


def diff_cs(df):
    """
    Computes the diff of a dataframe with an additional step at the begining.
    """

    df_out = df.copy()
    df_out.iloc[:, :] = df.diff().fillna(0)
    return df_out
