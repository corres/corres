# Copyright (C) 2019 DTU Wind

# auxiliar system
import glob
import itertools
import os
import pickle
import shutil
import warnings
from datetime import datetime
from pathlib import Path

# for plotting and maps
import matplotlib as mpl
import matplotlib.pyplot as plt

# main libraries
import numpy as np
import pandas as pd
import wrf
import xarray as xr
import yaml
import zarr
from dask import compute, delayed
from finitediff import get_weights
from numpy import newaxis as na

# for interpolation stencils
from sklearn.neighbors import NearestNeighbors

from corres.auxiliar_functions import (
    RepresentsInt,
    clean,
    flatten_nested_lists,
    get_shortname,
    last_path,
    mkdir,
    removefile,
    return_element,
    split,
    split_in_batch,
    valid_netcdf,
    watch_dog,
    watch_dog_netcdf,
)

# import cartopy
# import cartopy.crs as crs

# remove warning from wrf:
warnings.filterwarnings(
    "ignore",
    message="Using or importing the ABCs from 'collections' instead of from",
)
warnings.filterwarnings("ignore", category=DeprecationWarning)


class CorRES_Meso_extractor_warn(UserWarning):
    pass


class Meso_extractor(object):

    """
    An object of class Meso_extractor handles all data the meso scale
    weather data. The data can be given as netcdf or grib files, arranged in
    an arbitrary file structure.

    Parameters
    ----------

    meso_dir: str
        meso-scale data directory

    meso_dir_template: str
        meso-scale data directory template for interpreting the sub-folders
        and file names in meso_dir. For example:

        - ``{Domain}/{year}/daily_{year}-{month}-{day}.nc``
        - ``{Domain}/daily/{year}/daily_{year}-{month}-{day}.nc``
        - ``{Domain}/{year}/{year}{month}{day}.nc``

        Note: Only use '/' as separators

        The variables to be interpreted are:

        - ``{Domain}``: Must match the names in region_domain_fn
        - ``{year}``: Must be formated `yyyy`, i.e. 2018
        - ``{month}``: Must be formated `mm`, i.e. 01
        - ``{day}``: Must be formated `dd`, i.e. 06

    static_template: str
        Static meso file template for multiple regional analysis.

    corres_config_fn: str
        CorRES configuration yml file name.

    region_domain_fn: fn
        xlsx filename with region to domain matching.

    locs: DataFrame
        Table with the locations of interest.
        e.x. power plant locations. Should include the following columns:
        ['Name','Latitude','Longitude'].

        if 'HubHeight' is not included it is assumed to be 1[m] above ground.

        If a specific unique number is to be used add an 'ID' column.

    out_dir: str
        Folder for the CorRES project

    run_type: str, optional, default='cluster'
        run_type can be 'local', 'server'.
        The server run type relies on dask and will perform the analysis
        using distributed computing.

    n_batch: int
        Number of days that are processed at the same time. The more days the
        larger the memory footprint of the execution.

    vartime: str, optional, default='Times'
        Name for the time variable

    varlevs: str, optional, default='LEVS'
        Name for the levels variable

    vars_xy_logz: list
        List of variables to be interpolated in horizontal (x,y) using finite
        differences and power law piecewise interpolation in z.

    vars_xyz: list
        List of variables to be interpolated in horizontal (x,y) using finite
        differences and linear piecewise interpolation in z.

    vars_xy: list
        List of variables to be interpolated in horizontal (x,y) using finite
        differences

    vars_nearest_xy: list
        List of variables to be approximated to the nearest horizontal point (x,y)

    vars_nearest_xyz: list
        List of variables to be approximated to the nearest point (x,y,z)
    """

    def __init__(
        self,
        meso_dir,
        meso_dir_template,
        static_template,
        corres_config_fn,
        region_domain_fn,
        locs,
        out_dir,
        start_date=None,
        end_date=None,
        vartime="Times",
        varlevs="LEVS",
        vars_xy_logz=["WSPD"],
        vars_xyz=["WDIR"],  # "RHO"
        vars_xy=[],  # ["UST", "RMOL", "TAIR", "DIF_AVG", "DNI_AVG", "SUN_DUR"]
        vars_nearest_xy=[],
        vars_nearest_xyz=[],
        read_existing_interpolation_temp=True,
        memory_av=None,
        n_batch=1,
        n_sim_jobs=5,
    ):

        print(
            "\n----------------------------------------------------------------------"
        )
        print("Initilize a Meso_extractor object")
        print(
            "----------------------------------------------------------------------\n"
        )

        print(f"Inputs:\n")
        print(f"meso_dir = '{meso_dir}'")
        print(f"meso_dir_template = '{meso_dir_template}'")
        print(f"static_template = '{static_template} '")
        print(f"corres_config_fn = '{corres_config_fn}'")
        print(f"region_domain_fn = '{region_domain_fn}'")
        print(f"out_dir = '{out_dir}'")
        print(f"start_date = '{start_date}'")
        print(f"end_date = '{end_date}'")

        print(f"vartime = '{vartime}'")
        print(f"varlevs = '{varlevs}'")
        print(f"vars_xy_logz = '{vars_xy_logz}'")
        print(f"vars_xyz = '{vars_xyz}'")
        print(f"vars_xy = '{vars_xy}'")
        print(f"vars_nearest_xy = '{vars_nearest_xy}'")
        print(f"vars_nearest_xyz = '{vars_nearest_xyz}'")

        print(
            f"read_existing_interpolation_temp = '{read_existing_interpolation_temp}'"
        )
        print(f"n_sim_jobs = '{n_sim_jobs}'")
        print("\n\n")

        print("locs:")
        print(locs.head())
        print("\n\n")

        self.meso_dir = Path(meso_dir)
        self.meso_dir_template = meso_dir_template
        self.static_template = static_template

        # model setup
        self.corres_config_fn = corres_config_fn
        self.read_existing_interpolation_temp = read_existing_interpolation_temp

        if "ID" not in locs.columns:
            locs["ID"] = range(len(locs))

        # To solve the problem of running only one plant
        if len(locs) == 1:
            locs.loc[1, :] = locs.loc[0, :]
            locs.loc[1, "ID"] = locs.loc[0, "ID"] + 1

        # if Solar plants do not give Hub_height
        if "Hub_height" not in locs.columns:
            locs["Hub_height"] = 1  # [m] 1 meter above ground

        regdom = pd.read_excel(region_domain_fn)

        # Add a column to identify on which domain each locs belongs
        locs["WRF_Domain"] = [
            regdom.loc[regdom.Country == country, "WRF_Domain"].tolist()[0]
            for country in locs.Country.values
        ]

        # extract available domains from the WRF files
        domains = extract_domains(meso_dir, meso_dir_template)
        # drop domains not used by the locs
        domains_to_remove = []
        for domain in domains:
            if domain not in locs["WRF_Domain"].unique():
                domains_to_remove += [domain]
        for domain in domains_to_remove:
            domains.remove(domain)

        # extract files available
        all_fn = dict()
        for domain in domains:
            # grab one file available in the domain/year
            all_fn[domain] = sorted(
                glob.glob(
                    os.path.join(
                        meso_dir,
                        meso_dir_template.replace("{domain}", domain)
                        .replace("{year}", "*")
                        .replace("{month}", "*")
                        .replace("{day}", "*"),
                    )
                )
            )

        # determine the files from user request
        if start_date is None:
            # pick the first date out of all domains
            start_date = sorted(
                [make_date_string(all_fn[d][0], meso_dir_template) for d in domains]
            )[0]
        if end_date is None:
            # pick the last date out of all domains
            end_date = sorted(
                [make_date_string(all_fn[d][-1], meso_dir_template) for d in domains]
            )[-1]

        dates_rng_request = pd.date_range(start_date, end_date)
        request_fn = [
            meso_dir_template.replace("{year}", str(date.year).zfill(4))
            .replace("{month}", str(date.month).zfill(2))
            .replace("{day}", str(date.day).zfill(2))
            .split("/")[-1]
            .split(".")[0]
            for date in dates_rng_request
        ]

        # extract files to be read
        fn_to_read = dict()
        for domain in domains:
            fn_to_read[domain] = [
                fn for fn in all_fn[domain] if get_shortname(fn) in request_fn
            ]

        df_files = counting_files_per_domain(fn_to_read, domains)

        if (df_files["Number of WRF files"] == 0).any():
            print("\n\n")
            print("No files available in the date range specified")
            print()
            print("Available files summary:")
            print(counting_files_per_domain(all_fn, domains))

            # Removes domains without WRF files on the simulation time period
            domain_to_remove = df_files.loc[
                df_files["Number of WRF files"] == 0, :
            ].index.tolist()
            if len(domain_to_remove) > 0:
                print()
                print("The following domains will not be processed:")
                print(domain_to_remove)
                domains.remove(*domain_to_remove)

        # Remove domain not used in the locs
        domain_to_remove = []
        for domain in domains:
            locs_sel = locs.loc[locs.WRF_Domain == domain, :]
            if len(locs_sel) == 0:
                domain_to_remove += [domain]

        if len(domain_to_remove) > 0:
            print("The following domain(s) do not contain any loc:", *domain_to_remove)
            domains.remove(*domain_to_remove)

        # if memory_av is not None:
        #     print(f"memory_av = '{memory_av}'")
        #     example_temp_net_cdf = 8e9 * len(locs) / (5 * 19228)
        #     self.n_batch = int(np.floor(memory_av / example_temp_net_cdf))
        # else:
        #     self.n_batch = n_batch

        # print(f"n_batch = '{n_batch}'")

        self.n_batch = n_batch
        self.n_sim_jobs = n_sim_jobs
        self.domains = domains
        self.fn_to_read = fn_to_read
        self.df_files = df_files
        self.locs = locs

        # variable definitions
        self.vartime = vartime
        self.varlevs = varlevs
        self.vars_xy_logz = vars_xy_logz
        self.vars_xyz = vars_xyz
        self.vars_xy = vars_xy
        self.vars_nearest_xy = vars_nearest_xy
        self.vars_nearest_xyz = vars_nearest_xyz

        # Create out, temp, netcdsf folders
        self.out_dir = mkdir(out_dir)
        self.temp_dir = mkdir(os.path.join(self.out_dir, "temp"))
        self.weights_fn = dict()
        netcdf_dirs = dict()
        for domain in domains:
            tdir = mkdir(Path(os.path.join(self.temp_dir, domain + "/")))
            netcdf_dir = mkdir(Path(os.path.join(tdir, "netcdfs/")))
            netcdf_dirs[domain] = netcdf_dir

            weights_fn = os.path.join(tdir, "weights.nc")
            # store weight file names
            self.weights_fn[domain] = weights_fn

        self.netcdf_dirs = netcdf_dirs

    def plot_locs(self, kind="hexbin", figsize=(24, 8), **kwargs):
        """
        Method for plotting the locations in a map.

        Parameters
        ----------
        kind: str, optional, default = "hexbin"
            Type of plot, can be "hexbin" or "scatter"
        figsize: tuple, optional, default = (24,8)
            Size of the figure.
        kwargs: dict
            Additonal parameters for "scatter" plots
        """

        # Read the input excel sheets for locs and Region_Domains
        locs = self.locs

        fig = plt.figure(figsize=figsize)
        # crs_proj = crs.PlateCarree()
        # ax = plt.axes(projection=crs_proj)
        ax = plt.axes()
        if kind == "scatter":
            ax.plot(
                locs.Longitude.values,
                locs.Latitude.values,
                linestyle="",
                **kwargs,
            )
        else:
            plt.hexbin(
                locs.Longitude.values,
                locs.Latitude.values,
                gridsize=100,
                cmap=plt.cm.Blues,
                mincnt=1,
            )
            cb = plt.colorbar(fraction=0.046, pad=0.04)
            cb.set_label("Number of locs")
        ax.coastlines(resolution="10m", alpha=0.5)
        ax.add_feature(cartopy.feature.BORDERS, linestyle="-", alpha=0.25)

        min_lon, min_lat = locs.loc[:, ["Longitude", "Latitude"]].min().values
        max_lon, max_lat = locs.loc[:, ["Longitude", "Latitude"]].max().values
        xticks = np.arange(
            np.around(np.floor(min_lon), -1),
            np.around(np.ceil(max_lon), -1) + 0.1,
            10,
        )
        yticks = np.arange(
            np.around(np.floor(min_lat), -1),
            np.around(np.ceil(max_lat), -1) + 0.1,
            10,
        )

        ax.set_xticks(xticks)  # , crs=crs_proj)
        ax.set_yticks(yticks)  # , crs=crs_proj)

        return fig

    def read_projections(self):
        """
        Method that reads and stores the wrf projection properties for
        converting lat,lon to regular grid coordinates used in wrf.

        If it fails to read the projection coordinates it returns a None.
        When applied a None projection implies the domain is regular in lat,
        long instead of in Cartesian coordinates from a projection.
        """

        meso_dir = self.meso_dir
        static_template = self.static_template
        domains = self.domains

        wrf_proj, domain_x, domain_y = read_projections(
            meso_dir, static_template, domains
        )

        self.wrf_proj = wrf_proj
        self.domain_x = domain_x
        self.domain_y = domain_y

    def read_meso_check(
        self,
        expected_vars=[
            "WSPD",
            "WDIR",
            "RHO",
            "UST",
            "RMOL",
            "TAIR",
            "WS10",
            "DIF_AVG",
            "DNI_AVG",
            "SUN_DUR",
            "LEVS",
            "Times",
        ],
        expected_dims=["Time", "lev", "south_north", "west_east"],
        verbose=True,
    ):
        """
        Method that test the validity of the meso files.

        It reads one file and checks for variable and dimension names.

        This method is also responsible for inferring the vertical levels.

        Parameters
        ----------
        expected_vars: list, optional
            List of variables that should be present in the netcdf files
            default = ["WSPD", "WDIR", "RHO", "UST", "RMOL", "TAIR", "WS10",
            "DIF_AVG", "DNI_AVG", "SUN_DUR", "LEVS", "Times"]

        expected_dims: list, optional
            List of dimensions that should be present in the netcdf files
            default = ["Time", "lev", "south_north", "west_east"]

        Todo
        -----
            - Read expected_vars from the config.yml file
            - rosetta_stone from the config.yml file
            - rosetta_stone reflected through the code

        """
        if verbose:
            print(
                "\n------------------------------------------------------------------"
            )
            print("Check if a meso file can be read")
            print(
                "------------------------------------------------------------------\n"
            )

        meso_dir = self.meso_dir
        meso_dir_template = self.meso_dir_template
        domains = self.domains
        fn_to_read = self.fn_to_read

        # Test reading a file for each domain
        pass_list = [False] * len(domains)
        levels = dict()
        for i_dom, domain in enumerate(domains):
            # grab one file available in the domain
            test_fn = fn_to_read[domain][0]
            if verbose:
                print("Reading file " + test_fn)

            test = xr.open_dataset(test_fn)

            if (list(test.dims) == expected_dims) & (
                list(test.variables) == expected_vars
            ):
                pass_list[i_dom] = True
                levels[domain] = test.LEVS.values

            # to do later: else using rosetta stone
            test.close()

        self.levels = levels

        return all(pass_list)

    def prepare_interpolation(self):
        """
        Method that uses the projections to change coordinates,
        computes and stores the weights for interpolation for all the locs in
        each domain.

        Todo
        -----
            - Read stencil settings from the config.yml file
            - rosetta_stone from the config.yml file
            - rosetta_stone reflected through the code

        """
        meso_dir = self.meso_dir
        meso_dir_template = self.meso_dir_template
        static_template = self.static_template
        domains = self.domains
        fn_to_read = self.fn_to_read

        wrf_proj = self.wrf_proj
        domain_x = self.domain_x
        domain_y = self.domain_y
        levels = self.levels

        # Read the input excel sheets for locs and Region_Domains
        locs = self.locs

        for domain in domains:
            locs_in_domain = locs.loc[locs["WRF_Domain"] == domain, :]

            # Get wrf projection coordinates of the locations
            ds_x_y = wrf.ll_to_xy_proj(
                latitude=locs_in_domain.Latitude.values,
                longitude=locs_in_domain.Longitude.values,
                as_int=False,
                **wrf_proj[domain],
            )
            locs_x = ds_x_y.values[0, :]
            locs_y = ds_x_y.values[1, :]
            locs_z = locs_in_domain.Hub_height.values

            # self.locs[locs_in_domain.index, 'domain_x'] = locs_x
            # self.locs[locs_in_domain.index, 'domain_y'] = locs_y
            locs_in_domain.loc[:, "domain_x"] = locs_x
            locs_in_domain.loc[:, "domain_y"] = locs_y

            # Check that points are whithin.
            min_x = np.min(domain_x[domain])
            max_x = np.max(domain_x[domain])
            test_within = locs_in_domain.loc[
                (locs_in_domain.domain_x < min_x) | (locs_in_domain.domain_x > max_x), :
            ]

            if len(test_within) > 0:
                locs_x = np.maximum(np.minimum(locs_x, max_x), min_x)
                ds_l_l = wrf.xy_to_ll_proj(
                    x=locs_x,
                    y=locs_y,
                    **wrf_proj[domain],
                )

                locs_in_domain.domain_x = locs_x
                locs_in_domain.domain_y = locs_y
                locs_in_domain.Latitude = ds_l_l.values[0, :]
                locs_in_domain.Longitude = ds_l_l.values[1, :]

                suggest = locs_in_domain.loc[test_within.index.tolist(), :]

                raise Exception(
                    f"Some locations have domain_x coorinates"
                    + f" outside {domain} borders [{min_x},{max_x}]:"
                    + f"\n\n {test_within} \n\n\n Suggested locations are: \n\n {suggest}"
                )

            min_y = np.min(domain_y[domain])
            max_y = np.max(domain_y[domain])
            test_within = locs_in_domain.loc[
                (locs_in_domain.domain_y < min_y) | (locs_in_domain.domain_y > max_y), :
            ]

            if len(test_within) > 0:
                locs_y = np.maximum(np.minimum(locs_y, max_y), min_y)
                ds_l_l = wrf.xy_to_ll_proj(
                    x=locs_x,
                    y=locs_y,
                    **wrf_proj[domain],
                )

                locs_in_domain.domain_x = locs_x
                locs_in_domain.domain_y = locs_y
                locs_in_domain.Latitude = ds_l_l.values[0, :]
                locs_in_domain.Longitude = ds_l_l.values[1, :]

                suggest = locs_in_domain.loc[test_within.index.tolist(), :]

                raise Exception(
                    f"Some locations have domain_y coorinates"
                    + f" outside {domain} borders [{min_y},{max_y}]"
                    + f"\n\n {test_within} \n\n\n Suggested locations are: \n\n {suggest}"
                )

            # prepare coordinates for interpolation
            all_x = domain_x[domain]
            all_y = domain_y[domain]
            all_z = levels[domain]

            # Get interpolation weights
            weights_ds = get_interpolation_weights(
                locs_x,
                locs_y,
                locs_z,
                all_x,
                all_y,
                all_z,
                n_stencil=4,
                locs_ID=locs_in_domain.ID.values,
            )

            # Store interpolation weights
            weights_fn = self.weights_fn[domain]
            removefile(weights_fn, verbose=False)
            weights_ds.to_netcdf(weights_fn, mode="w")
            weights_ds.close()
            del weights_ds

    def apply_interpolation(self, client=None):
        """
        Method that applies the interpolation at locs.

        Todo
        -----
            - Read stencil settings from the config.yml file
            - rosetta_stone from the config.yml file
            - rosetta_stone reflected through the code

        """

        print(
            "\n----------------------------------------------------------------------"
        )
        print("Apply interpolation")
        print(
            "----------------------------------------------------------------------\n"
        )

        self.read_projections()
        self.prepare_interpolation()
        weights_fn = self.weights_fn
        n_batch = self.n_batch

        netcdf_dirs = dict()
        # loop over all domains
        for domain in self.domains:
            print(f"\nDomain: {domain}")

            # split the files per domain
            meso_fn_list_batches = split_in_batch(self.fn_to_read[domain], n_batch)
            total_batch = len(meso_fn_list_batches)

            digits = int(np.log10(len(meso_fn_list_batches))) + 1

            # Check if all the files for the domain exists
            netcdf_dir = self.netcdf_dirs[domain]
            if len(glob.glob(str(netcdf_dir) + "/*")) == total_batch:
                print(f"Interpolation in domain {domain} is done")

            else:
                # print("Starting distributed computation")
                # delayed compute. This distributes the computation.
                # It requires to set up dask client

                # loop over batches, submitting n_sim_jobs batches at the same
                # time
                batch_simultaneous = split_in_batch(range(total_batch), self.n_sim_jobs)
                for i_batch, batch in enumerate(batch_simultaneous):
                    # simulatenous computations
                    all_dlyd = []
                    for i in batch:
                        ml = meso_fn_list_batches[i]
                        out_fn = os.path.join(
                            netcdf_dir,
                            "meso_at_locs_" + str(i).zfill(digits) + ".nc",
                        )
                        # out_fn = os.path.join(
                        #     self.netcdf_dirs[domain], "meso_at_locs.zarr")

                        if (
                            os.path.exists(out_fn)
                            & ~self.read_existing_interpolation_temp
                        ):
                            removefile(out_fn)
                            excecute_ = True
                        elif (
                            os.path.exists(out_fn)
                            & self.read_existing_interpolation_temp
                        ):
                            # print("File already saved:", out_fn)
                            excecute_ = False
                        else:
                            excecute_ = True

                        if excecute_:
                            all_dlyd = all_dlyd + [
                                delayed(apply_interpolation_from_fn)(
                                    wrf_fn=ml,
                                    weights_fn=self.weights_fn[domain],
                                    out_fn=out_fn,
                                    vartime=self.vartime,
                                    varlevs=self.varlevs,
                                    vars_xy_logz=self.vars_xy_logz,
                                    vars_xyz=self.vars_xyz,
                                    vars_xy=self.vars_xy,
                                    vars_nearest_xy=self.vars_nearest_xy,
                                    vars_nearest_xyz=self.vars_nearest_xyz,
                                )
                            ]

                    # To avoid computing empty task graphs
                    if len(all_dlyd) > 0:
                        print(f"Submitting batches {batch} out of {total_batch}")
                        _ = compute(all_dlyd)

                        # wait for the last file to be written
                        # if watch_dog(out_fn, N_max_tries=120):
                        if client is None:
                            pass
                        else:  # elif np.mod(i_batch,1)==0:
                            client.restart()

    def check_temp_files(self, var_check="Time", min_count=24):
        """
        Method that checks if the interpolated wrf files (temp files) are valid.
        If a file(s) is not valid, it will delete the file(s) and re-run the
        interpolation on the missing files.
        """

        re_execute = False
        for domain in self.domains:
            list_temp_fn = glob.glob(str(self.netcdf_dirs[domain]) + "/*")
            for fn in list_temp_fn:
                if valid_netcdf(fn, var_check=var_check, min_count=min_count):
                    pass
                else:
                    removefile(fn)
                    re_execute = True

        if re_execute:
            self.apply_interpolation()

    def extract_meso_ts(
        self,
        sel_var="ID",
        list_values_sel=[],
        vars_to_extract=[],
    ):
        """
        Method that extracts and saves the timeseries on a selection of the locs.
        The resulting netcdf can be too big (larger than memory) if there are many
        locations in the specified selection, in that case make an outter loop to
        store many smaller files::

            list_fn = []
            for ID in self.locs.ID.unique():
                list_fn += [self.extract_meso_ts(
                    sel_var = 'ID',
                    list_values_sel = [ID],
                    fn = 'ts_locs_ID_'+str(ID)+'.nc',
                    return_fn = True)]


        Todo
        -----
            - Read stencil settings from the config.yml file
            - rosetta_stone from the config.yml file
            - rosetta_stone reflected through the code

        """

        # If levels are not available then run read_meso_check
        if "levels" in self.__dict__.keys():
            self.read_meso_check(verbose=False)
        # If wrf_proj is not available then run read_projections
        if "wrf_proj" in self.__dict__.keys():
            self.read_projections()
        # self.prepare_interpolation()

        # Read the input excel sheets for locs and Region_Domains
        locs = self.locs
        if len(list_values_sel) == 0:
            list_values_sel = locs[sel_var].values

        locs_sel = locs.loc[locs[sel_var].isin(list_values_sel), :]

        if len(locs_sel) == 0:
            print("No matching values found: ", sel_var, list_values_sel)
            return None

        else:
            # df_var is a DataFrame per variable for each plant on the
            # current domain
            df_var = dict()
            # df_var_out has all domains joint in to a single DataFrame
            df_var_out = dict()

            # Loop over domains in the locs table and in the available WRF DB
            domains_sel = [
                dom for dom in locs_sel["WRF_Domain"].unique() if dom in self.domains
            ]

            if len(domains_sel) == 0:
                domains_not_found = [
                    dom
                    for dom in locs_sel["WRF_Domain"].unique()
                    if dom not in self.domains
                ]
                print(domains_not_found, " WRF_domain(s) not found")
                return None

            else:
                for i, domain in enumerate(domains_sel):
                    locs_ID_sel = locs_sel.loc[
                        locs_sel["WRF_Domain"] == domain, "ID"
                    ].values

                    all_fn_at_domain = sorted(
                        glob.glob(os.path.join(self.netcdf_dirs[domain], "*.nc"))
                    )

                    def pre_process_sel(ds):
                        return ds.loc[{"locs_ID": locs_ID_sel}]

                    all_ds = xr.open_mfdataset(
                        all_fn_at_domain,
                        # chunks={"Time": -1, "locs_ID": 100},
                        combine="by_coords",
                        concat_dim="Time",
                        decode_times=True,
                        data_vars="minimal",
                        coords="minimal",
                        compat="override",
                        preprocess=pre_process_sel,
                    )

                    # out_fn = os.path.join(
                    #     self.netcdf_dirs[domain], "meso_at_locs.zarr")
                    # all_ds = xr.open_zarr(out_fn)

                    ds_sel = all_ds.sel(locs_ID=locs_ID_sel)
                    list_vars = ds_sel.data_vars

                    if len(vars_to_extract) == 0:
                        vars_to_extract = list_vars

                    out_fns = []
                    for var_ in vars_to_extract:
                        df_var[var_] = (
                            ds_sel[var_]
                            .to_dataframe()
                            .reset_index()
                            .set_index("Time")
                            .pivot(columns="locs_ID", values=var_)
                        )
                        # Use the names instead of plant ID as column headers
                        df_var[var_].columns = [
                            name
                            for name in self.locs.set_index("ID")
                            .loc[df_var[var_].columns.values, "Name"]
                            .values
                        ]

                        if i == 0:
                            df_var_out[var_] = df_var[var_]
                        else:
                            df_var_out[var_] = df_var_out[var_].join(df_var[var_])

                    ds_sel.close()
                    del ds_sel

                    # Remove duplicate columns
                    for var_ in vars_to_extract:
                        df_var_out[var_] = df_var_out[var_].loc[
                            :, ~df_var_out[var_].columns.duplicated()
                        ]
                return df_var_out

    def remove_temp(self):
        """
        Method that removes the temp folder. This means that previos interpolations
        done will be erased.
        """
        clean(self.temp_dir)


# ----------------------------
# Auxiliary functions for meso
# ----------------------------


def extract_domains(meso_dir, meso_dir_template):
    """
    Function to extract the domains in a meso_dir using the meso_dir_template
    """
    domains = sorted(
        os.listdir(os.path.join(meso_dir, meso_dir_template.split("{domain}")[0]))
    )
    domains = [x for x in domains if not x.startswith(".")]
    return domains


def make_date_string(fn, meso_dir_template):
    """
    Returns consistent date_string out of a filename with date data given the
    meso_dir_template
    """
    separators = split(get_shortname(meso_dir_template), ["{year}", "{month}", "{day}"])
    date_order = split(get_shortname(meso_dir_template), separators)

    date_dict = {}
    date_read = [a for a in split(get_shortname(fn), separators)]
    for i, var in enumerate(date_order):
        date_dict[var] = date_read[i]
    date_dict

    return "-".join([date_dict[var] for var in date_order])


def counting_files_per_domain(fn_to_read, domains):
    """
    Function to count files per domain in a fn dictionary.
    """
    # count files per year
    df_files = pd.DataFrame(
        columns=["Domain", "Number of WRF files", "First file", "Last file"]
    )
    df_files["Domain"] = domains
    df_files.set_index("Domain", inplace=True)

    for domain in domains:
        df_files.loc[domain, "Number of WRF files"] = len(fn_to_read[domain])
        df_files.loc[domain, "First file"] = get_shortname(
            return_element(fn_to_read[domain], 0)
        )
        df_files.loc[domain, "Last file"] = get_shortname(
            return_element(fn_to_read[domain], -1)
        )
    return df_files


def get_interpolation_weights(
    px,
    py,
    pz,
    all_x,
    all_y,
    all_z,
    n_stencil=4,
    locs_ID=[],
):
    """
    Function that creates the 3D interpolation weights using finite
    differences for multiple interpolations points (px,py,pz), given a grid of
    observed points [all_x, all_y, all_z].

    This function computes the weights for interpolation for different order
    in the horizontal dimensions (x,y), while it computes the weights for both
    linear interpolation and for piecewise logarithmic profile in z.

    Parameters
    ----------
    px: numpy.array
        Interpolation (prediction) points in x
    py: numpy.array
        Interpolation (prediction) points in y
    pz: numpy.array
        Interpolation (prediction) points in z
    all_x: numpy.array
        Observed points in x
    all_y: numpy.array
        Observed points in y
    all_z: numpy.array
        Observed points in z
    n_stencil: int, optional, default=4
        Number of points used in the horizontal interpolation
    locs_ID: list
        Names or ID to identify the locations
    """
    # Number of prediction points and observed points
    Np = len(px)
    Nx = len(all_x)
    Ny = len(all_y)
    Nz = len(all_z)

    if (Np != len(py)) or (Np != len(pz)):
        raise Exception("The len of px, py and pz should be the same")

    # get stencils for interpolations
    n_st_x = n_stencil
    n_st_y = n_stencil
    n_st_z = 2  # In z, interpolation is always based on two points
    if n_stencil > Nx:
        n_st_x = Nx
    if n_stencil > Ny:
        n_st_y = Ny
    if 2 > Nz:
        n_st_z = Nz
    nnx = NearestNeighbors(n_neighbors=n_st_x).fit(all_x[:, na])
    nny = NearestNeighbors(n_neighbors=n_st_y).fit(all_y[:, na])
    nnz = NearestNeighbors(n_neighbors=n_st_z).fit(all_z[:, na])

    # Find the indexes of the observed points to be used for interpolation
    ind_x = np.sort(nnx.kneighbors(px[:, na], return_distance=False), axis=1)
    ind_y = np.sort(nny.kneighbors(py[:, na], return_distance=False), axis=1)
    ind_z = np.sort(nnz.kneighbors(pz[:, na], return_distance=False), axis=1)

    # Find the index for nearest point selection
    nnx_1 = NearestNeighbors(n_neighbors=1).fit(all_x[:, na])
    nny_1 = NearestNeighbors(n_neighbors=1).fit(all_y[:, na])
    nnz_1 = NearestNeighbors(n_neighbors=1).fit(all_z[:, na])
    ind_x_1 = nnx_1.kneighbors(px[:, na], return_distance=False)
    ind_y_1 = nny_1.kneighbors(py[:, na], return_distance=False)
    ind_z_1 = nnz_1.kneighbors(pz[:, na], return_distance=False)

    # Allocate weight matrices
    # Horizontal interpolation weights have the size of the stencil
    weights_x = np.zeros([Np, n_st_x])
    weights_y = np.zeros([Np, n_st_y])
    # Vertical extrapolation weights are always the same size: all available
    # heights
    weights_log_z = np.zeros([Np, Nz])
    weights_z = np.zeros([Np, Nz])
    for i in range(Np):
        weights_x[i, :] = get_weights(grid=all_x[ind_x[i, :]], xtgt=px[i], maxorder=0)[
            :, 0
        ]

        weights_y[i, :] = get_weights(grid=all_y[ind_y[i, :]], xtgt=py[i], maxorder=0)[
            :, 0
        ]

        weights_log_z[i, ind_z[i, :]] = get_weights(
            grid=np.log(all_z[ind_z[i, :]]), xtgt=np.log(pz[i]), maxorder=0
        )[:, 0]

        weights_z[i, ind_z[i, :]] = get_weights(
            grid=all_z[ind_z[i, :]], xtgt=pz[i], maxorder=0
        )[:, 0]

    if len(locs_ID) == 0:
        locs_ID = np.arange(Np, dtype=int)

    # Build dataset
    weights_ds = xr.Dataset(
        data_vars={
            "weights_x": (
                ["locs_ID", "ix"],
                weights_x,
                {"description": "Interpolation weights based on finite differences"},
            ),
            "ind_x": (
                ["locs_ID", "ix"],
                ind_x,
                {"description": "Indices of WRF grid to use in the interpolation"},
            ),
            "weights_y": (
                ["locs_ID", "iy"],
                weights_y,
                {"description": "Interpolation weights based on finite differences"},
            ),
            "ind_y": (
                ["locs_ID", "iy"],
                ind_y,
                {"description": "Indices of WRF grid to use in the interpolation"},
            ),
            "weights_z": (
                ["locs_ID", "iz"],
                weights_z,
                {"description": "Interpolation weights based on finite differences"},
            ),
            "weights_log_z": (
                ["locs_ID", "iz"],
                weights_log_z,
                {
                    "description": "Logaritmic interpolation weights based on finite differences"
                },
            ),
            "ind_z": (
                ["locs_ID", "iz"],
                np.repeat(np.arange(len(all_z))[na, :], Np, axis=0),
                {"description": "Indices of WRF grid to use in the interpolation"},
            ),
            "ind_x_1": (
                ["locs_ID"],
                ind_x_1.flatten(),
                {
                    "description": "Indices of WRF grid to use in nearest point selection"
                },
            ),
            "ind_y_1": (
                ["locs_ID"],
                ind_y_1.flatten(),
                {
                    "description": "Indices of WRF grid to use in nearest point selection"
                },
            ),
            "ind_z_1": (
                ["locs_ID"],
                ind_z_1.flatten(),
                {
                    "description": "Indices of WRF grid to use in nearest point selection"
                },
            ),
        },
        coords={"locs_ID": locs_ID},
    )

    return weights_ds


def pre_process(wrf_ds, vartime="Times", varlevs="LEVS", t_format="%Y-%m-%d_%H:%M:%S"):
    """
    Function that returns a dataset with the time decoded and with variables
    for the cartesian coordinates of wind velocity WSx, WSy

    Parameters
    ----------
    wrf_ds: xarray.Dataset
        WRF simulation Dataset
    vartime: str, optional, default='Times'
        Name of time variable
    varlevs: str, optional, default='LEVS'
        Name of the levels variable
    t_format: str, optional, default='%Y-%m-%d_%H:%M:%S'
        Format fo datetime
    """

    wrf_ds["datetime"] = xr.DataArray(
        dims=["Time"],
        data=pd.to_datetime(np.char.decode(wrf_ds.get(vartime)), format=t_format),
    )
    wrf_ds["lev"] = wrf_ds.get(varlevs)

    wrf_ds = wrf_ds.set_coords(["lev"])
    wrf_ds = wrf_ds.drop([vartime, varlevs, "datetime"])

    # wrf_ds["WSx"] = wrf_ds.WSPD * np.cos(np.deg2rad(270 - wrf_ds.WDIR))
    # wrf_ds["WSy"] = wrf_ds.WSPD * np.sin(np.deg2rad(270 - wrf_ds.WDIR))

    return wrf_ds


def read_projections(meso_dir, static_template, domains):
    """
    Function that returns the wrf projection properties for
    converting lat,lon to regular grid coordinates used in wrf.

    If it fails to read the projection coordinates it returns a None. When
    applied a None projection implies the domain is regular in lat, long
    instead of in Cartesian coordinates from a projection.

    Parameters
    ----------
    meso_dir: str
        meso-scale data directory

    static_template: str
        Static meso file template for multiple regional analysis.

    domains: list[str]
        List of domain names
    """

    # read static_file and extract wrf proj
    wrf_proj = dict()
    domain_x = dict()
    domain_y = dict()
    for domain in domains:
        static_fn = os.path.join(meso_dir, static_template.replace("{domain}", domain))
        if os.path.isfile(static_fn):
            # print('Reading file ' + static_fn)
            static = xr.open_dataset(static_fn)
            # print(static)

            # Find the XLONG variable name
            if ("XLONG" in list(static.coords)) or ("XLONG" in list(static.variables)):
                var_lon = "XLONG"
            elif ("XLON" in list(static.coords)) or ("XLON" in list(static.variables)):
                var_lon = "XLON"
            else:
                print("Unknown longitude variable name in the static file")
                warnings.warn(
                    "The static file "
                    + static_fn
                    + " has an unknown longitude variable name.",
                    CorRES_Meso_extractor_warn,
                )

            # Be flexible with the shapes in the static file
            if len(static.XLAT.shape) == 2:
                ref_lat = (static.XLAT.values[0, 0],)  # origin
                ref_lon = (static.get(var_lon).values[0, 0],)  # origin
            elif len(static.XLAT.shape) == 3:
                ref_lat = (static.XLAT.values[0, 0, 0],)  # origin
                ref_lon = (static.get(var_lon).values[0, 0, 0],)  # origin

            try:
                # define a dictionary with the wrf projection
                # ready to be passed to wrf library.
                wrf_proj[domain] = dict(
                    map_proj=static.MAP_PROJ,
                    truelat1=static.TRUELAT1,
                    truelat2=static.TRUELAT2,
                    stand_lon=static.STAND_LON,
                    ref_lat=ref_lat,  # origin
                    ref_lon=ref_lon,  # origin
                    known_x=0.0,  # origin
                    known_y=0.0,  # origin
                    dx=static.DX,
                    dy=static.DY,
                )
                if "POLE_LAT" in list(static.attrs.keys()):
                    wrf_proj[domain]["pole_lat"] = static.POLE_LAT
                if "POLE_LAT" in list(static.attrs.keys()):
                    wrf_proj[domain]["pole_lon"] = static.POLE_LON

                # Extract the x,y values present in the static file
                domain_x[domain] = static.west_east.values
                domain_y[domain] = static.south_north.values
            except BaseException:
                warnings.warn(
                    "The static file "
                    + static_fn
                    + " does not include the wrf porjection info",
                    CorRES_Meso_extractor_warn,
                )
                wrf_proj[domain] = None

            static.close()
            del static
        else:
            warnings.warn(static_fn + " does not exists", CorRES_Meso_extractor_warn)
            wrf_proj[domain] = None

    return wrf_proj, domain_x, domain_y


def apply_interpolation_f(
    wrf_ds,
    weights_ds,
    vars_xy_logz=["WSPD"],
    vars_xyz=["WDIR", "RHO"],
    vars_xy=["UST", "RMOL", "TAIR", "DIF_AVG", "DNI_AVG"],
    vars_nearest_xy=[],
    vars_nearest_xyz=[],
    var_x_grid="west_east",
    var_y_grid="south_north",
    var_z_grid="height",
    varWD="WDIR",
):
    """
    Function that applies interpolation to a wrf simulation.

    Parameters
    ----------
    wrf_ds: xarray.Dataset
        Weather timeseries
    weights_ds: xarray.Dataset
        Weights for locs interpolation for several methods::

            <xarray.Dataset>
            Dimensions:        (ix: 4, iy: 4, iz: 5, loc: 14962)
            Coordinates:
            * loc            (loc) int64
            Dimensions without coordinates: ix, iy, iz
            Data variables:
                weights_x      (loc, ix) float64
                ind_x          (loc, ix) int64
                weights_y      (loc, iy) float64
                ind_y          (loc, iy) int64
                weights_z      (loc, iz) float64
                weights_log_z  (loc, iz) float64
                ind_z          (loc, iz) int64
                ind_x_1        (loc)     int64
                ind_y_1        (loc)     int64
                ind_z_1        (loc)     int64

    vars_xy_logz: list
        List of variables to be interpolated in horizontal (x,y) using finite
        differences and power law piecewise interpolation in z.
    vars_xyz: list
        List of variables to be interpolated in horizontal (x,y) using finite
        differences and linear piecewise interpolation in z.
    vars_xy: list
        List of variables to be interpolated in horizontal (x,y) using finite
        differences
    vars_nearest_xy: list
        List of variables to be approximated to the nearest horizontal point (x,y)
    vars_nearest_xyz: list
        List of variables to be approximated to the nearest point (x,y,z)
    var_x_grid: string, default:'west_east'
        Name of the variable in the weather data used as x in the interpolation
    var_y_grid: string, default: 'south_north'
        Name of the variable in the weather data used as y in the interpolation
    var_z_grid: string, default:'height'
        Name of the variable in the weather data used as z in the interpolation
    varWD: string, default:'wd'
        Name of the wind direction variable for ensuring it is in [0,360]

    Returns
    --------
    interp: xarray.Dataset
        Dataset including meso-variables timeseries, interpolated at each locs.
        The arrays have two dimensions: ('Time', 'locs').

    """

    interp = xr.Dataset()

    # power law profile in z
    for var in vars_xy_logz:
        if var not in ["WSPD", "WS", "ws", "wspd"]:
            interp[var] = (
                wrf_ds.get(var).isel(
                    {
                        var_x_grid: weights_ds.ind_x,
                        var_y_grid: weights_ds.ind_y,
                        var_z_grid: weights_ds.ind_z,
                    }
                )
                * weights_ds.weights_x
                * weights_ds.weights_y
                * weights_ds.weights_log_z
            ).sum(["ix", "iy", "iz"])
        else:
            interp[var] = np.exp(
                (
                    np.log(wrf_ds.get(var) + 1e-12).isel(
                        {
                            var_x_grid: weights_ds.ind_x,
                            var_y_grid: weights_ds.ind_y,
                            var_z_grid: weights_ds.ind_z,
                        }
                    )
                    * weights_ds.weights_x
                    * weights_ds.weights_y
                    * weights_ds.weights_log_z
                ).sum(["ix", "iy", "iz"])
            )

    # linear profile in z
    for var in vars_xyz:
        interp[var] = (
            wrf_ds.get(var).isel(
                {
                    var_x_grid: weights_ds.ind_x,
                    var_y_grid: weights_ds.ind_y,
                    var_z_grid: weights_ds.ind_z,
                }
            )
            * weights_ds.weights_x
            * weights_ds.weights_y
            * weights_ds.weights_z
        ).sum(["ix", "iy", "iz"])

    # only horizontal interpolation
    for var in vars_xy:
        interp[var] = (
            wrf_ds.get(var).isel(
                {
                    var_x_grid: weights_ds.ind_x,
                    var_y_grid: weights_ds.ind_y,
                }
            )
            * weights_ds.weights_x
            * weights_ds.weights_y
        ).sum(["ix", "iy"])

    # nearest horizontal point approximation
    for var in vars_nearest_xy:
        interp[var] = wrf_ds.get(var).isel(
            {
                var_x_grid: weights_ds.ind_x_1,
                var_y_grid: weights_ds.ind_y_1,
            }
        )

    # nearest point approximation
    for var in vars_nearest_xyz:
        interp[var] = wrf_ds.get(var).isel(
            {
                var_x_grid: weights_ds.ind_x_1,
                var_y_grid: weights_ds.ind_y_1,
                var_z_grid: weights_ds.ind_z_1,
            }
        )

    if varWD in vars_xy_logz + vars_xyz + vars_xy + vars_nearest_xy + vars_nearest_xyz:
        interp[varWD] = np.mod(interp[varWD], 360)

    return interp


def apply_interpolation_from_fn(
    wrf_fn,
    weights_fn,
    out_fn,
    vartime,
    varlevs,
    vars_xy_logz,
    vars_xyz,
    vars_xy,
    vars_nearest_xy,
    vars_nearest_xyz,
):
    """
    Function that reads a wrf simulation, applies interpolation and stores the
    outputs into a file, if the file exists then appends the results by
    time.

    Parameters
    ----------
    wrf_fn: str
        wrf simulation file name
    weights_fn: str
        Interpolation weights file name
    out_fn: str
        Output file name (should include .nc)
    """

    with xr.open_dataset(weights_fn) as weights_ds:
        weights_ds.load()
    locs_ID = weights_ds.locs_ID.values

    # if a single file is passed
    if isinstance(wrf_fn, str):
        wrf_fn = [wrf_fn]

    def pre_process_ins(ds):
        return pre_process(ds, vartime, varlevs)

    with xr.open_mfdataset(
        wrf_fn, combine="by_coords", concat_dim="Time", preprocess=pre_process_ins
    ) as wrf_ds:
        wrf_ds.load()

    # apply interpolation
    ds_interpolated = apply_interpolation_f(
        wrf_ds,
        weights_ds,
        vars_xy_logz,
        vars_xyz,
        vars_xy,
        vars_nearest_xy,
        vars_nearest_xyz,
        var_x_grid="west_east",
        var_y_grid="south_north",
        var_z_grid="lev",
        varWD="WDIR",
    )

    # def pre_process_ins(ds):
    #     wrf_ds = pre_process(ds, vartime, varlevs)
    #     ds_interpolated = apply_interpolation_f(
    #         wrf_ds,
    #         weights_ds,
    #         vars_xy_logz,
    #         vars_xyz,
    #         vars_xy,
    #         vars_nearest_xy,
    #         vars_nearest_xyz
    #     )
    #     return ds_interpolated

    # ds_interpolated = xr.open_mfdataset(
    #     paths=wrf_fn,
    #     combine='by_coords',
    #     concat_dim='Time',
    #     decode_times=False,
    #     preprocess=pre_process_ins).compute()

    # apply interpolation

    # to do later:
    # - use zarr.
    # - use mode="a"
    """
        mode: bool, optional, default="a"
        mode for file writing. Write 'w' overwrites an existing file, 'a'
        appends results by Time.
    """
    #
    # - encoding breaks the ability to append the zarr files
    # - compressor = zarr.Blosc(cname='zstd', clevel=3, shuffle=2)

    # if a compressor is passed compressed
    # encoding = {
    #    vname: {"compressor": compressor} for vname in ds_interpolated.variables
    # }

    #     # remove files if the mode is write
    #     if mode == "w":
    #         shutil.rmtree(out_fn, ignore_errors=True)
    #         ds_interpolated.to_zarr(out_fn, mode=mode)  # encoding=encoding,
    #     else:

    # ds_interpolated.to_zarr(out_fn, append_dim="Time")  # encoding=encoding,
    ds_interpolated.to_netcdf(out_fn, mode="w", engine="h5netcdf")

    weights_ds.close()
    wrf_ds.close()
    ds_interpolated.close()
    del weights_ds, wrf_ds, ds_interpolated

    # if ~watch_dog_netcdf(
    #         out_fn,
    #         N_max_tries=10,
    #         var_check='Time',
    #         min_count=24 * 3):
    #     removefile(out_fn)

    return out_fn


def get_file_to_read(domain, meso_dir, meso_dir_template, start_date, end_date):
    # extract files available
    all_fn = sorted(
        glob.glob(
            os.path.join(
                meso_dir,
                meso_dir_template.replace("{domain}", domain)
                .replace("{year}", "*")
                .replace("{month}", "*")
                .replace("{day}", "*"),
            )
        )
    )

    # determine the files from user request
    if start_date is None:
        # pick the first date out of all domains
        start_date = make_date_string(all_fn[0], meso_dir_template)
    if end_date is None:
        # pick the last date out of all domains
        end_date = make_date_string(all_fn[-1], meso_dir_template)

    dates_rng_request = pd.date_range(start_date, end_date)
    request_fn = [
        meso_dir_template.replace("{year}", str(date.year).zfill(4))
        .replace("{month}", str(date.month).zfill(2))
        .replace("{day}", str(date.day).zfill(2))
        .split("/")[-1]
        .split(".")[0]
        for date in dates_rng_request
    ]

    # extract files to be read
    fn_to_read = [fn for fn in all_fn if get_shortname(fn) in request_fn]

    return fn_to_read


def get_ds(batch, meso_dir_template, vars_needed):
    start_date = make_date_string(batch[0], meso_dir_template)
    end_date = make_date_string(batch[-1], meso_dir_template)
    ds = xr.open_mfdataset(
        batch, concat_dim="Time", decode_times=False, combine="nested"
    )
    ds = ds.rename(
        {
            "WSPD": "WS",
            "WDIR": "WD",
            "DIF_AVG": "DHI",
            "DNI_AVG": "DNI",
            "Time": "time",
            "lev": "height",
        }
    )
    ds["time"] = pd.date_range(
        start=f"{start_date} 00:00:00", end=f"{end_date} 23:00:00", freq="1h"
    )
    ds["height"] = ds.LEVS.isel(time=0).values
    ds["RHO"] = ds.RHO.sel(height=100)
    ds = ds.drop(["Times", "LEVS", "SUN_DUR", "WS10"])
    ds = ds.drop([var for var in ds.variables if var not in vars_needed])
    ds = ds.chunk({"time": 2208, "south_north": 50, "west_east": 50})
    return ds


def read_projections_zarr(
    static,
    domain,
    var_lon="longitude",
    var_lat="latitude",
):
    """
    Function that returns the wrf projection properties for
    converting lat,lon to regular grid coordinates used in wrf.

    If it fails to read the projection coordinates it returns a None. When
    applied a None projection implies the domain is regular in lat, long
    instead of in Cartesian coordinates from a projection.

    Parameters
    ----------
    static: Dataset
        Meso-scale xarray dataset including Lat, Lon and wrf projection

    domains: str
        Domain name
    """

    # Be flexible with the shapes in the static file
    if len(static[var_lat].shape) == 2:
        ref_lat = (static[var_lat].values[0, 0],)  # origin
        ref_lon = (static[var_lon].values[0, 0],)  # origin
    elif len(static[var_lat].shape) == 3:
        ref_lat = (static[var_lat].values[0, 0, 0],)  # origin
        ref_lon = (static[var_lon].values[0, 0, 0],)  # origin

    try:
        # define a dictionary with the wrf projection
        # ready to be passed to wrf library.
        wrf_proj = dict(
            map_proj=static.MAP_PROJ,
            truelat1=static.TRUELAT1,
            truelat2=static.TRUELAT2,
            stand_lon=static.STAND_LON,
            ref_lat=ref_lat,  # origin
            ref_lon=ref_lon,  # origin
            known_x=0.0,  # origin
            known_y=0.0,  # origin
            dx=static.DX,
            dy=static.DY,
        )
        if "POLE_LAT" in list(static.attrs.keys()):
            wrf_proj["pole_lat"] = static.POLE_LAT
        if "POLE_LAT" in list(static.attrs.keys()):
            wrf_proj["pole_lon"] = static.POLE_LON

    except BaseException:
        warnings.warn(
            "The file " + meso_dir + " does not include the wrf porjection info",
            CorRES_Meso_extractor_warn,
        )
        wrf_proj = None

    return wrf_proj


def project_locations(
    locs,
    region_domain_fn,
    ds,
    domain,
    var_lon="longitude",
    var_lat="latitude",
):
    """
    Function that uses the wrf projection properties for
    converting the locations (lat,lon) to regular grid coordinates used in wrf.

    Parameters
    ----------
    locs: Dataframe
        Table with locations including Latitude and Longitude

    region_domain_fn: str
        Filename of region_domain_fn excel file. To describe wich domain to use per
        region (or country).

    ds: Dataset
        Meso-scale xarray dataset including Lat, Lon and wrf projection

    domains: str
        Domain name
    """
    locs = locs.copy()

    if domain is None:
        locs_in_domain = locs
        wrf_proj = read_projections_zarr(ds, domain="")
    else:
        regdom = pd.read_excel(region_domain_fn)

        if "WRF_Domain" not in locs.columns:
            # Add a column to identify on which domain each locs belongs
            if "Country" in locs.columns:
                locs["WRF_Domain"] = [
                    regdom.loc[regdom.Country == country, "WRF_Domain"].tolist()[0]
                    for country in locs.Country.values
                ]
            else:
                locs["WRF_Domain"] = domain

        # extract available domains from the WRF files
        wrf_proj = read_projections_zarr(ds, domain, var_lon=var_lon, var_lat=var_lat)

        locs_in_domain = locs.loc[locs["WRF_Domain"] == domain, :]

    # Get wrf projection coordinates of the locations
    ds_x_y = wrf.ll_to_xy_proj(
        latitude=locs_in_domain.Latitude.values,
        longitude=locs_in_domain.Longitude.values,
        as_int=False,
        **wrf_proj,
    )
    locs_x = ds_x_y.values[0, :]
    locs_y = ds_x_y.values[1, :]
    locs_z = locs_in_domain.Hub_height.values

    locs_in_domain.loc[:, "x"] = locs_x
    locs_in_domain.loc[:, "y"] = locs_y
    return locs_in_domain


def apply_interpolation_IDW(ds_dssr, px, var="ghi", n_neighbors=4, IDW_p=2):
    """
    Function that interpolates as dataset using inverse distance weighting (IDW).
    Usefull for unstructured datasets or with nan's on several locations.

    Parameters
    ----------
    ds_dssr: Dataset
        Dataset with latitude and longitude as coordinates

    px: array
        points to interpolate in [lon,lat]

    var: str
        Variable to interpolate available in ds_dssr

    n_neighbors: int
        Number of neares neighbor points to use in the interpolation

    IDW_p: float
        IDW exponent coefficient to compute the weitghs = 1/d**IDW_p
    """

    ds_dssr["ilat"] = xr.DataArray(
        data=np.arange(len(ds_dssr.latitude)), dims={"latitude": ds_dssr.latitude}
    )
    ds_dssr["ilon"] = xr.DataArray(
        data=np.arange(len(ds_dssr.longitude)), dims={"longitude": ds_dssr.longitude}
    )

    locs_with_values = ds_dssr.max(dim="time", skipna=True).compute()
    locs_with_values = (
        locs_with_values[["latitude", "longitude", "ilon", "ilat", var]]
        .to_dataframe()
        .reset_index()
    )
    locs_with_values.columns = ["latitude", "longitude", "ilon", "ilat", var]
    locs_with_values = locs_with_values.loc[locs_with_values[var] > 0]

    nx = NearestNeighbors(n_neighbors=n_neighbors).fit(
        locs_with_values.loc[:, ["latitude", "longitude"]].values
    )
    dx, ix = nx.kneighbors(px, return_distance=True)

    # add a small distance to avoid dividing by 0
    weights = 1 / (dx + 1e-12) ** IDW_p
    nnp = ix.shape[0]
    nnx = ix.shape[1]

    xr_weights = xr.Dataset(
        data_vars={
            "weights": (
                ["locs_ID", "ix"],
                weights,
                {"description": "Interpolation weights, IDW"},
            ),
        },
        coords={"locs_ID": np.arange(nnp)},
    )

    # Select the nearest points in ds_dssr
    xr_sel = xr.concat(
        [
            xr.concat(
                [
                    ds_dssr[var].isel(
                        longitude=locs_with_values.ilon.values[ixi],
                        latitude=locs_with_values.ilat.values[ixi],
                    )
                    for ixi in ix[ip, :]
                ],
                dim="ix",
            )
            for ip in range(nnp)
        ],
        dim="locs_ID",
    )

    interp_IDW = xr.Dataset()
    interp_IDW[var] = (xr_weights.weights * xr_sel).sum("ix") / xr_weights.weights.sum(
        "ix"
    )

    return interp_IDW


def add_mean_wind_speeds_cols(locs, config, dataset_name, exec_type):
    dataset_dict = config["weather_data"][dataset_name]

    path = dataset_dict[exec_type]["mean_ws"]
    var_x_grid = dataset_dict["var_x_grid"]
    var_y_grid = dataset_dict["var_y_grid"]
    var_z_grid = dataset_dict["var_z_grid"]

    var_x = dataset_dict["var_x"]
    var_y = dataset_dict["var_y"]
    var_z = dataset_dict["var_z"]

    _, fileext = os.path.splitext(path)

    if fileext == ".nc":
        ds = xr.open_dataset(path)
    elif fileext == ".zarr":
        if "ERA5_mean_WS_2006_2018" in path:
            ds = xr.open_zarr(path, consolidated=False)
        else:
            ds = xr.open_zarr(path)

    dslog = ds.copy()
    dslog = dslog.assign_coords(height=(("height",), np.log(dslog.height.values)))

    ds_pts = locs.loc[:, [var_x, var_y, var_z]].to_xarray()

    x = xr.DataArray(locs[var_x], dims=("pt",))
    y = xr.DataArray(locs[var_y], dims=("pt",))

    z = xr.DataArray(locs[var_z], dims=("pt",))

    locs["vals"] = dslog.WS.interp(
        {var_x_grid: x, var_y_grid: y, var_z_grid: np.log(z)},
        kwargs={"bounds_error": False, "fill_value": None},
    )

    not_nan_mask = locs["vals"].notnull().values
    locs = locs.drop(["vals"], axis=1)

    locs[dataset_name + "_mean_WS"] = [None] * len(locs)

    if len(ds_pts[var_x].values[not_nan_mask]) == 0:
        locs.loc[not_nan_mask, dataset_name + "_mean_WS"] = [1] * len(locs)
    else:
        weights_cordex = get_interpolation_weights(
            px=ds_pts[var_x].values[not_nan_mask],
            py=ds_pts[var_y].values[not_nan_mask],
            pz=ds_pts[var_z].values[not_nan_mask],
            all_x=ds[var_x_grid].values,
            all_y=ds[var_y_grid].values,
            all_z=ds[var_z_grid].values,
            n_stencil=2,
            locs_ID=ds_pts.index.values[not_nan_mask],
        )

        cordex_interp = apply_interpolation_f(
            wrf_ds=ds,
            weights_ds=weights_cordex,
            vars_xy_logz=["WS"],
            vars_xyz=[],
            vars_xy=[],
            vars_nearest_xy=[],
            vars_nearest_xyz=[],
            var_x_grid=var_x_grid,
            var_y_grid=var_y_grid,
            var_z_grid=var_z_grid,
        )

        locs.loc[not_nan_mask, dataset_name + "_mean_WS"] = cordex_interp.WS.values

    return locs


def calculate_ratio_datasets(locs, config, dataset, mean_WS_scaler, exec_type):

    locs = add_mean_wind_speeds_cols(locs, config, dataset, exec_type)
    locs = add_mean_wind_speeds_cols(locs, config, mean_WS_scaler, exec_type)
    locs["ratio_" + mean_WS_scaler + "_" + dataset + "_mean_WS"] = (
        locs[mean_WS_scaler + "_mean_WS"] / locs[dataset + "_mean_WS"]
    )
    mask_nan = locs["ratio_" + mean_WS_scaler + "_" + dataset + "_mean_WS"].isna()
    locs.loc[mask_nan, "ratio_" + mean_WS_scaler + "_" + dataset + "_mean_WS"] = 1

    return locs
