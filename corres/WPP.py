# basic libraries
import time
from collections import defaultdict

import numpy as np
import pandas as pd
import xarray as xr
from dask import compute
from dask import dataframe as df_dsk
from dask import delayed
from numpy import newaxis as na

from corres.auxiliar_functions import split_in_batch


def computes_generation_WPP_distributed(
    WPP,
    ds_interp_fn,
    power_curves: str | xr.Dataset,
    storm_shutdown=True,
    varWS="WS",
    varWD="WD",
    save_temp_xr=False,
    n_batch=10,
):
    """
    Makes the prediction of the time series of PV production for multiple solar
    power plants using dask distributed computations.

    Parameters
    ----------

    SPP: DataFrame
        Solar power plants table including properties of each plant: Name, Latitude,
        Longitude, Surface_tilt, Surface_azimuth.

    ds_interp_fn: Str
        Filename of the netcdf with the time series of the irradiances at each WPP.

    """

    def transform_to_lists(data):
        # Create a defaultdict of lists
        result = defaultdict(list)

        # Iterate through each dictionary in the list
        for item in data:
            for key, value in item.items():
                result[key].append(value)

        # Convert defaultdict to a regular dict before returning
        return dict(result)

    batches = split_in_batch(WPP.index.tolist(), n_batch)

    dly = []
    for ib, batch in enumerate(batches):
        if len(batch) < 1:
            continue

        locs_batch = WPP.loc[batch, :]
        if isinstance(power_curves, str):
            dly.append(
                delayed(computes_generation_WPP)(
                    WPP=locs_batch,
                    ds_interp_fn=ds_interp_fn,
                    power_curves_fn=power_curves,
                    storm_shutdown=storm_shutdown,
                    batch=batch,
                    varWS=varWS,
                    varWD=varWD,
                    save_temp_xr=save_temp_xr,
                )
            )
        elif isinstance(power_curves, xr.Dataset):
            dly.append(
                delayed(computes_generation_WPP_ext_ml_table)(
                    WPP=locs_batch,
                    ds_interp_fn=ds_interp_fn,
                    pc_table=power_curves,
                    storm_shutdown=storm_shutdown,
                    batch=batch,
                )
            )

    dly_computed = []
    for task in dly:
        dly_computed.append(compute(task)[0])

    dly_computed_dict = transform_to_lists(dly_computed)
    out = dict()

    for iter1 in dly_computed_dict.keys():
        out[iter1] = df_dsk.concat(dly_computed_dict[iter1], axis=1)

    return out


def computes_generation_WPP_ext_ml_table(
    WPP: pd.DataFrame,
    ds_interp_fn: str,
    pc_table: xr.Dataset,
    storm_shutdown: bool = True,
    batch: list = None,
):
    """Makes the prediction of the time series of wind production for multiple wind
    power plants (WPP).

    Parameters
    ----------
    WPP : pd.DataFrame
        Wind power plants table including plant name, Power_curve and plant storm
        shutdown parameters:  'Name', 'Power_curve', 'ws_shutdown_begin',
        'ws_shutdown_end', 'ws_restart_begin', 'ws_restart_end'

    ds_interp_fn : str
        Filename of the netcdf with the time series of the weather conditions at each WPP.

    pc_table : xr.Dataset
        Generic power curve definition, can be a function of wind speed and air density.

    storm_shutdown : bool, optional
        Decides if to apply storm shutdown to power output, by default True.

    batch : list, optional
        List of names of plants to evaluate, by default None to evaluate all at once.
    """
    ds = xr.open_dataset(ds_interp_fn)
    if batch is None:
        batch = ds.locs_ID.values
    WPP = WPP.loc[batch, :]
    ds = ds.sel(locs_ID=batch)
    list_plants = WPP["locs_ID"].values

    if "RHO" in ds.data_vars:
        ds["Air_density"] = ds.RHO
    if "Air_density" not in ds.data_vars:
        print("Using default value of air density!")
        rho_values = np.ones((ds.time.size, ds.locs_ID.size))
        for loc_id in list_plants:
            index = np.where(ds.locs_ID.values == loc_id)[0][0]
            rho_values[:, index] = WPP.loc[loc_id, "Air_density"]
        ds["Air_density"] = xr.DataArray(
            data=rho_values,
            coords={"time": ds.time.values, "locs_ID": ds.locs_ID.values},
            dims=["time", "locs_ID"],
        )
    assert ds.Air_density.dims == ("time", "locs_ID")

    pc_ds = pc_table
    ds_out = xr.Dataset()
    for i, loc_id in enumerate(list_plants):
        pc = pc_ds.pc.sel(iloc=loc_id)
        # extend WS range
        ws_spacing = pc.ws.values[1] - pc.ws.values[0]
        ws_fill_values = np.arange(ws_spacing, 50.0 + ws_spacing, ws_spacing)
        pc = pc.interp(ws=ws_fill_values, kwargs={"fill_value": "extrapolate"})
        ds_aux = pc.interp(
            ws=ds.sel(locs_ID=loc_id).WS,
            Air_density=ds.sel(locs_ID=loc_id).Air_density,
            # some of the air density exceed the range in the table
            kwargs={"fill_value": None},
        )
        ds_aux = ds_aux.drop("Air_density")

        if i == 0:
            ds_P = ds_aux
        else:
            ds_P = xr.concat([ds_P, ds_aux], dim="locs_ID", coords="all")

    ds_out["P"] = ds_P

    if storm_shutdown:
        for plant in list_plants:

            name = WPP.loc[plant, "Name"]
            (
                ws_shutdown_begin,
                ws_shutdown_end,
                ws_restart_begin,
                ws_restart_end,
            ) = WPP.loc[
                WPP.Name == name,
                [
                    "ws_shutdown_begin",
                    "ws_shutdown_end",
                    "ws_restart_begin",
                    "ws_restart_end",
                ],
            ].values[
                0
            ]

            plant_pc = ds_out.sel(locs_ID=plant)

            ws = plant_pc["ws"]
            all_times = plant_pc["time"].values

            first_step_flag = False

            if ws.values[0] > ws_restart_end:
                first_step_flag = True
                init_step = ws[0].values
                ws.data[0] = ws_restart_end

            # all_times = ws.index
            wind_storms = pd.DataFrame(index=all_times)
            wind_storms["above_ws_restart_end"] = (ws > ws_restart_end) + 0
            wind_storms["diff_above_ws_restart_end"] = np.append(
                np.diff(wind_storms.above_ws_restart_end + 0), [0]
            )
            wind_storms.reset_index(inplace=True)

            storm_start_time = wind_storms.loc[
                wind_storms.diff_above_ws_restart_end > 0, :
            ].index.values
            storm_end_time = wind_storms.loc[
                wind_storms.diff_above_ws_restart_end < 0, :
            ].index.values
            if len(storm_start_time) > len(storm_end_time):
                storm_end_time = np.append(storm_end_time, [len(all_times) - 1])
            elif len(storm_start_time) < len(storm_end_time):
                storm_start_time = np.append([0], storm_start_time)

            assert len(storm_start_time) == len(storm_end_time)

            new_plant_pc = apply_storm_shutdown(
                ws.values,
                plant_pc["P"].values,
                storm_start_time,
                storm_end_time,
                ws_shutdown_begin,
                ws_shutdown_end,
                ws_restart_begin,
                ws_restart_end,
            )

            if first_step_flag:
                ws.data[0] = init_step

            # assign new power curve to ds_out
            ds_out["P"].loc[dict(locs_ID=plant)] = new_plant_pc

    if "Efficiency" in WPP.columns:
        efficiency = WPP.loc[list_plants, "Efficiency"].values[:]
        efficiency = np.tile(efficiency[:, np.newaxis], (1, ds_out["P"].shape[1]))
        ds_out["P"] = efficiency * ds_out["P"]

    df_var = dict()
    # df_var["P"] = (
    #    ds_out.P.to_dataframe()
    #    .reset_index()
    #    .pivot_table(values="P", index="time", columns="locs_ID")
    # )
    # df_var["P"].columns.name = None
    # df_var["P"].columns = WPP.loc[list_plants, "Name"].values
    ds_out = ds_out.P.to_dask_dataframe().reset_index().categorize(columns=["locs_ID"])

    df_var["P"] = (
        ds_out.pivot_table(values="P", index="time", columns="locs_ID")
    ).repartition(npartitions=2)

    for iter1 in ds.keys():
        ds_daskdf = (
            ds[iter1].to_dask_dataframe().reset_index().categorize(columns=["locs_ID"])
        )

        # df_var[iter1] = ds_daskdf.pivot_table(
        #    values=iter1, index="time", columns="locs_ID"
        # ).repartition(npartitions=2)
        df_var[iter1] = ds_daskdf.pivot_table(
            values=iter1, index="time", columns="locs_ID"
        ).repartition(npartitions=2)

    return df_var


def computes_generation_WPP(
    WPP,
    ds_interp_fn,
    power_curves_fn,
    storm_shutdown=True,
    batch=None,
    varWS="WS",
    varWD="WD",
    save_temp_xr=False,
):
    """
    Makes the prediction of the time series of wind production for multiple wind
    power plants (WPP).

    Parameters
    ----------

    WPP: DataFrame
        Wind power plants table including plant name, Power_curve and plant storm
        shutdown parameters:  'Name', 'Power_curve',
        'ws_shutdown_begin', 'ws_shutdown_end', 'ws_restart_begin', 'ws_restart_end'

    ds_interp_fn: Str
        Filename of the netcdf with the time series of the irradiances at each WPP.

    power_curves_fn: str
        xlsx filename with the power curves per plant.

        The power curves can be only a function of ws, or per wind direction.
        The Name of the power curve should match one of the WPP.Power_curve

        WD dependent plant power curves must have the following columns
        ['Name','wd'] + ws_range

    storm_shutdown: Boolean
        Flag to evaluate the plant storm shutdown model.

    batch: list, default = []
        List of names of plants to evaluate

    """

    ds = xr.open_dataset(ds_interp_fn)
    if batch is None:
        batch = ds.locs_ID.values

    WPP = WPP.loc[batch, :]
    ds = ds.sel(locs_ID=batch)
    list_pc = WPP["Power_curve"].values
    list_plants = WPP["locs_ID"].values
    pc_df = pd.read_excel(power_curves_fn)
    pc_ds = convert_power_curves_to_xarray(pc_df, list_plants, list_pc)

    # get the power by interpolating the power curves in xarray
    ds_out = xr.Dataset()
    for i, loc_id in enumerate(list_plants):
        if "wd" in pc_df.columns:
            # wind speed and wind direction dependent power curves
            ds_aux = pc_ds.pc.sel(locs_ID=loc_id).interp(
                WS=ds.sel(locs_ID=loc_id).WS, WD=ds.sel(locs_ID=loc_id).WD
            )
        else:
            # 1D power curves: wind speed dependent
            ds_aux = pc_ds.pc.sel(locs_ID=loc_id).interp(WS=ds.sel(locs_ID=loc_id).WS)

        if i == 0:
            ds_P = ds_aux
        else:
            ds_P = xr.concat([ds_P, ds_aux], dim="locs_ID", coords="all")

    ds_out["P"] = ds_P.drop("WS")

    # Apply stormshutdown
    if storm_shutdown:
        P_all = []
        for ii in list_plants:
            name = WPP.loc[ii, "Name"]

            (
                pc_out,
                ws_shutdown_begin,
                ws_shutdown_end,
                ws_restart_begin,
                ws_restart_end,
                ws_HWS,
                p_HWS,
            ) = get_pc_shutdown_parameters(
                df_plant_power_curves=pc_df, WPP=WPP, name=name
            )

            ws = ds.WS.sel(locs_ID=ii, drop=True).to_dataframe()
            P = ds_out.P.sel(locs_ID=ii, drop=True).to_dataframe()

            P_storm = get_P_storm_shutdown(
                ws,
                P,
                pc_out,
                ws_shutdown_begin,
                ws_shutdown_end,
                ws_restart_begin,
                ws_restart_end,
                ws_HWS,
                p_HWS,
                storm_shutdown=storm_shutdown,
            )

            P_all += [P_storm]

        # Update the Power

        # check if wd
        if P_all[0].shape[1] == 2:

            P_all_P = [array[:, 1] for array in P_all]

            ds_out["P"] = xr.DataArray(
                data=P_all_P,
                coords={"time": ds_out.time.values, "locs_ID": ds_out.locs_ID.values},
                dims=["locs_ID", "time"],
            )

        else:
            ds_out["P"] = xr.DataArray(
                data=np.hstack(P_all).T,
                coords={"time": ds_out.time.values, "locs_ID": ds_out.locs_ID.values},
                dims=["locs_ID", "time"],
            )
    # Apply efficiency if available
    if "Efficiency" in WPP.columns:
        efficiency = WPP.loc[list_plants, "Efficiency"].values[:]
        efficiency = np.tile(efficiency[:, np.newaxis], (1, ds_out["P"].shape[1]))
        ds_out["P"] = efficiency * ds_out["P"]

    df_var = dict()

    ds_out = ds_out.P.to_dask_dataframe().reset_index().categorize(columns=["locs_ID"])

    df_var["P"] = (
        ds_out.pivot_table(values="P", index="time", columns="locs_ID")
    ).repartition(npartitions=2)
    # df_var["P"] = (
    #    ds_out.pivot_table(values="P", index="time", columns="locs_ID")
    # ).repartition(npartitions=2)

    # df_var["P"].columns.name = None
    # df_var["P"].columns = WPP.loc[list_plants, "Name"].values

    for iter1 in ds.keys():
        ds_daskdf = (
            ds[iter1].to_dask_dataframe().reset_index().categorize(columns=["locs_ID"])
        )

        # df_var[iter1] = ds_daskdf.pivot_table(
        #    values=iter1, index="time", columns="locs_ID"
        # ).repartition(npartitions=2)
        df_var[iter1] = ds_daskdf.pivot_table(
            values=iter1, index="time", columns="locs_ID"
        ).repartition(npartitions=2)

        # df_var[iter1].columns.name = None
        # df_var[iter1].columns = WPP.loc[list_plants, "Name"].values

    if save_temp_xr:
        ds_out.to_netcdf(ds_interp_fn.replace("_meso_", "_P_"))

    return df_var


# --------------------------------------------------------------------------------
# Auxiliar functions to compute power
# --------------------------------------------------------------------------------


def convert_power_curves_to_xarray(pc_df, list_plants, list_pc):
    plant_pc_dict = dict(zip(list_plants, list_pc))

    if "wd" in pc_df.columns:
        all_pcs = np.stack(
            [
                pc_df.loc[pc_df.Power_curve == plant_pc_dict[plant], :]
                .drop(columns="Power_curve")
                .set_index("wd")
                .values
                for plant in list_plants
            ]
        )
        pc_ds = xr.Dataset(
            data_vars={
                "pc": (
                    ["locs_ID", "WD", "WS"],
                    all_pcs,
                    {"description": "plant wake affected power curves"},
                ),
            },
            coords={
                "locs_ID": list_plants,
                "WD": pc_df.wd.unique(),
                "WS": pc_df.columns[2:].astype(float).values,
            },
        )

        ds_aux = pc_ds.sel(WD=0)
        ds_aux["WD"] = 360

        pc_ds = xr.concat([pc_ds, ds_aux], dim="WD")

    else:

        all_pcs = np.stack(
            [
                pc_df.loc[pc_df.Power_curve == plant_pc_dict[plant], :]
                .drop(columns="Power_curve")
                .values.flatten()
                for plant in list_plants
            ]
        )
        pc_ds = xr.Dataset(
            data_vars={
                "pc": (
                    ["locs_ID", "WS"],
                    all_pcs,
                    {"description": "plant wake affected power curves"},
                ),
            },
            coords={
                "locs_ID": list_plants,
                "WS": pc_df.columns[1:].astype(float).values,
            },
        )
    return pc_ds


def get_pc_shutdown_parameters(df_plant_power_curves, WPP, name):
    """
    Function that returns the plant power curve (xarray) for a given plant
    name, using then storm shutdown parameters in the WPP table and the
    df_plant_power_curves.

    The power curves must have possitive values of power until a high enough
    wind speed (i.e. 50.0), this means that the cut-off wind speed should be ignored since the storm shutdown will deal with the cut-off.

    Parameters
    ----------
    df_plant_power_curves: DataFrame
        Plant power curves. columns = ['Power_curve','wd'] + ws_range
        Power_curve should match one of the WPP.Power_curve

    WPP: DataFrame
        Wind power plants tables including Power_curve and storm_shutdown parameters, i.e. following variables:  'Power_curve', 'ws_shutdown_begin', 'ws_shutdown_end', 'ws_restart_begin', 'ws_restart_end'

    name: str
        Name of the plant to process.

    """
    if name not in WPP.Name.values:
        print(f"error: {name} not in WPP")
        return None, None, None, None, None, None
    else:
        power_curve_name = WPP.loc[WPP.Name == name, "Power_curve"].values[0]
        pass

    if "wd" in df_plant_power_curves.columns:
        # use wd=0 power curve for shutdown curves estimation
        cols_table = [
            col
            for col in df_plant_power_curves.columns
            if ("Power_curve" not in str(col)) & ("wd" not in str(col))
        ]

        pc = df_plant_power_curves.loc[
            (df_plant_power_curves.Power_curve == power_curve_name)
            & (df_plant_power_curves.wd == 0),
            cols_table,
        ]

        pc_out = df_plant_power_curves.loc[
            df_plant_power_curves.Power_curve == power_curve_name, cols_table + ["wd"]
        ]
    else:
        cols_table = [
            col
            for col in df_plant_power_curves.columns
            if ("Power_curve" not in str(col)) & ("wd" not in str(col))
        ]

        pc = df_plant_power_curves.loc[
            df_plant_power_curves.Power_curve == power_curve_name, cols_table
        ]

        pc_out = pc

    # Extend power curve above ws_cut_out
    ind_ws = np.where(pc.values > 0)[1][-1]
    ws_fill = cols_table[ind_ws]
    P_fill = pc.iloc[:, ind_ws].values[0]

    # Extend the power curve above cut out
    cols_table_sel = [col for col in cols_table if (col > ws_fill)]
    pc.loc[:, cols_table_sel] = P_fill

    # extract storm shutdown parameters
    ws_shutdown_begin, ws_shutdown_end, ws_restart_begin, ws_restart_end = WPP.loc[
        WPP.Name == name,
        ["ws_shutdown_begin", "ws_shutdown_end", "ws_restart_begin", "ws_restart_end"],
    ].values[0]

    # Only modify wind speeds larger than restart end.
    ws_HWS = np.array([ws for ws in pc.columns[1:] if ws >= ws_restart_end])
    p_HWS = pc[ws_HWS].mean().values

    def get_P_HWS(x):
        return np.interp(x=x, xp=ws_HWS, fp=p_HWS, left=1, right=p_HWS[-1])

    P_shutdown_begin = get_P_HWS(ws_shutdown_begin)

    def get_P_shutdown_line(x):
        return np.interp(
            x,
            np.array([ws_shutdown_begin, ws_shutdown_end]),
            np.array([P_shutdown_begin, 0]),
            get_P_HWS(ws_shutdown_begin),
            0,
        )

    # apply the shutdown to the basic power curve
    ws_above = [ws for ws in pc.columns[1:] if ws > ws_shutdown_begin]
    for ws in ws_above:
        pc[ws] = get_P_shutdown_line(ws)

    return (
        pc_out,
        ws_shutdown_begin,
        ws_shutdown_end,
        ws_restart_begin,
        ws_restart_end,
        ws_HWS,
        p_HWS,
    )


def apply_storm_shutdown(
    ws,
    P,
    storm_start_time,
    storm_end_time,
    ws_shutdown_begin,
    ws_shutdown_end,
    ws_restart_begin,
    ws_restart_end,
    ws_HWS=None,
    p_HWS=None,
):

    if ws_HWS is None or p_HWS is None:
        P_max = np.max(P)
        get_P_HWS = lambda _: P_max
    else:
        P_max = np.max(p_HWS)
        ws_HWS = ws_HWS[np.where(p_HWS == P_max)[0][0] :]
        p_HWS = p_HWS[np.where(p_HWS == P_max)[0][0] :]

        def get_P_HWS(x):
            return np.interp(x, ws_HWS, p_HWS, p_HWS[0], p_HWS[-1])

    P_shutdown_begin = get_P_HWS(ws_shutdown_begin)

    def get_P_shutdown_line(x):
        return np.interp(
            x,
            np.array([ws_shutdown_begin, ws_shutdown_end]),
            np.array([P_shutdown_begin, 0]),
            get_P_HWS(ws_shutdown_begin),
            0,
        )

    def get_ws_shutdown_line(x):
        return np.interp(
            x,
            np.array([0, P_shutdown_begin]),
            np.array([ws_shutdown_end, ws_shutdown_begin]),
            ws_shutdown_end,
            ws_shutdown_begin,
        )

    def get_P_restart_line(x):
        return np.interp(
            x,
            np.array([ws_restart_end, ws_restart_begin]),
            np.array([P_max, 0]),
            P_max,
            0,
        )

    def get_ws_restart_line(x):
        return np.interp(
            x,
            np.array([0, P_max]),
            np.array([ws_restart_begin, ws_restart_end]),
            ws_restart_begin,
            ws_restart_end,
        )

    P_new = np.copy(P)

    for i in range(len(storm_start_time)):

        for i_time in range(storm_start_time[i], storm_end_time[i] + 1):
            # if i_time>0:
            # Do nothing on the first step
            # wind.loc[time,['ws','P']].values
            ws_now, P_now = ws[i_time], P_new[i_time]
            if ws_now > ws_restart_end:
                # Only modify power at wind speeds larger than restart end
                # wind.loc[prev,['ws','P']].values
                ws_prev, P_prev = ws[i_time - 1], P_new[i_time - 1]

                ws_shutdown_pre = get_ws_shutdown_line(P_prev)
                ws_restart_pre = get_ws_restart_line(P_prev)

                if get_P_HWS(ws_prev) > 0:
                    factor = P_prev / get_P_HWS(ws_prev)
                else:
                    factor = 1

                if ws_now <= ws_prev:
                    # if the ws decreased
                    if ws_now >= ws_restart_pre:
                        # ws_now is above restart then keep constant power
                        # wind.loc[time,'P'] = P_prev
                        # ws_now is above restart then move in the rated power
                        # curve scaled to match the P_prev
                        P_new[i_time] = get_P_HWS(ws_now) * factor

                    else:
                        # ws_now is below the restart then use restart_line
                        P_new[i_time] = get_P_restart_line(ws_now)

                else:
                    # if the ws increased
                    if ws_now <= ws_shutdown_pre:
                        # ws_now below shutdown then keep constant power
                        # wind.loc[time,'P'] = P_prev
                        # ws_now below shutdown then move in the rated power
                        # curve scaled to match the P_prev
                        P_new[i_time] = get_P_HWS(ws_now) * factor

                    else:
                        P_new[i_time] = get_P_shutdown_line(ws_now)

                # check P_new again for consistency with the shutdown and restart lines
                # Necessary because the lines were checked with P_prev and not
                # P_new
                ws_shutdown_new = get_ws_shutdown_line(P_new[i_time])
                ws_restart_new = get_ws_restart_line(P_new[i_time])
                if ws_now <= ws_restart_new:
                    P_new[i_time] = get_P_restart_line(ws_now)

                if ws_now >= ws_shutdown_new:
                    P_new[i_time] = get_P_shutdown_line(ws_now)

                if P_new[i_time] > get_P_HWS(ws_now):
                    P_new[i_time] = get_P_HWS(ws_now)

    return P_new


def get_P_storm_shutdown(
    ws,
    P,
    pc,
    ws_shutdown_begin,
    ws_shutdown_end,
    ws_restart_begin,
    ws_restart_end,
    ws_HWS,
    p_HWS,
    storm_shutdown=True,
):
    """
    Function that returns the power time series for a given plant
    name, using then storm shutdown parameters and the power curve.

    Parameters
    ----------
    ws: DataFrame
        wind speed time series

    P: DataFrame
        Power time series

    pc: DataFrame
        Power curve

    ws_shutdown_begin: float
        Wind speed at shutdown begin

    ws_shutdown_end: float
        Wind speed at shutdown end

    ws_restart_begin: float
        Wind speed at restart begin

    ws_restart_end: float
        Wind speed at restart end

    ws_HWS: array
        Wind speed array at high wind speed part of the power curve. After rated power.

    p_HWS: array
        Power array at high wind speed part of the power curve. After rated power.

    storm_shutdown: Boolean
        Flag to evaluate the plant storm shutdown model.

    """

    if storm_shutdown:

        first_step_flag = False

        if ws.values[0] > ws_restart_end:
            first_step_flag = True
            init_step = ws.iloc[0, 0]
            ws.iloc[0, 0] = ws_restart_end

        all_times = ws.index
        wind_storms = pd.DataFrame(index=all_times)
        wind_storms["above_ws_restart_end"] = (ws > ws_restart_end) + 0
        wind_storms["diff_above_ws_restart_end"] = np.append(
            np.diff(wind_storms.above_ws_restart_end + 0), [0]
        )
        wind_storms.reset_index(inplace=True)

        storm_start_time = wind_storms.loc[
            wind_storms.diff_above_ws_restart_end > 0, :
        ].index.values
        storm_end_time = wind_storms.loc[
            wind_storms.diff_above_ws_restart_end < 0, :
        ].index.values
        if len(storm_start_time) > len(storm_end_time):
            storm_end_time = np.append(storm_end_time, [len(all_times) - 1])
        elif len(storm_start_time) < len(storm_end_time):
            storm_start_time = np.append([0], storm_start_time)

        P["P"] = apply_storm_shutdown(
            ws.values,
            P["P"].values,
            storm_start_time,
            storm_end_time,
            ws_shutdown_begin,
            ws_shutdown_end,
            ws_restart_begin,
            ws_restart_end,
            ws_HWS,
            p_HWS,
        )

        if "WD" in list(P.keys()):
            P = P.values

        if first_step_flag:
            ws.iloc[0, 0] = init_step

    return P
