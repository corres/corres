# syntax=docker/dockerfile:experimental

# Stage 1 -- Create container with boa to build from
FROM mambaorg/micromamba:0.21.2 AS install_pip

RUN --mount=type=cache,mode=0777,uid=1000,target=/opt/conda/pkgs \
     micromamba install -n base -c conda-forge -y pip git

# Stage2 -- Build package
FROM install_pip AS builder

COPY --chown=$MAMBA_USER:$MAMBA_USER . /tmp/.

WORKDIR /tmp

ARG ARG MAMBA_DOCKERFILE_ACTIVATE=1
RUN --mount=type=cache,mode=0777,uid=1000,target=/opt/conda/pkgs \
    pip wheel -w wheels .


# Stage 3 create environment
FROM mambaorg/micromamba:0.20.0

COPY --from=builder /tmp/wheels /tmp/wheels
COPY --chown=$MAMBA_USER:$MAMBA_USER .conda_env/dev_unix.yml /tmp/dev_unix.yml
COPY --chown=$MAMBA_USER:$MAMBA_USER executable/app_corres_script.py /opt/conda/bin/app_corres_script.py
RUN chmod ugo+rx /opt/conda/bin/app_corres_script.py


RUN --mount=type=cache,mode=0777,uid=1000,target=/opt/conda/pkgs \
    --mount=type=cache,mode=0777,uid=1000,target=/home/mambauser/.cache/pip \
    micromamba install -y -f /tmp/dev_unix.yml


ARG ARG MAMBA_DOCKERFILE_ACTIVATE=1

RUN filename=$(find /tmp/wheels -name 'corres*' -type f -printf "%f\n") \
     && pip install --no-deps --upgrade /tmp/wheels/$filename


# Add configuration file
COPY --from=builder /tmp/executable/config.yml /etc/corres/config.yml
