Research
==============

Comprehensive, fast, user-friendly and thoroughly validated open-source energy system planning framework (Mopo) (2023-2026)
-----------------------------------------------------------------------------------------------------------------------------

The overall objective of Mopo is to develop a validated, user-friendly, feature-rich, innovative and well-performing energy system modelling toolset to serve public authorities, network operators, industry and academia to plan sustainable and resilient energy systems in a cost-effective manner. CorRES is used to analyse wind and solar generation and to provide weather related inputs to hydro power and demand side modelling. Both reanalysis data and climate projections are applied. Partners: VTT (coordinator), TNO, EPRI, DTU, Energy Reform, KU Leuven, KTH, VITO, Fortum, eScience Center, Fondazione ICONS, UCD, Fluxys, Ministerie van Infrastructuur en Waterstaat. Funding: EU Horizon Europe.

See the  `project page <https://cris.vtt.fi/en/projects/comprehensive-fast-user-friendly-and-thoroughly-validated-open-so>`_ for more information.

Resilient energy systems (Resilient) (2023-2026)
------------------------------------------------

Resiliency of future Nordic and European power and energy systems, focusing on weather and climate variability. CorRES is used to analyse wind and solar generation and to provide weather related inputs to hydro power and demand side modelling. Both reanalysis data and climate projections are applied. Funding: Research Council of Norway.

Renewable Energy Analytics for Lifetime Investment and Systems Engineering (REALISE) (2022-2024)
---------------------------------------------------------------------------------------------------

The purpose of the project is to model and optimise complete and integrated renewable energy parks, meaning parks with multiple types of electricity production and storage, conversion into heat, hydrogen, etc. and co-locating with power intensive industries. This will increase the value of the parks both to technology developers, owners, and society. CorRES is used to simulate wind and solar generation both at the system-level and at the plant-level to enable estimation of revenues in future scenarios. Partners: Vattenfall (lead), DTU and Energy Cluster Denmark. Funding: EUDP (Danish Energy Agency).

BaltHub (2021-2022)
-------------------

The Interconnecting the Baltic Sea countries via offshore energy hubs (BaltHub) project analyses the cost-effectiveness of Baltic Sea energy hubs and how they can be used to interconnect the onshore energy systems of the Baltic Sea countries. CorRES has been used to simulate pan-European wind and solar time series, with focus on modeling offshore energy hubs. Partners: DTU, Tallinn University of Technology, SINTEF, Kaunas University of Technology. Funding: Nordic Energy Research.

More information: `https://orbit.dtu.dk/en/projects/interconnecting-the-baltic-sea-countries-via-offshore-energy-hubs <https://orbit.dtu.dk/en/projects/interconnecting-the-baltic-sea-countries-via-offshore-energy-hubs>`_

PSfuture (2019-2022)
--------------------

The Power system impacts of highly weather dependent future energy systems (PSfuture) project studies the highly weather dependent energy systems of the future, to help to ensure that power systems can continue their reliable operation with as low costs as possible under increasing wind and solar penetration and sector coupling. The inclusion of climate change impacts on CorRES simulations is studied. Funding: La Cour Fellowship (DTU Wind Energy).

More information: `https://orbit.dtu.dk/en/projects/power-system-impacts-of-highly-weather-dependent-future-energy-sy <https://orbit.dtu.dk/en/projects/power-system-impacts-of-highly-weather-dependent-future-energy-sy>`_

LowWind (2019-2021)
-------------------

Partners: DTU, Vestas. The project explores the potentials of a completely new wind turbine concept – the Low-Wind turbine designed for optimal integration in a power system with a considerable amount of renewables. CorRES was used for pan-European wind and solar generation time series runs, with focus on modeling onshore wind technologies in detail. Funding: EUDP (Danish Energy Agency).

More information: `https://energiforskning.dk/en/node/15865 <https://energiforskning.dk/en/node/15865>`_

OffshoreWake (2016-2021)
------------------------

The OffshoreWake project focuses on detailed wind farm wake modeling, especially farm-to-farm wakes, and an ocean wave modeling, to quantify the wind farm wakes and sea surface effects on the operation of the Danish power system. CorRES was used for wind time series simulation, with focus on how to include large-scale farm-to-farm wakes in the runs. Partners: DTU, Vattenfall. Funding: EUDP (Danish Energy Agency).

More information: `https://www.offshorewake.dk/ <https://www.offshorewake.dk/>`_

NSON-DK (2016-2020)
-------------------

The NSON-DK project is the Danish part of a North Sea Offshore Network (NSON) project. Its main objective is to study how the future massive offshore wind power and the associated offshore grid development will affect the Danish power system on short term, medium term, and long term towards a future sustainable energy system. CorRES was used for high resolution wind generation time series runs, including forecast error analysis. Partners: DTU, EA Energy Analyses. Funding: EUDP (Danish Energy Agency), previously ForskEL.

More information: `http://www.nson-dk-project.dk/ <http://www.nson-dk-project.dk/>`_
