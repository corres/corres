Getting Started
================
CorRES is a tool developed at DTU Wind for simulating wind and solar generation time series.


Essential Tools and Datasets for CorRES
------------------------------------------

- Wind power plant and power curve data for use in CorRES can be obtained from  `thewindpower.net <https://www.thewindpower.net/>`_
- The `ERA5 Reanalysis data <https://www.ecmwf.int/en/forecasts/dataset/ecmwf-reanalysis-v5>`_  from ECMWF is used for the input atmospheric data.
- The ERA5 data can be scaled using the data from version 2 of the `Global Wind Atlas <https://globalwindatlas.info/en>`_
- CorRES uses `pvlib <https://pvlib-python.readthedocs.io/en/stable/>`_  for solar simulations.
- `PyWake <https://topfarm.pages.windenergy.dtu.dk/PyWake/>`_ powers the generic power curve and wake modelling in CorRES

Usage
-----

.. include:: ../README.md
   :parser: myst_parser.sphinx_
   :start-after: ## Usage
   :end-before: ## Contributing
