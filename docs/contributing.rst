Contributing: Bug reports & Feedback
=====================================

.. include:: ../README.md
   :parser: myst_parser.sphinx_
   :start-after: ## Contributing
   :end-before: ## Authors
