.. include:: intro.rst

Contents:
=========

.. toctree::
   :maxdepth: 2
   :caption: Getting started

   getting_started
   installation

.. toctree::
   :caption: Users Guide

   usage
   contributing

.. toctree::
   :maxdepth: 2
   :caption: Api

   modules

.. toctree::
   :maxdepth: 2
   :caption: Related work and references

   research
   consultancy
   authors
   history
