Welcome to CorRES
===================

Correlations in renewable energy sources (CorRES) is a tool to simulate wind and solar generation time series, developed at DTU Wind Energy. CorRES is used in power and energy system studies, enabling large-scale runs and plant-level analyses. It allows simulation of 10000+ plants over 35+ years with hourly resolution, considering correlations between wind and solar.


Please find an `overview of CorRES (PDF) <../static/corres_about.pdf>`_. More details can be found in the publications.

CorRES is distributed through the conda package, dependency, and environment manager via DTU Wind’s Open Conda Channel.

Consultancy
============
CorRES has been applied in many consultancy projects ( `see the Consultancy Projects section <consultancy.html>`_) . If you are interested in a quotation for a consultancy project, where we can handle the CorRES runs, required input data, and output data analyses, please get in touch with the CorRES team.

Research
============
CorRES has found application in numerous research projects too , as demonstrated in the Projects section ( `see the Research Projects section <research.html>`_).
