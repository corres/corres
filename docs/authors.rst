Authors
============

.. include:: ../README.md
   :parser: myst_parser.sphinx_
   :start-after: ## Authors
