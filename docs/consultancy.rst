Consultancy
==============

Analysis of offshore wind generation variability in Japan (2022)
-------------------------------------------------------------------
Commissioned work for the Danish Energy Agency. CorRES was used to analyze variability and the correlation of offshore wind generation across Japan. The project report `Estimation of the correlation of offshore wind power generation across Japan <https://orbit.dtu.dk/en/publications/estimation-of-the-correlation-of-offshore-wind-power-generation-a>`_ presents the results of the study.


Enhanced Operational Services for the Energy Sector, Lot 2: Support to ENTSO-E in the preparation of the Pan-European Climate Database (PECD) (2022-2025)
-----------------------------------------------------------------------------------------------------------------------------------------------------------
The work for the ECMWF, carried out in co-operation with the European Network of Transmission System Operators for Electricity (ENTSO-E), analyses how climate change impacts the key weather driven aspects of future power systems. CorRES methodology is used in the modelling of wind power.

CorRES for Energinet (2022)
-------------------------------
A web service for running CorRES is created for Energinet, the Danish Transmission System Operator.

MOG II System Integration (2020-2022)
-----------------------------------------
CorRES is used to study the system-level impacts of increasing offshore wind installations in the Belgian waters, with focus on storm shutdown and generation ramp risks. Commissioned work for Elia, the Belgian Transmission System Operator. A report of the 2020 study is available at: `https://orbit.dtu.dk/en/publications/elia-mog-ii-system-integration-public-version <https://orbit.dtu.dk/en/publications/elia-mog-ii-system-integration-public-version>`_


PECD (2013-2021)
-----------------
CorRES had been used to simulated pan-European wind and solar generation time series for the Pan-European Climate Database (PECD). Commissioned work for the European Network of Transmission System Operators for Electricity (ENTSO-E). See the Open Data section for available data.

VRE simulations for Ørsted (2019, 2021)
------------------------------------------
CorRES had been used to simulated pan-European wind and solar time series as commissioned work for Ørsted.
