
Installation Guide
===========================

CorRES is distributed as both pypi and conda packages. However, due to dependencies on several GIS utilities (GeoPandas, Rasterio), which depend on large C based libraries (GEOS, GDAL, PROJ), we recommend installing using the conda packages whenever possible.


.. include:: ../README.md
   :parser: myst_parser.sphinx_
   :start-after: ## Installation Instructions
   :end-before: ## Usage
