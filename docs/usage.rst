Simple Wind Example
====================

In this section a simple Wind simulation example is presented.

In order to replicate the example you will need to have a weather dataset saved in your locall machine. You can use as example the ERA5 dataset cropped to encompass only the geographical area of Denmark `here <https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-pressure-levels?tab=overview/>`_ . Then you need to update the location of the previously downloaded file in the examples/jupyter/config_example.yml line 31.

The same example in jupyter notebook can be found `here <../../examples/jupyter/example_hourly_run_denmark.ipynb>`_

1. Import all required packages:

.. code-block:: python

    	#!/usr/bin/python
	# -*- coding: UTF-8 -*-

	from corres.auxiliar_functions import clean, mkdir
	from corres.api import (
	    Corres,
	    get_dask_client,
	    save_vars
	    )
	from corres.fluctuations import (
	    XGLarsen_S_f,
	    Schlez_coherence_jk_f,
	    Soerensen_circular_plant_Fwf_f,
	    Kaimal_S_f,
	    )
	import corres.forecasts as fc
	import time

2. Define required inputs. By pressing on the following links you can find the input files used in this example `WPP_dataset_tests <../../examples/datasets/WPP_input_example.xlsx>`_ , `PowerCurves <../../examples/datasets/PowerCurves_test_small.xlsx>`_  and  `config <../../examples/jupyter/config_example.yml>`_:

.. code-block:: python

	config_fn = 'examples/jupyter/config_example.yml'
	locs_fn = 'examples/datasets/WPP_input_example.xlsx'
	power_curves_fn = 'examples/datasets/PowerCurves_test_small.xlsx'
	domain = None
	dataset = 'ERA5'
	exec_type = 'local_pc'
	simulation = 'WPP'
	start_date = '2020-01-01'
	end_date = '2020-12-31'
	group_by = None #'Country' #
	weigth_var = None #'Installed_capacity_MW'
	out_dir = './Results/'


3. Create Corres object. Needed in the next steps

.. code-block:: python

	cr = Corres(
		config_fn = config_fn,
		locs_fn = locs_fn,
		power_curves_fn = power_curves_fn,
		dataset = dataset,
		domain = domain,
		exec_type = exec_type,
		simulation = simulation,
		start_date = start_date,
		end_date = end_date,
		group_by = group_by,
		weigth_var = weigth_var
		)

4. Start a parallel setup

.. code-block:: python

	client = get_dask_client()

5. Extract time series from reanalysis dataset

.. code-block:: python

	cr.extract_ts()

6. Convert to power

.. code-block:: python

	df_out = cr.run_power(
		case='meso'
		)

7. Save outputs as csv

.. code-block:: python

	save_vars(
		df_var = df_out,
		out_dir = out_dir,
		)
