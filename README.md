
# CorRES

**Correlations in renewable energy sources (CorRES)** is a tool to simulate
wind and solar generation time series, developed at DTU Wind Energy.
CorRES is used in power and energy system studies, enabling large-scale
runs and plant-level analyses. It allows simulation of 10000+ plants
over 35+ years with hourly resolution, considering correlations between
wind and solar.

For more information, you can refer to the [overview of CorRES (PDF)](https://corres.windenergy.dtu.dk/static/pdf/corres_about.pdf). Additionally, detailed descriptions of the CorRES method and applications thereof can be found in related [scientific publications](https://corres.windenergy.dtu.dk/publications).

CorRES is distributed through the conda package, dependency, and
environment manager via DTU Wind’s Open Conda Channel.

## Installation Instructions

### Pre-Installation

Before installing CorRES, ensure you have a functional Python distribution with a package manager. We recommend using Anaconda, a comprehensive scientific Python distribution:

1. Download and install Anaconda (choose the Python 3.x version, 64-bit installer) from anaconda.com.
2. Update the root Anaconda environment by running the following command in your terminal:

    ```
    conda update --all
    ```
### Installation

When installing CorRES, it's advisable to create a new environment, especially if you have other Python programs. This practice minimizes potential conflicts among package dependencies. Follow these steps:

1. Create a new environment (e.g., `corres_venv_dev`):

    ```sh
    conda create -y -c conda-forge -n corres_venv_dev python=">3.10" pip git wrf-python finitediff
    ```

2. Activate the newly created environment:

    ```sh
    activate corres_venv_dev
    ```

3. Clone the CorRES repository and install it:

    ```sh
    git clone https://gitlab.windenergy.dtu.dk/corres/corres.git
    cd corres
    pip install .
    ```

## Build Documentation

1. Install the documentation tools:

    ```sh
    pip install spinx sphinx_rtd_theme myst-parser
    ```

2. Build documentation

    ```sh
    sphinx-build -b html -d docs/documentation/doctrees   docs/. docs/documentation/html
    ```

3. Open the file `docs/documentation/html/index.html` in your browser.

## Usage

A jupyter example for a simple wind run can be found in `examples/jupyter/example_hourly_run_denmark.ipynb`.

In order to replicate the example you will need to download [the weather
dataset](https://filesender.deic.dk/?s=download&token=4b9f7551-88fd-4062-ad15-2dd9107d7f85).

## Contributing

We are not currently supporting code contributions at this time.

If you wish to report bugs, are interested in a new feature, or wish to provide us with feedback please open an issue.

Please include the following:

-   Your operating system name and version.
-   Any details about your local setup that might be helpful
    in troubleshooting.
-   Detailed steps to reproduce the bug.


## Authors

-   Juan Pablo Murcia Leon
-   Matti Juhani Koivisto
-   Polyneikis Kanellas
-   Neil Davis
